package courier.couriersolutions.exceptions;

import courier.couriersolutions.exceptions.error.ApplicationError;
import org.springframework.web.servlet.NoHandlerFoundException;

public class HandlerNotFoundException extends PublicException {

    public HandlerNotFoundException(NoHandlerFoundException e) {
        super(ApplicationError.CS_NOT_FOUND, e);
    }

}
