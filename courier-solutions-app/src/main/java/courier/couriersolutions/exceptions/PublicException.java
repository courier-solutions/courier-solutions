package courier.couriersolutions.exceptions;

import courier.couriersolutions.exceptions.error.ApplicationError;
import org.springframework.http.HttpStatus;

public class PublicException extends RuntimeException {

    private final String errorName;

    private final String errorMessage;

    private final HttpStatus httpStatus;

    public PublicException(ApplicationError error) {
        super(error.getMessage());
        this.errorName = error.getErrorName();
        this.errorMessage = error.getMessage();
        this.httpStatus = error.getHttpStatus();
    }

    public PublicException(ApplicationError error, Exception exception) {
        super(error.getMessage() + ". " + exception.getMessage(), exception);
        this.errorName = error.getErrorName();
        this.errorMessage = error.getMessage();
        this.httpStatus = error.getHttpStatus();
    }

    public PublicException(ApplicationError error, String message) {
        super(error.getMessage() + ". " + message);
        this.errorName = error.getErrorName();
        this.errorMessage = error.getMessage();
        this.httpStatus = error.getHttpStatus();
    }

    protected PublicException(
            String errorName,
            String errorMessage,
            HttpStatus httpStatus,
            Exception exception
    ) {
        super(errorMessage + ". " + exception.getMessage(), exception);
        this.errorName = errorName;
        this.errorMessage = errorMessage;
        this.httpStatus = httpStatus;
    }

    public String getErrorName() {
        return errorName;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    @Override
    public String toString() {
        return "PublicException{" +
                "errorName='" + errorName + '\'' +
                ", errorMessage='" + errorMessage + '\'' +
                ", httpStatus=" + httpStatus +
                '}';
    }
}
