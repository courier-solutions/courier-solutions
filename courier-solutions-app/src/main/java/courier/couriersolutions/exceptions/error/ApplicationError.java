package courier.couriersolutions.exceptions.error;

import org.springframework.http.HttpStatus;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.HttpStatus.NOT_ACCEPTABLE;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;
import static org.springframework.http.HttpStatus.UNSUPPORTED_MEDIA_TYPE;
import static org.springframework.http.HttpStatus.METHOD_NOT_ALLOWED;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.BAD_REQUEST;

public enum ApplicationError {

    CS_SYS_ERR("System error", INTERNAL_SERVER_ERROR),
    CS_ACCESS_DENIED("Access denied", FORBIDDEN),
    CS_MISSING_PAYMENT_INFORMATION("Payment information missing"),
    CS_NOT_VALID_PICKUP_INFORMATION("Order pickup information is not valid"),
    CS_ORDER_CANCEL_NOT_ALLOWED("Order cancellation is not allowed"),
    CS_ORDER_EDIT_NOT_ALLOWED("Order editing is not allowed"),
    CS_ARGUMENT_TYPE_MISMATCH_ERR("Argument type mismatched with required one"),
    CS_MESSAGE_NOT_READABLE("Http message is not readable"),
    CS_NOT_ACCEPTABLE_MEDIA_TYPE("Supported media types: %s", NOT_ACCEPTABLE),
    CS_UNSUPPORTED_MEDIA_TYPE("Request media type is unsupported", UNSUPPORTED_MEDIA_TYPE),
    CS_NOT_VALID_ARGUMENT("Some of the arguments are missing"),
    CS_UNSUPPORTED_HTTP_METHOD("Http request method is unsupported", METHOD_NOT_ALLOWED),
    CS_REQUEST_PARAMETER_MISSING("Mandatory request parameter is missing"),
    CS_NOT_FOUND("Requested resource not found", NOT_FOUND),
    CS_BAD_REQUEST("Access denied"),
    CS_SESSION_EXPIRED("Session is no longer active", UNAUTHORIZED),
    CS_SESSION_ALREADY_DELETED("Session has been already deleted"),
    CS_CLIENT_NOT_FOUND("Client does not exist", NOT_FOUND),
    CS_MISSING_SESS_KEY("Missing sess key header", UNAUTHORIZED),
    CS_PASSWORDS_DONT_MATCH("Passwords do not match"),
    CS_CLIENT_ALREADY_EXISTS("Client with this email already exists"),
    CS_OPT_LOCK_ERROR("Optimistic lock exception occurred");

    private final String errorName;
    private final String message;
    private final HttpStatus httpStatus;

    ApplicationError(String message, HttpStatus httpStatus) {
        this.errorName = this.name();
        this.message = message;
        this.httpStatus = httpStatus;
    }

    ApplicationError(String message) {
        this.errorName = this.name();
        this.message = message;
        this.httpStatus = BAD_REQUEST;
    }

    public String getErrorName() {
        return errorName;
    }

    public String getMessage() {
        return message;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

}
