package courier.couriersolutions.exceptions;

import org.springframework.web.HttpMediaTypeNotAcceptableException;

import static courier.couriersolutions.exceptions.error.ApplicationError.CS_NOT_ACCEPTABLE_MEDIA_TYPE;

public class NotAcceptableMediaTypeException extends PublicException {

    public NotAcceptableMediaTypeException(HttpMediaTypeNotAcceptableException exception) {
        super(
                CS_NOT_ACCEPTABLE_MEDIA_TYPE.getErrorName(),
                String.format(CS_NOT_ACCEPTABLE_MEDIA_TYPE.getMessage(), exception.getSupportedMediaTypes()),
                CS_NOT_ACCEPTABLE_MEDIA_TYPE.getHttpStatus(),
                exception
        );
    }

}
