package courier.couriersolutions.exceptions;

public class EmptyPaymentInformation extends Exception {

    public EmptyPaymentInformation() {
        super();
    }

}
