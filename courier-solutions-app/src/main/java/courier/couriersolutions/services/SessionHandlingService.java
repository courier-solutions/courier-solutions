package courier.couriersolutions.services;

import courier.couriersolutions.entities.Client;
import courier.couriersolutions.entities.Session;
import javassist.NotFoundException;

public interface SessionHandlingService {

    String createSessionKey(Client client);

    boolean isValidSession(Client client, String session);

    Session getActiveSession(String sessKey) throws NotFoundException;

    Session getClientsActiveSession(Client client) throws NotFoundException;

    void disableSessionForClient(Client client) throws NotFoundException;

}
