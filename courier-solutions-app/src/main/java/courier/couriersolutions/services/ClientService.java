package courier.couriersolutions.services;

import courier.couriersolutions.dto.ClientDTO;
import courier.couriersolutions.exceptions.PasswordsDontMatchException;
import courier.couriersolutions.exceptions.ClientAlreadyExistsException;

public interface ClientService {

    void validate(ClientDTO client) throws PasswordsDontMatchException, ClientAlreadyExistsException;

}
