package courier.couriersolutions.services;

import courier.couriersolutions.constants.PaymentType;
import courier.couriersolutions.dto.OrderInformationDTO;
import courier.couriersolutions.entities.Client;
import courier.couriersolutions.entities.Order;
import courier.couriersolutions.entities.PaymentInformation;
import courier.couriersolutions.exceptions.EmptyPaymentInformation;
import courier.couriersolutions.repository.OrderRepository;
import courier.couriersolutions.repository.PaymentInformationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.transaction.Transactional;

@Service
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;

    private final PaymentInformationRepository paymentInformationRepository;

    @Autowired
    public OrderServiceImpl(
            OrderRepository orderRepository,
            PaymentInformationRepository paymentInformationRepository
    ) {
        this.orderRepository = orderRepository;
        this.paymentInformationRepository = paymentInformationRepository;
    }

    @Override
    public boolean isValidPickupInformation(OrderInformationDTO pickupInfo, Client sessionClient) {
        return pickupInfo.getPhoneNumber().equals(sessionClient.getMobile()) &&
                pickupInfo.getAddress().equals(sessionClient.getAddress()) &&
                pickupInfo.getEmail().equals(sessionClient.getEmail());
    }

    @Override
    @Transactional
    public Order saveOrder(Order newOrder, PaymentInformation paymentInformation) throws EmptyPaymentInformation {
        Order savedOrder = orderRepository.save(newOrder);

        if (newOrder.getPaymentType().equals(PaymentType.DEBIT_CREDIT_CARD)) {
            if (StringUtils.isEmpty(paymentInformation.getCardOwner()) ||
                    StringUtils.isEmpty(paymentInformation.getCardNumber()) ||
                    StringUtils.isEmpty(paymentInformation.getSecurityCode()) ||
                    StringUtils.isEmpty(paymentInformation.getValidThru())) {
                throw new EmptyPaymentInformation();
            }

            paymentInformation.setOrder(savedOrder);

            paymentInformationRepository.save(paymentInformation);
        }

        return savedOrder;
    }

}
