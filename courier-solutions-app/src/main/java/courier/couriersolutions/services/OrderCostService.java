package courier.couriersolutions.services;

import courier.couriersolutions.constants.DeliveryType;
import courier.couriersolutions.dto.OrderDTO;
import courier.couriersolutions.dto.ParcelDTO;

import java.time.LocalDate;

public abstract class OrderCostService {

    public abstract Float calculateOrderCost(OrderDTO order);

    protected Float getCost(OrderDTO order) {
        float cost = order.getParcelList().stream().map(this::calculateParcelCost).reduce(0f, Float::sum);

        cost += calculateDistanceCost(order.getPickupInfo().getPostalCode(), order.getDeliveryInfo().getPostalCode());

        if (order.getPickupDate().equals(LocalDate.now())) {
            cost += 1;
        }
        if (order.getDeliveryType() == DeliveryType.FAST_SHIPPING) {
            cost += 2;
        }

        if (cost < 3) {
            cost = 3;
        }

        return cost;
    }

    private Float calculateParcelCost(ParcelDTO parcel) {
        float cost = parcel.getLength() * parcel.getWidth() * parcel.getHeight() * 5;

        cost += parcel.getWeight() * 0.5;

        switch (parcel.getType()) {
            case ANIMALS:
                cost *= 2;
                break;
            case DOCUMENTS:
                cost *= 1.1;
                break;
            case FRAGILE:
                cost *= 1.5;
                break;
            default:
                break;
        }

        cost *= parcel.getQuantity();

        return cost;
    }

    // TODO see: https://ba-jira.atlassian.net/browse/CS-36
    private Float calculateDistanceCost(String pickupPostalCode, String deliveryPostalCode) {
        // ...
        // if (distance < 20) cost = distance * 0.1
        // if (distance > 100) cost = distance * 0.2
        // else cost = distance * 0.15

        // For now we'll use predefined value
        return 25f;
    }
}
