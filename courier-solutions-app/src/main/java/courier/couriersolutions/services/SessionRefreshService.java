package courier.couriersolutions.services;

import courier.couriersolutions.constants.ClientAuditActionType;
import courier.couriersolutions.entities.ClientAudit;
import courier.couriersolutions.entities.Session;
import courier.couriersolutions.repository.ClientAuditRepository;
import courier.couriersolutions.repository.SessionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class SessionRefreshService {

    private final SessionRepository sessionRepository;

    private final ClientAuditRepository clientAuditRepository;

    private static final Logger logger = LoggerFactory.getLogger(SessionRefreshService.class);

    @Value("${session.timeout.minutes}")
    public long sessionTimeoutMinutes;

    @Autowired
    public SessionRefreshService(
            SessionRepository sessionRepository,
            ClientAuditRepository clientAuditRepository
    ) {
        this.sessionRepository = sessionRepository;
        this.clientAuditRepository = clientAuditRepository;
    }

    @Transactional
    @Scheduled(fixedRate = 10000) //runs every 10 seconds
    public void sessionRefresh() {
        logger.info("[B] Session refresh");

        LocalDateTime localDateTime = LocalDateTime.now().minusMinutes(sessionTimeoutMinutes);

        List<Session> notValidSessions = sessionRepository.findAllByUpdateDateTimeBefore(localDateTime);

        if (!notValidSessions.isEmpty()) {
            sessionRepository.deleteAll(notValidSessions);

            updateClientAudit(notValidSessions);
            logger.info("Deleted {} sessions", notValidSessions.size());
        }

        logger.info("[E] Session refresh");
    }

    private void updateClientAudit(List<Session> deactivatedSessions) {
        List<ClientAudit> clientAudits = deactivatedSessions.stream()
                .map(session -> {
                    ClientAudit clientAudit = new ClientAudit();
                    clientAudit.setClient(session.getClient());
                    clientAudit.setActionType(ClientAuditActionType.SYSTEM);
                    clientAudit.setAction("Delete session");

                    return clientAudit;
                })
                .collect(Collectors.toList());
        clientAuditRepository.saveAll(clientAudits);
    }

}
