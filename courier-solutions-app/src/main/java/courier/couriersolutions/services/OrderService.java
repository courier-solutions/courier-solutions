package courier.couriersolutions.services;

import courier.couriersolutions.dto.OrderInformationDTO;
import courier.couriersolutions.entities.Client;
import courier.couriersolutions.entities.Order;
import courier.couriersolutions.entities.PaymentInformation;
import courier.couriersolutions.exceptions.EmptyPaymentInformation;

public interface OrderService {

    boolean isValidPickupInformation(OrderInformationDTO pickupInfo, Client sessionClient);

    Order saveOrder(Order newOrder, PaymentInformation paymentInformation) throws EmptyPaymentInformation;

}
