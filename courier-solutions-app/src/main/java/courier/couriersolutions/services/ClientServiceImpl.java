package courier.couriersolutions.services;

import courier.couriersolutions.dto.ClientDTO;
import courier.couriersolutions.exceptions.PasswordsDontMatchException;
import courier.couriersolutions.exceptions.ClientAlreadyExistsException;
import courier.couriersolutions.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClientServiceImpl implements ClientService {

    private final ClientRepository clientRepository;

    @Autowired
    public ClientServiceImpl(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    @Override
    public void validate(ClientDTO client)
        throws PasswordsDontMatchException, ClientAlreadyExistsException {
        if (!client.getPassword().equals(client.getRepeatPassword())) {
            throw new PasswordsDontMatchException();
        }

        if (clientRepository.existsByEmail(client.getEmail())) {
            throw new ClientAlreadyExistsException();
        }
    }

}
