package courier.couriersolutions.services;

import courier.couriersolutions.dto.OrderDTO;
import courier.couriersolutions.utils.NumericInputUtils;

public class ReducedOrderCostService extends OrderCostService {

    @Override
    public Float calculateOrderCost(OrderDTO order) {
        Float cost = super.getCost(order);

        return NumericInputUtils.roundUpTo2Digits(cost * 0.85f);
    }

}
