package courier.couriersolutions.services;

import courier.couriersolutions.constants.ClientAuditActionType;
import courier.couriersolutions.entities.Client;
import courier.couriersolutions.entities.ClientAudit;
import courier.couriersolutions.entities.Session;
import courier.couriersolutions.repository.ClientAuditRepository;
import courier.couriersolutions.repository.SessionRepository;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.Base64;
import java.util.Optional;
import java.util.UUID;

@Service
public class SessionHandlingServiceImpl implements SessionHandlingService {

    private final SessionRepository sessionRepository;

    private final ClientAuditRepository clientAuditRepository;

    private static final String NO_VALID_SESSION = "Client doesn't have any active sessions";

    @Autowired
    public SessionHandlingServiceImpl(
            SessionRepository sessionRepository,
            ClientAuditRepository clientAuditRepository
    ) {
        this.sessionRepository = sessionRepository;
        this.clientAuditRepository = clientAuditRepository;
    }

    @Transactional
    public String createSessionKey(Client client) {
        Optional<Session> oldClientSession = sessionRepository.findByClient(client);

        if (oldClientSession.isPresent()) {
            Session oldSession = oldClientSession.get();
            sessionRepository.delete(oldSession);
        }

        Session session = new Session(client, getGeneratedSessKey());

        session = sessionRepository.save(session);

        saveSessionUpdateToClientAudit(client, "Create session");

        return session.getSessionKey();
    }

    @Transactional
    public boolean isValidSession(Client client, String session) {
        Optional<Session> currentSession = sessionRepository.findByClientAndSessionKey(client, session);

        if (currentSession.isPresent()) {
            updateSession(currentSession.get());
            return true;
        }

        return false;
    }

    @Transactional
    public Session getActiveSession(String sessKey) throws NotFoundException {
        Session currentSession = sessionRepository.findBySessionKey(sessKey)
                .orElseThrow(() -> new NotFoundException(NO_VALID_SESSION));

        updateSession(currentSession);

        return currentSession;
    }

    @Transactional
    public Session getClientsActiveSession(Client client) throws NotFoundException {
        Session currentSession = sessionRepository.findByClient(client)
                .orElseThrow(() -> new NotFoundException(NO_VALID_SESSION));

        updateSession(currentSession);

        return currentSession;
    }

    @Transactional
    public void disableSessionForClient(Client client) throws NotFoundException {
        Session currentSession = sessionRepository.findByClient(client)
                .orElseThrow(() -> new NotFoundException(NO_VALID_SESSION));

        sessionRepository.delete(currentSession);

        saveSessionUpdateToClientAudit(client, "Delete session");
    }

    private void saveSessionUpdateToClientAudit(Client client, String action) {
        ClientAudit clientAudit = new ClientAudit();
        clientAudit.setClient(client);
        clientAudit.setActionType(ClientAuditActionType.SYSTEM);
        clientAudit.setAction(action);

        clientAuditRepository.save(clientAudit);
    }

    private void updateSession(Session session) {
        session.setUpdateDateTime(LocalDateTime.now());
        sessionRepository.save(session);
    }

    private String getGeneratedSessKey() {
        return Base64.getEncoder()
                .encodeToString(
                        UUID.randomUUID().toString().getBytes(StandardCharsets.UTF_8)
                );
    }

}
