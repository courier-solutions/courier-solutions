package courier.couriersolutions.configuration;

import courier.couriersolutions.CSHttpHeaders;
import courier.couriersolutions.constants.ClientAuditActionType;
import courier.couriersolutions.entities.Client;
import courier.couriersolutions.entities.ClientAudit;
import courier.couriersolutions.entities.Session;
import courier.couriersolutions.entities.SessionInformation;
import courier.couriersolutions.exceptions.PublicException;
import courier.couriersolutions.exceptions.error.ApplicationError;
import courier.couriersolutions.repository.ClientAuditRepository;
import courier.couriersolutions.services.SessionHandlingService;
import javassist.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class SessionInterceptor extends HandlerInterceptorAdapter {

    private static final Logger logger = LoggerFactory.getLogger(SessionInterceptor.class);

    private final SessionHandlingService sessionHandlingService;

    private final ClientAuditRepository clientAuditRepository;

    private final boolean auditLoggingEnabled;

    public SessionInterceptor(
            SessionHandlingService sessionHandlingService,
            ClientAuditRepository clientAuditRepository,
            boolean auditLoggingEnabled
    ) {
        this.sessionHandlingService = sessionHandlingService;
        this.clientAuditRepository = clientAuditRepository;
        this.auditLoggingEnabled = auditLoggingEnabled;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        logger.info("[B] Session check");
        String sessKeyCookie = getCookieValue(request.getCookies());

        if (StringUtils.isEmpty(sessKeyCookie)) {
            throw new PublicException(ApplicationError.CS_MISSING_SESS_KEY);
        }

        Session session;

        try {
            session = sessionHandlingService.getActiveSession(sessKeyCookie);
        } catch (NotFoundException e) {
            throw new PublicException(ApplicationError.CS_SESSION_EXPIRED);
        }

        SessionClientId.createInstance(session.getClient().getId());

        if (auditLoggingEnabled) {
            saveClientAudit(session.getClient(), String.join(" ", request.getMethod(), request.getRequestURI()));
        }

        SessionInformation sessionInformation = toSessionInformation(session);

        request.setAttribute("session", sessionInformation);

        logger.info("[E] Session check");

        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        super.postHandle(request, response, handler, modelAndView);

        SessionClientId.deleteInstance();
    }

    private void saveClientAudit(Client authenticatedClient, String endpoint) {
        ClientAudit clientAudit = new ClientAudit();
        clientAudit.setClient(authenticatedClient);
        clientAudit.setActionType(ClientAuditActionType.ACCESS_API);
        clientAudit.setAction(endpoint);

        clientAuditRepository.save(clientAudit);
    }

    private SessionInformation toSessionInformation(Session session) {
        return new SessionInformation(session.getSessionKey(), session.getClient());
    }

    private String getCookieValue(Cookie[] cookies) {
        if (cookies == null) {
            return null;
        }

        List<Cookie> sortedCookiesList = Arrays.stream(cookies)
                .filter(cookie -> cookie.getName().equals(CSHttpHeaders.SESS_KEY))
                .collect(Collectors.toList());

        if (sortedCookiesList.isEmpty()) {
            return null;
        }

        return sortedCookiesList.get(0).getValue();
    }

}
