package courier.couriersolutions.configuration;

import courier.couriersolutions.services.OrderCostService;
import courier.couriersolutions.services.ReducedOrderCostService;
import courier.couriersolutions.services.StandardOrderCostService;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OrderCostConfig {

    @Bean(name = "standardOrderCost")
    @ConditionalOnProperty(prefix = "orders", name = "pricing", havingValue = "standard")
    public OrderCostService orderCostServiceStandard() {
        return new StandardOrderCostService();
    }

    @Bean(name = "reducedOrderCost")
    @ConditionalOnProperty(prefix = "orders", name = "pricing", havingValue = "reduced")
    public OrderCostService orderCostServiceReduced() {
        return new ReducedOrderCostService();
    }

}
