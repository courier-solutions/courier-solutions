package courier.couriersolutions.configuration;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

@Aspect
@Component
@ConditionalOnProperty(
        value = "audit.logging.enabled",
        havingValue = "true",
        matchIfMissing = false
)
public class LoggingAspect {

    private static final Logger logger = LoggerFactory.getLogger(LoggingAspect.class);

    @Around("execution(* courier.couriersolutions.controller..*(..))")
    public Object logControllers(ProceedingJoinPoint joinPoint) throws Throwable {
        return logExecutionTime(joinPoint);
    }

    @Around("execution(* courier.couriersolutions.repository..*(..))")
    public Object logRepositories(ProceedingJoinPoint joinPoint) throws Throwable {
        return logExecutionTime(joinPoint);
    }

    @Around("execution(* courier.couriersolutions.services..*(..))")
    public Object logServices(ProceedingJoinPoint joinPoint) throws Throwable {
        return logExecutionTime(joinPoint);
    }

    @Around("execution(* courier.couriersolutions.utils..*(..))")
    public Object logUtils(ProceedingJoinPoint joinPoint) throws Throwable {
        return logExecutionTime(joinPoint);
    }

    @Around("execution(* courier.couriersolutions.configuration.SessionInterceptor.*(..))")
    public Object logConfiguration(ProceedingJoinPoint joinPoint) throws Throwable {
        return logExecutionTime(joinPoint);
    }

    public Object logExecutionTime(ProceedingJoinPoint joinPoint) throws Throwable {
        Object proceed;

        Long clientId = SessionClientId.getClientId();

        try {
            proceed = joinPoint.proceed();
        } catch (Exception e) {
            logger.info("[INFO_LOGGING] clientId={}, status=FAILED, method={}", clientId, joinPoint.getSignature());
            throw e;
        }

        logger.info("[INFO_LOGGING]  clientId={}, status=OK, method={}", clientId, joinPoint.getSignature());

        return proceed;
    }

}
