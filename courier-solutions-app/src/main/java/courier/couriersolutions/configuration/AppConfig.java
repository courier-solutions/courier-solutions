package courier.couriersolutions.configuration;

import courier.couriersolutions.repository.ClientAuditRepository;
import courier.couriersolutions.services.SessionHandlingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableAspectJAutoProxy(proxyTargetClass = true)
@ComponentScan(basePackages = "courier.couriersolutions")
public class AppConfig implements WebMvcConfigurer {

    private final SessionHandlingService sessionHandlingService;

    private final ClientAuditRepository clientAuditRepository;

    @Value("${audit.logging.enabled}")
    private boolean auditLoggingEnabled;

    @Autowired
    public AppConfig(
            SessionHandlingService sessionHandlingService,
            ClientAuditRepository clientAuditRepository
    ) {
        this.sessionHandlingService = sessionHandlingService;
        this.clientAuditRepository = clientAuditRepository;
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/internal/docs/**")
                .addResourceLocations("/docs/");
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new SessionInterceptor(sessionHandlingService, clientAuditRepository, auditLoggingEnabled))
                .addPathPatterns("/api/**")
                .excludePathPatterns("/api/login**")
                .excludePathPatterns("/api/register**");
    }

}
