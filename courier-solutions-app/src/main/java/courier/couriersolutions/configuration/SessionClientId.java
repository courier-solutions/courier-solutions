package courier.couriersolutions.configuration;

public class SessionClientId {

    private static SessionClientId singleInstance = null;

    private static Long clientId;

    private SessionClientId() {
        //Empty for auto init
    }

    public static Long getClientId() {
        return clientId;
    }

    public static SessionClientId createInstance(Long clientId) {
        singleInstance = new SessionClientId();

        SessionClientId.clientId = clientId;

        return singleInstance;
    }

    public static void deleteInstance() {
        SessionClientId.clientId = null;
        singleInstance = null;
    }

}
