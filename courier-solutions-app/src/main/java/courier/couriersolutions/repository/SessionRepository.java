package courier.couriersolutions.repository;

import courier.couriersolutions.entities.Client;
import courier.couriersolutions.entities.Session;
import org.springframework.data.repository.CrudRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface SessionRepository extends CrudRepository<Session, Long> {

    Optional<Session> findByClient(Client client);

    Optional<Session> findByClientAndSessionKey(Client client, String sessionKey);

    Optional<Session> findBySessionKey(String sessionKey);

    List<Session> findAllByUpdateDateTimeBefore(LocalDateTime localDateTime);

}
