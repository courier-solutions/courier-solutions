package courier.couriersolutions.repository;

import courier.couriersolutions.entities.PaymentInformation;
import org.springframework.data.repository.CrudRepository;

public interface PaymentInformationRepository extends CrudRepository<PaymentInformation, Long> {

}
