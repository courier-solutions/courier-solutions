package courier.couriersolutions.repository;

import courier.couriersolutions.entities.Order;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface OrderRepository extends JpaRepository<Order, Long> {

    List<Order> findAllByClientId(Long clientId);

    Optional<Order> findByIdAndClientId(Long id, Long clientId);

}
