package courier.couriersolutions.repository;

import courier.couriersolutions.entities.Client;
import courier.couriersolutions.entities.Session;
import org.springframework.data.repository.CrudRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface ClientRepository extends CrudRepository<Client, Long> {

    boolean existsByEmail(String email);

    Optional<Client> findByEmailAndPassword(String email, String password);

}
