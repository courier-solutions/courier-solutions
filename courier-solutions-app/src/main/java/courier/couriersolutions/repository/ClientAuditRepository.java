package courier.couriersolutions.repository;

import courier.couriersolutions.entities.ClientAudit;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

public interface ClientAuditRepository extends CrudRepository<ClientAudit, Long> {

    @Transactional
    void deleteAllByClientId(long id);

}
