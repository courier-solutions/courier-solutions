package courier.couriersolutions.controller;

import courier.couriersolutions.dto.OrderDTO;
import courier.couriersolutions.dto.OrderInformationDTO;
import courier.couriersolutions.dto.OrderWithPaymentDTO;
import courier.couriersolutions.dto.ParcelDTO;
import courier.couriersolutions.dto.PaymentInformationDTO;
import courier.couriersolutions.constants.OrderStatus;
import courier.couriersolutions.entities.Client;
import courier.couriersolutions.entities.Order;
import courier.couriersolutions.entities.OrderInformation;
import courier.couriersolutions.entities.Parcel;
import courier.couriersolutions.entities.PaymentInformation;
import courier.couriersolutions.entities.SessionInformation;
import courier.couriersolutions.exceptions.EmptyPaymentInformation;
import courier.couriersolutions.exceptions.PublicException;
import courier.couriersolutions.exceptions.error.ApplicationError;
import courier.couriersolutions.repository.OrderRepository;
import courier.couriersolutions.services.OrderCostService;
import courier.couriersolutions.services.OrderService;
import courier.couriersolutions.utils.NumericInputUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
public class OrderController implements BaseController {

    private final OrderRepository orderRepository;

    private final OrderService orderService;

    private final OrderCostService orderCostService;

    @Autowired
    public OrderController(
            OrderRepository orderRepository,
            OrderService orderService,
            OrderCostService orderCostService
    ) {
        this.orderRepository = orderRepository;
        this.orderService = orderService;
        this.orderCostService = orderCostService;
    }

    @PostMapping("/orders")
    public Order createOrder(
            @ModelAttribute("sessionBeingUsed") SessionInformation sessionBeingUsed,
            @RequestBody @Valid OrderWithPaymentDTO order
    ) {
        //ToDo: add check that if price exceeds 10 Eur, CASH payment type cannot be used.
        if (!orderService.isValidPickupInformation(order.getPickupInfo(), sessionBeingUsed.getClient())) {
            throw new PublicException(ApplicationError.CS_NOT_VALID_PICKUP_INFORMATION);
        }

        List<Parcel> parcelList = parcelDTOListToParcelList(order.getParcelList());

        OrderInformation pickupInformation = orderInformationDTOtoOrderInformation(order.getPickupInfo());

        OrderInformation deliveryInformation = orderInformationDTOtoOrderInformation(order.getDeliveryInfo());

        PaymentInformation paymentInformation = paymentInformationDTOtoPaymentInformation(order.getPaymentInformationDTO());

        Float cost = orderCostService.calculateOrderCost(order);

        Order newOrder = new Order(
                sessionBeingUsed.getClient(),
                parcelList,
                pickupInformation,
                order.getPickupDate(),
                deliveryInformation,
                order.getDeliveryType(),
                order.getComment(),
                order.getPaymentType(),
                cost
        );

        try {
            return orderService.saveOrder(newOrder, paymentInformation);
        } catch (EmptyPaymentInformation e) {
            throw new PublicException(ApplicationError.CS_MISSING_PAYMENT_INFORMATION);
        }
    }

    @GetMapping("/orders")
    public List<Order> getOrders(
            @ModelAttribute("sessionBeingUsed") SessionInformation sessionBeingUsed
    ) {
        Client client = sessionBeingUsed.getClient();
        return orderRepository.findAllByClientId(client.getId());
    }

    @PutMapping("/orders/cancel/{id}")
    public Order cancelOrder(
            @ModelAttribute("sessionBeingUsed") SessionInformation sessionBeingUsed,
            @PathVariable Long id
    ) {
        Client client = sessionBeingUsed.getClient();

        Order order = orderRepository.findByIdAndClientId(id, client.getId())
                .orElseThrow(() -> new PublicException(ApplicationError.CS_ACCESS_DENIED));

        if (!order.getStatus().equals(OrderStatus.VERIFIED)) {
            throw new PublicException(ApplicationError.CS_ORDER_CANCEL_NOT_ALLOWED);
        }

        order.setStatus(OrderStatus.CANCELED);
        return orderRepository.save(order);
    }

    @PutMapping("/orders/{id}")
    public Order editOrder(
            @ModelAttribute("sessionBeingUsed") SessionInformation sessionBeingUsed,
            @RequestBody @Valid OrderDTO orderDTO,
            @PathVariable Long id
    ) {
        Client client = sessionBeingUsed.getClient();

        Order order = orderRepository.findByIdAndClientId(id, client.getId())
                .orElseThrow(() -> new PublicException(ApplicationError.CS_ACCESS_DENIED));

        if (!order.getStatus().equals(OrderStatus.VERIFIED)) {
            throw new PublicException(ApplicationError.CS_ORDER_EDIT_NOT_ALLOWED);
        }

        if (!orderService.isValidPickupInformation(orderDTO.getPickupInfo(), client)) {
            throw new PublicException(ApplicationError.CS_NOT_VALID_PICKUP_INFORMATION);
        }

        OrderInformation pickupInformation = orderInformationDTOtoOrderInformation(orderDTO.getPickupInfo());
        pickupInformation.setId(order.getPickupInfo().getId());

        OrderInformation deliveryInformation = orderInformationDTOtoOrderInformation(orderDTO.getDeliveryInfo());
        deliveryInformation.setId(order.getDeliveryInfo().getId());

        Float cost = orderCostService.calculateOrderCost(orderDTO);
        order.setCost(cost);

        order.setPickupInfo(pickupInformation);
        order.setPickupDate(orderDTO.getPickupDate());
        order.setDeliveryInfo(deliveryInformation);
        order.setDeliveryType(orderDTO.getDeliveryType());
        order.setComment(orderDTO.getComment());
        return orderRepository.save(order);
    }

    private PaymentInformation paymentInformationDTOtoPaymentInformation(PaymentInformationDTO paymentInformationDTO) {
        return new PaymentInformation(
                paymentInformationDTO.getCardNumber(),
                paymentInformationDTO.getSecurityCode(),
                paymentInformationDTO.getValidThru()
        );
    }

    private List<Parcel> parcelDTOListToParcelList(List<ParcelDTO> parcelList) {
        return parcelList.stream()
                .map(parcel -> new Parcel(
                        parcel.getType(),
                        NumericInputUtils.roundUpTo2Digits(parcel.getLength()),
                        NumericInputUtils.roundUpTo2Digits(parcel.getWidth()),
                        NumericInputUtils.roundUpTo2Digits(parcel.getHeight()),
                        NumericInputUtils.roundUpTo2Digits(parcel.getWeight()),
                        parcel.getQuantity()
                ))
                .collect(Collectors.toList());
    }

    private OrderInformation orderInformationDTOtoOrderInformation(OrderInformationDTO orderInformationDTO) {
        return new OrderInformation(
                orderInformationDTO.getName(),
                orderInformationDTO.getEmail(),
                orderInformationDTO.getCity(),
                orderInformationDTO.getAddress(),
                orderInformationDTO.getPostalCode(),
                orderInformationDTO.getPhoneNumber()
        );
    }
}
