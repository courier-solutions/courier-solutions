package courier.couriersolutions.controller;

import courier.couriersolutions.CSHttpHeaders;
import courier.couriersolutions.dto.ClientDTO;
import courier.couriersolutions.dto.LogInDTO;
import courier.couriersolutions.dto.LogInResponseDTO;
import courier.couriersolutions.entities.Client;
import courier.couriersolutions.exceptions.PasswordsDontMatchException;
import courier.couriersolutions.exceptions.PublicException;
import courier.couriersolutions.exceptions.ClientAlreadyExistsException;
import courier.couriersolutions.exceptions.error.ApplicationError;
import courier.couriersolutions.repository.ClientRepository;
import courier.couriersolutions.services.SessionHandlingService;
import courier.couriersolutions.services.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@RestController
@RequestMapping("/api")
public class AccessController {

    private final SessionHandlingService sessionHandlingService;

    private final ClientService clientService;

    private final ClientRepository clientRepository;

    @Autowired
    public AccessController(
            SessionHandlingService sessionHandlingService,
            ClientService clientService,
            ClientRepository clientRepository
    ) {
        this.sessionHandlingService = sessionHandlingService;
        this.clientService = clientService;
        this.clientRepository = clientRepository;
    }

    @PostMapping("/register")
    public Client createNewClient(@RequestBody @Valid ClientDTO clientDTO) {
        try {
            clientService.validate(clientDTO);
        } catch (PasswordsDontMatchException e) {
            throw new PublicException(ApplicationError.CS_PASSWORDS_DONT_MATCH);
        } catch (ClientAlreadyExistsException e) {
            throw new PublicException(ApplicationError.CS_CLIENT_ALREADY_EXISTS);
        }
        Client client = new Client(
            clientDTO.getEmail(),
            clientDTO.getPassword(),
            clientDTO.getFirstName(),
            clientDTO.getLastName(),
            clientDTO.getAddress(),
            clientDTO.getMobile()
        );
        return clientRepository.save(client);
    }

    @PostMapping("/login")
    public ResponseEntity<LogInResponseDTO> loginUser(
            @RequestBody @Valid LogInDTO logInDTO,
            HttpServletResponse response
    ) {
        Client authenticatedClient = clientRepository.findByEmailAndPassword(logInDTO.getUsername(), logInDTO.getPassword())
                .orElseThrow(() -> new PublicException(ApplicationError.CS_CLIENT_NOT_FOUND));

        String sessionKey = sessionHandlingService.createSessionKey(authenticatedClient);

        Cookie cookie = new Cookie(CSHttpHeaders.SESS_KEY, sessionKey);
        cookie.setHttpOnly(true);
        cookie.setSecure(true);

        response.addCookie(cookie);
        return ResponseEntity.ok(new LogInResponseDTO("Success"));
    }

}
