package courier.couriersolutions.controller;

import courier.couriersolutions.dto.LogOutResponseDTO;
import courier.couriersolutions.entities.Client;
import courier.couriersolutions.entities.SessionInformation;
import courier.couriersolutions.exceptions.PublicException;
import courier.couriersolutions.exceptions.error.ApplicationError;
import courier.couriersolutions.services.SessionHandlingService;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class LogOutController implements BaseController {

    private final SessionHandlingService sessionHandlingService;

    @Autowired
    public LogOutController(
            SessionHandlingService sessionHandlingService
    ) {
        this.sessionHandlingService = sessionHandlingService;
    }

    @PostMapping("/logout")
    public ResponseEntity<LogOutResponseDTO> logoutUser(
            @ModelAttribute("sessionBeingUsed") SessionInformation sessionBeingUsed
    ) {
        Client currentClient = sessionBeingUsed.getClient();

        try {
            sessionHandlingService.disableSessionForClient(currentClient);
        } catch (NotFoundException e) {
            throw new PublicException(ApplicationError.CS_SESSION_ALREADY_DELETED);
        }

        return ResponseEntity.ok(new LogOutResponseDTO("Success"));
    }

}
