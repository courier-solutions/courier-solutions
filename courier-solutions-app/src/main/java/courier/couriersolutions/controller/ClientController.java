package courier.couriersolutions.controller;

import courier.couriersolutions.dto.ClientInfoDTO;
import courier.couriersolutions.dto.PassInfoDTO;
import courier.couriersolutions.entities.Client;
import courier.couriersolutions.entities.SessionInformation;
import courier.couriersolutions.exceptions.PublicException;
import courier.couriersolutions.exceptions.error.ApplicationError;
import courier.couriersolutions.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api")
public class ClientController implements BaseController {

    private final ClientRepository clientRepository;

    private final boolean sleepEnabled;

    @Autowired
    public ClientController(
            ClientRepository clientRepository,
            @Value("${client.update.endpoint.sleep.enable}") boolean sleepEnabled
    ) {
        this.clientRepository = clientRepository;
        this.sleepEnabled = sleepEnabled;
    }

    @PutMapping("/client-password")
    public Client updateClientPass(
            @RequestBody @Valid PassInfoDTO passInfoDTO,
            @ModelAttribute("sessionBeingUsed") SessionInformation sessionBeingUsed
    ) {
        Client currentClient = sessionBeingUsed.getClient();

        if (!passInfoDTO.getNewPassword().equals(passInfoDTO.getRepeatNewPassword())) {
            throw new PublicException(ApplicationError.CS_PASSWORDS_DONT_MATCH);
        }

        if (!currentClient.getPassword().equals(passInfoDTO.getCurrentPassword())) {
            throw new PublicException(ApplicationError.CS_ACCESS_DENIED);
        }

        currentClient.setPassword(passInfoDTO.getNewPassword());

        return clientRepository.save(currentClient);
    }

    @PutMapping("/client-info")
    public Client updateClientInfo(
            @RequestParam(required = false, name = "bypassOptLock") boolean bypassOptLock,
            @RequestBody @Valid ClientInfoDTO clientInfoDTO,
            @ModelAttribute("sessionBeingUsed") SessionInformation sessionBeingUsed
    ) {
        Client currentClient = sessionBeingUsed.getClient();

        //Thread sleep is only for demonstrating OptLock exception handling
        if (sleepEnabled) {
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                throw new PublicException(ApplicationError.CS_CLIENT_NOT_FOUND);
            }
        }

        currentClient.setFirstName(clientInfoDTO.getFirstName());
        currentClient.setLastName(clientInfoDTO.getLastName());
        currentClient.setAddress(clientInfoDTO.getAddress());
        currentClient.setMobile(clientInfoDTO.getMobile());

        try {
            return clientRepository.save(currentClient);
        } catch (OptimisticLockingFailureException e) {
            if (!bypassOptLock) {
                throw new PublicException(ApplicationError.CS_OPT_LOCK_ERROR);
            }

            currentClient = clientRepository.findById(currentClient.getId()).orElseThrow(() ->
                    new PublicException(ApplicationError.CS_CLIENT_NOT_FOUND));

            currentClient.setFirstName(clientInfoDTO.getFirstName());
            currentClient.setLastName(clientInfoDTO.getLastName());
            currentClient.setAddress(clientInfoDTO.getAddress());
            currentClient.setMobile(clientInfoDTO.getMobile());

            return clientRepository.save(currentClient);
        }
    }

    @GetMapping("/client-info")
    public Client getClientInfo(
            @ModelAttribute("sessionBeingUsed") SessionInformation sessionBeingUsed
    ) {
        return sessionBeingUsed.getClient();
    }

}
