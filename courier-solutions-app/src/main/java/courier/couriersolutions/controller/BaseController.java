package courier.couriersolutions.controller;

import courier.couriersolutions.entities.SessionInformation;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.servlet.http.HttpServletRequest;

public interface BaseController {

    @ModelAttribute("sessionBeingUsed")
    default SessionInformation sessionBeingUsed(HttpServletRequest httpServletRequest) {
        return (SessionInformation) httpServletRequest.getAttribute("session");
    }

}
