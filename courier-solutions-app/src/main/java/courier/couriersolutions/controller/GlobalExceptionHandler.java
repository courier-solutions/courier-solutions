package courier.couriersolutions.controller;

import courier.couriersolutions.exceptions.HandlerNotFoundException;
import courier.couriersolutions.exceptions.NotAcceptableMediaTypeException;
import courier.couriersolutions.exceptions.PublicException;
import courier.couriersolutions.exceptions.error.ApplicationError;
import courier.couriersolutions.exceptions.error.RequestError;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.multipart.support.MissingServletRequestPartException;
import org.springframework.web.servlet.NoHandlerFoundException;

@RestControllerAdvice
public class GlobalExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @ExceptionHandler(value = {PublicException.class})
    public ResponseEntity<RequestError> publicExceptionHandler(PublicException e) {
        if (e.getHttpStatus().is4xxClientError()) {
            logger.warn("httpStatus={}, errorName={}, errorMessage={}, detailMessage={}",
                    e.getHttpStatus(), e.getErrorName(), e.getErrorMessage(), e.getMessage());
        } else {
            logger.error("httpStatus={}, errorName={}, errorMessage={}, detailMessage={}",
                    e.getHttpStatus(), e.getErrorName(), e.getErrorMessage(), e.getMessage());
            logger.error(e.getMessage(), e);
        }
        RequestError requestError = new RequestError();
        requestError.setHttpStatus(e.getHttpStatus().toString());
        requestError.setErrorName(e.getErrorName());
        requestError.setText(e.getErrorMessage());

        return new ResponseEntity<>(requestError, new HttpHeaders(),
                e.getHttpStatus() != null ? e.getHttpStatus() : HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = {Exception.class})
    public ResponseEntity<RequestError> anyExceptionHandler(Exception e) {
        return publicExceptionHandler(new PublicException(ApplicationError.CS_SYS_ERR, e));
    }

    @ExceptionHandler(value = {MethodArgumentTypeMismatchException.class})
    public ResponseEntity<RequestError> methodArgumentTypeMismatchExceptionHandler(MethodArgumentTypeMismatchException e) {
        return publicExceptionHandler(new PublicException(ApplicationError.CS_ARGUMENT_TYPE_MISMATCH_ERR, e));
    }

    @ExceptionHandler(value = {NoHandlerFoundException.class})
    public ResponseEntity<RequestError> noHandlerFoundExceptionHandler(NoHandlerFoundException e) {
        return publicExceptionHandler(new HandlerNotFoundException(e));
    }

    @ExceptionHandler(value = {HttpMessageNotReadableException.class, MissingServletRequestPartException.class})
    public ResponseEntity<RequestError> httpMessageNotReadableExceptionHandler(Exception e) {
        return publicExceptionHandler(new PublicException(ApplicationError.CS_MESSAGE_NOT_READABLE, e));
    }

    @ExceptionHandler(value = {HttpMediaTypeNotAcceptableException.class})
    public ResponseEntity<RequestError> httpMediaTypeNotAcceptableExceptionHandler(HttpMediaTypeNotAcceptableException e) {
        return publicExceptionHandler(new NotAcceptableMediaTypeException(e));
    }

    @ExceptionHandler(value = {HttpMediaTypeNotSupportedException.class})
    public ResponseEntity<RequestError> httpMediaTypeNotSupportedExceptionHandler(HttpMediaTypeNotSupportedException e) {
        return publicExceptionHandler(new PublicException(ApplicationError.CS_UNSUPPORTED_MEDIA_TYPE, e));
    }

    @ExceptionHandler(value = {MethodArgumentNotValidException.class})
    public ResponseEntity<RequestError> methodArgumentNotValidExceptionHandler(MethodArgumentNotValidException e) {
        return publicExceptionHandler(new PublicException(ApplicationError.CS_NOT_VALID_ARGUMENT, e));
    }

    @ExceptionHandler(value = {HttpRequestMethodNotSupportedException.class})
    public ResponseEntity<RequestError> httpRequestMethodNotSupportedExceptionHandler(HttpRequestMethodNotSupportedException e) {
        return publicExceptionHandler(new PublicException(ApplicationError.CS_UNSUPPORTED_HTTP_METHOD, e));
    }

    @ExceptionHandler(value = {MissingServletRequestParameterException.class})
    public ResponseEntity<RequestError> missingServletRequestParameterExceptionHandler(MissingServletRequestParameterException e) {
        return publicExceptionHandler(new PublicException(ApplicationError.CS_REQUEST_PARAMETER_MISSING, e));
    }

}
