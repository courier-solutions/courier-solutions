package courier.couriersolutions.utils;

import java.math.RoundingMode;
import java.text.DecimalFormat;

public class NumericInputUtils {

    private NumericInputUtils() {
        //To hide implicit public constructor
    }

    public static Float roundUpTo2Digits(Float number) {
        DecimalFormat formatter = new DecimalFormat();
        formatter.setMaximumFractionDigits(2);
        formatter.setRoundingMode(RoundingMode.UP);

        return Float.parseFloat(formatter.format(number).replace(',', '.'));
    }
}
