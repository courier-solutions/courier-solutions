package courier.couriersolutions.dto;

import courier.couriersolutions.utils.Password;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

public class LogInDTO {

    @Email
    @NotBlank
    private String username;

    @NotBlank
    @Password
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
