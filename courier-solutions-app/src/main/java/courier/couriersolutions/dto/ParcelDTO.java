package courier.couriersolutions.dto;

import courier.couriersolutions.constants.ParcelType;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class ParcelDTO {
    @NotNull
    private ParcelType type;

    @NotNull
    @Positive
    private Float length;

    @NotNull
    @Positive
    private Float width;

    @NotNull
    @Positive
    private Float height;

    @NotNull
    @Positive
    private Float weight;

    @NotNull
    @Positive
    private Integer quantity;

    public ParcelType getType() {
        return type;
    }

    public void setType(ParcelType type) {
        this.type = type;
    }

    public Float getLength() {
        return length;
    }

    public void setLength(Float length) {
        this.length = length;
    }

    public Float getWidth() {
        return width;
    }

    public void setWidth(Float width) {
        this.width = width;
    }

    public Float getHeight() {
        return height;
    }

    public void setHeight(Float height) {
        this.height = height;
    }

    public Float getWeight() {
        return weight;
    }

    public void setWeight(Float weight) {
        this.weight = weight;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
}
