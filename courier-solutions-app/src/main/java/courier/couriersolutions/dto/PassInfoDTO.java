package courier.couriersolutions.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import courier.couriersolutions.utils.Password;

import javax.validation.constraints.NotBlank;

public class PassInfoDTO {

    @NotBlank
    @JsonProperty("current_password")
    @Password
    private String currentPassword;

    @NotBlank
    @JsonProperty("new_password")
    @Password
    private String newPassword;

    @NotBlank
    @JsonProperty("repeat_new_password")
    @Password
    private String repeatNewPassword;

    public PassInfoDTO(String currentPassword, String newPassword, String repeatNewPassword) {
        this.currentPassword = currentPassword;
        this.newPassword = newPassword;
        this.repeatNewPassword = repeatNewPassword;
    }

    public PassInfoDTO() {
        //empty for auto init
    }

    public String getCurrentPassword() {
        return currentPassword;
    }

    public void setCurrentPassword(String currentPassword) {
        this.currentPassword = currentPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getRepeatNewPassword() {
        return repeatNewPassword;
    }

    public void setRepeatNewPassword(String repeatNewPassword) {
        this.repeatNewPassword = repeatNewPassword;
    }
}
