package courier.couriersolutions.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OrderWithPaymentDTO extends OrderDTO {

    @JsonProperty("payment_information")
    public PaymentInformationDTO getPaymentInformationDTO() {
        return paymentInformationDTO;
    }

    public void setPaymentInformationDTO(PaymentInformationDTO paymentInformationDTO) {
        this.paymentInformationDTO = paymentInformationDTO;
    }

    private PaymentInformationDTO paymentInformationDTO;

}
