package courier.couriersolutions.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

public class PaymentInformationDTO {

    @NotBlank
    @JsonProperty("card_owner")
    private String cardOwner;

    @Pattern(regexp = "[1-9][0-9]{15}")
    @JsonProperty("card_number")
    private String cardNumber;

    @Pattern(regexp = "[0-9]{3}")
    @JsonProperty("security_code")
    private String securityCode;

    @Pattern(regexp = "(0[1-9]|1[0-2])\\/[0-9]{2}")
    @JsonProperty("valid_thru")
    private String validThru;

    public String getCardOwner() {
        return cardOwner;
    }

    public void setCardOwner(String cardOwner) {
        this.cardOwner = cardOwner;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getSecurityCode() {
        return securityCode;
    }

    public void setSecurityCode(String securityCode) {
        this.securityCode = securityCode;
    }

    public String getValidThru() {
        return validThru;
    }

    public void setValidThru(String validThru) {
        this.validThru = validThru;
    }

}
