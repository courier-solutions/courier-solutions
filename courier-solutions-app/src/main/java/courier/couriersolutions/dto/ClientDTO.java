package courier.couriersolutions.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import courier.couriersolutions.utils.Password;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

public class ClientDTO {

    @Email
    @NotBlank
    private String email;

    @NotBlank
    @Password
    private String password;

    @NotBlank
    @JsonProperty("repeat_password")
    @Password
    private String repeatPassword;

    @NotBlank
    @JsonProperty("first_name")
    private String firstName;

    @NotBlank
    @JsonProperty("last_name")
    private String lastName;

    @NotBlank
    private String address;

    @NotBlank
    private String mobile;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRepeatPassword() {
        return repeatPassword;
    }

    public void setRepeatPassword(String repeatPassword) {
        this.repeatPassword = repeatPassword;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

}
