package courier.couriersolutions.dto;

public class LogInResponseDTO {

    private String status;

    public LogInResponseDTO() {
        //empty for auto init
    }

    public LogInResponseDTO(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
