package courier.couriersolutions.dto;

public class LogOutResponseDTO {

    private String status;

    public LogOutResponseDTO() {
        //empty for auto init
    }

    public LogOutResponseDTO(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
