package courier.couriersolutions.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import courier.couriersolutions.constants.DeliveryType;
import courier.couriersolutions.constants.PaymentType;

import javax.validation.Valid;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;

public class OrderDTO {
    @JsonProperty("parcels")
    @NotEmpty
    @Valid
    private List<ParcelDTO> parcelList;

    @JsonProperty("pickup_information")
    @NotNull
    @Valid
    private OrderInformationDTO pickupInfo;

    @JsonProperty("pickup_date")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @JsonSerialize(using = LocalDateSerializer.class)
    @FutureOrPresent
    private LocalDate pickupDate;

    @JsonProperty("delivery_information")
    @NotNull
    @Valid
    private OrderInformationDTO deliveryInfo;

    @JsonProperty("delivery_type")
    @NotNull
    private DeliveryType deliveryType;

    @JsonProperty("payment_type")
    @NotNull
    private PaymentType paymentType;

    @NotNull
    private String comment;

    public List<ParcelDTO> getParcelList() {
        return parcelList;
    }

    public void setParcelList(List<ParcelDTO> parcelList) {
        this.parcelList = parcelList;
    }

    public OrderInformationDTO getPickupInfo() {
        return pickupInfo;
    }

    public void setPickupInfo(OrderInformationDTO pickupInfo) {
        this.pickupInfo = pickupInfo;
    }

    public LocalDate getPickupDate() {
        return pickupDate;
    }

    public void setPickupDate(LocalDate pickupDate) {
        this.pickupDate = pickupDate;
    }

    public OrderInformationDTO getDeliveryInfo() {
        return deliveryInfo;
    }

    public void setDeliveryInfo(OrderInformationDTO deliveryInfo) {
        this.deliveryInfo = deliveryInfo;
    }

    public DeliveryType getDeliveryType() {
        return deliveryType;
    }

    public void setDeliveryType(DeliveryType deliveryType) {
        this.deliveryType = deliveryType;
    }

    public PaymentType getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(PaymentType paymentType) {
        this.paymentType = paymentType;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
