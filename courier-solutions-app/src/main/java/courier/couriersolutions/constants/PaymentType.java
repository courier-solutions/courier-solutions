package courier.couriersolutions.constants;

public enum PaymentType {
    DEBIT_CREDIT_CARD,
    CASH,
    BANK_PAYMENT;
}
