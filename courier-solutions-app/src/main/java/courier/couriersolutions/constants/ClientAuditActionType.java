package courier.couriersolutions.constants;

public enum ClientAuditActionType {
    ACCESS_API,
    SYSTEM
}
