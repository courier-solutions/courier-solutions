package courier.couriersolutions.constants;

public enum OrderStatus {
    VERIFIED,
    RECEIVED,
    ON_THE_WAY,
    DELIVERED,
    PICKUP,
    CANCELED
}
