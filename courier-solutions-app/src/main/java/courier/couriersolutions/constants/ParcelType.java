package courier.couriersolutions.constants;

public enum ParcelType {
    STANDARD,
    ANIMALS,
    DOCUMENTS,
    FRAGILE,
}
