package courier.couriersolutions.constants;

public enum DeliveryType {
    STANDARD_SHIPPING,
    FAST_SHIPPING,
}
