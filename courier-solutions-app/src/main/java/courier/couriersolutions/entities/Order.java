package courier.couriersolutions.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import courier.couriersolutions.constants.DeliveryType;
import courier.couriersolutions.constants.OrderStatus;
import courier.couriersolutions.constants.PaymentType;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.validator.internal.util.stereotypes.Lazy;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "orders")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Lazy
    @ManyToOne
    private Client client;

    @Column(name = "status", columnDefinition = "integer default 0")
    @Enumerated(value = EnumType.STRING)
    private OrderStatus status;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "fk_order")
    @JsonProperty("parcels")
    private List<Parcel> parcelList;

    @OneToOne(cascade = CascadeType.ALL)
    @JsonProperty("pickup_information")
    private OrderInformation pickupInfo;

    @Column(name = "pickup_date", nullable = false)
    @JsonProperty("pickup_date")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate pickupDate;

    @OneToOne(cascade = CascadeType.ALL)
    @JsonProperty("delivery_information")
    private OrderInformation deliveryInfo;

    @Column(name = "delivery_type", nullable = false)
    @JsonProperty("delivery_type")
    @Enumerated(value = EnumType.STRING)
    private DeliveryType deliveryType;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "payment_type", nullable = false)
    @JsonProperty("payment_type")
    private PaymentType paymentType;

    @Column(name = "cost", nullable = false)
    @NotNull
    @Positive
    private Float cost;

    @Column(name = "comment", nullable = false)
    private String comment;

    @CreationTimestamp
    @Column(name = "create_timestamp", nullable = false)
    @JsonProperty("create_timestamp")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime creationDateTime;

    @UpdateTimestamp
    @Column(name = "update_timestamp", nullable = false)
    @JsonProperty("update_timestamp")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime updateDateTime;

    public Order() {
        //Empty for auto init
    }

    public Order(
            Client client,
            List<Parcel> parcelList,
            OrderInformation pickupInfo,
            LocalDate pickupDate,
            OrderInformation deliveryInfo,
            DeliveryType deliveryType,
            String comment,
            PaymentType paymentType,
            Float cost
    ) {
        this.client = client;
        this.status = OrderStatus.VERIFIED;
        this.parcelList = parcelList;
        this.pickupInfo = pickupInfo;
        this.pickupDate = pickupDate;
        this.deliveryInfo = deliveryInfo;
        this.deliveryType = deliveryType;
        this.comment = comment;
        this.paymentType = paymentType;
        this.cost = cost;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public OrderStatus getStatus() {
        return status;
    }

    public void setStatus(OrderStatus status) {
        this.status = status;
    }

    public List<Parcel> getParcelList() {
        return parcelList;
    }

    public void setParcelList(List<Parcel> parcelList) {
        this.parcelList = parcelList;
    }

    public OrderInformation getPickupInfo() {
        return pickupInfo;
    }

    public void setPickupInfo(OrderInformation pickupInfo) {
        this.pickupInfo = pickupInfo;
    }

    public LocalDate getPickupDate() {
        return pickupDate;
    }

    public void setPickupDate(LocalDate pickupDate) {
        this.pickupDate = pickupDate;
    }

    public OrderInformation getDeliveryInfo() {
        return deliveryInfo;
    }

    public void setDeliveryInfo(OrderInformation deliveryInfo) {
        this.deliveryInfo = deliveryInfo;
    }

    public DeliveryType getDeliveryType() {
        return deliveryType;
    }

    public void setDeliveryType(DeliveryType deliveryType) {
        this.deliveryType = deliveryType;
    }

    public PaymentType getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(PaymentType paymentType) {
        this.paymentType = paymentType;
    }

    public Float getCost() {
        return cost;
    }

    public void setCost(Float cost) {
        this.cost = cost;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public LocalDateTime getCreationDateTime() {
        return creationDateTime;
    }

    public void setCreationDateTime(LocalDateTime creationDateTime) {
        this.creationDateTime = creationDateTime;
    }

    public LocalDateTime getUpdateDateTime() {
        return updateDateTime;
    }

    public void setUpdateDateTime(LocalDateTime updateDateTime) {
        this.updateDateTime = updateDateTime;
    }
}
