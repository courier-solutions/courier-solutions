package courier.couriersolutions.entities;

public class SessionInformation {

    private String sessKey;

    private Client client;

    public SessionInformation(String sessKey, Client client) {
        this.sessKey = sessKey;
        this.client = client;
    }

    public String getSessKey() {
        return sessKey;
    }

    public void setSessKey(String sessKey) {
        this.sessKey = sessKey;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }
}
