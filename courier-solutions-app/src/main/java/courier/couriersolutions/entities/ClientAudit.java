package courier.couriersolutions.entities;

import courier.couriersolutions.constants.ClientAuditActionType;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.Enumerated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.EnumType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name = "client_audit")
public class ClientAudit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ManyToOne
    private Client client;

    @Column(name = "action_type")
    @Enumerated(value = EnumType.STRING)
    private ClientAuditActionType actionType;

    @Column(name = "action")
    private String action;

    @CreationTimestamp
    @Column(name = "create_timestamp")
    private LocalDateTime createTimestamp;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public ClientAuditActionType getActionType() {
        return actionType;
    }

    public void setActionType(ClientAuditActionType actionType) {
        this.actionType = actionType;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public LocalDateTime getCreateTimestamp() {
        return createTimestamp;
    }

    public void setCreateTimestamp(LocalDateTime createTimestamp) {
        this.createTimestamp = createTimestamp;
    }

}
