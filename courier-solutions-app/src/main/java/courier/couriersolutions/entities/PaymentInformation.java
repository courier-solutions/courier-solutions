package courier.couriersolutions.entities;

import javax.persistence.Entity;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.Column;
import javax.persistence.OneToOne;
import javax.persistence.JoinColumn;
import javax.validation.constraints.Pattern;

@Entity
@Table(name = "payment_information")
public class PaymentInformation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "card_owner", nullable = false)
    private String cardOwner;

    @Pattern(regexp = "[1-9][0-9]{15}")
    @Column(name = "card_number", nullable = false)
    private String cardNumber;

    @Pattern(regexp = "[0-9]{3}")
    @Column(name = "security_code", nullable = false)
    private String securityCode;

    @Pattern(regexp = "(0[1-9]|1[0-2])\\/[0-9]{2}")
    @Column(name = "valid_thru", nullable = false)
    private String validThru;

    @OneToOne
    @JoinColumn(name = "order_id")
    private Order order;

    public PaymentInformation() {
        //Empty for auto init
    }

    public PaymentInformation(String cardNumber, String securityCode, String validThru) {
        this.cardNumber = cardNumber;
        this.securityCode = securityCode;
        this.validThru = validThru;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCardOwner() {
        return cardOwner;
    }

    public void setCardOwner(String cardOwner) {
        this.cardOwner = cardOwner;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getSecurityCode() {
        return securityCode;
    }

    public void setSecurityCode(String securityCode) {
        this.securityCode = securityCode;
    }

    public String getValidThru() {
        return validThru;
    }

    public void setValidThru(String validThru) {
        this.validThru = validThru;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

}
