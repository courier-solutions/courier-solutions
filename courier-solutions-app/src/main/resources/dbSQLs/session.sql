create table if not exists session
(
    id               bigserial    not null
        constraint session_pk
            primary key,
    session_key      varchar(256) not null,
    client_id        bigint       not null
        constraint client__fk
            references client,
    update_timestamp timestamp    not null,
    create_timestamp timestamp    not null
);

alter table session
    owner to testuser;

create unique index session_session_key_uindex
    on session (session_key);
