create table if not exists client_audit
(
    id               bigserial not null
        constraint client_audit_pk
            primary key,
    client_id        bigint    not null
        constraint client_audit_client_id_fk
            references client,
    action_type      varchar   not null,
    action           varchar   not null,
    create_timestamp timestamp not null
);

alter table client_audit
    owner to testuser;