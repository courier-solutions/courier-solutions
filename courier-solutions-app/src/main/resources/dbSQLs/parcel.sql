create table if not exists parcel
(
    id       bigserial not null
        constraint parcel_pkey
            primary key,
    height   real      not null,
    length   real      not null,
    quantity integer   not null,
    type     integer   not null,
    weight   real      not null,
    width    real      not null,
    fk_order bigint
        constraint fk9y2lq2bsqpf53ogvn6q8b30gq
            references orders
);

alter table parcel
    owner to testuser;
