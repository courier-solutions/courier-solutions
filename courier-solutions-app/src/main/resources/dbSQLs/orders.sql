create table if not exists orders
(
    id               bigserial    not null
        constraint orders_pkey
            primary key,
    comment          varchar(255) not null,
    create_timestamp timestamp    not null,
    delivery_type    varchar      not null,
    pickup_date      date         not null,
    status           varchar      not null,
    update_timestamp timestamp    not null,
    client_id        bigint       not null
        constraint fk17yo6gry2nuwg2erwhbaxqbs9
            references client
            on update cascade on delete restrict,
    delivery_info_id bigint       not null
        constraint fks5wte4vqu0b7sqc609njao94m
            references order_information
            on update cascade on delete restrict,
    payment_type     varchar      not null,
    pickup_info_id   bigint       not null
        constraint orders_order_information_id_fk
            references order_information
            on update cascade on delete restrict,
    cost             real         not null
);

alter table orders
    owner to testuser;