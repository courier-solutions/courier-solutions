create table if not exists client
(
    id               bigserial         not null
        constraint client_pkey
            primary key,
    address          varchar(255)      not null,
    created          timestamp         not null,
    email            varchar(255)      not null,
    first_name       varchar(255)      not null,
    last_name        varchar(255)      not null,
    mobile           varchar(255)      not null,
    password         varchar(255)      not null,
    last_updated     timestamp         not null,
    opt_lock_version integer default 0 not null
);

alter table client
    owner to testuser;

create unique index client_email_uindex
    on client (email);