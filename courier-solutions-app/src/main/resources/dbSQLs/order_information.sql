create table if not exists order_information
(
    id           bigserial    not null
        constraint order_information_pkey
            primary key,
    address      varchar(255) not null,
    city         varchar(255) not null,
    email        varchar(255) not null,
    full_name    varchar(255) not null,
    phone_number varchar(255) not null,
    postal_code  varchar(255) not null
);

alter table order_information
    owner to testuser;
