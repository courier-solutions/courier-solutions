create table if not exists payment_information
(
    id            bigserial   not null
        constraint payment_information_pk
            primary key,
    card_number   varchar(16)  not null,
    card_owner    varchar(255) not null,
    security_code varchar(3)   not null,
    valid_thru    varchar(5)   not null,
    order_id      bigint       not null
);

alter table payment_information
    owner to testuser;