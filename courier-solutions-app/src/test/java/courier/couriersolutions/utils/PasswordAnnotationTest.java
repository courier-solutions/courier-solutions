package courier.couriersolutions.utils;

import courier.couriersolutions.repository.ClientAuditRepository;
import courier.couriersolutions.repository.ClientRepository;
import courier.couriersolutions.repository.OrderRepository;
import courier.couriersolutions.repository.PaymentInformationRepository;
import courier.couriersolutions.repository.SessionRepository;
import javax.validation.ConstraintValidatorContext;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;

@WebMvcTest(PasswordValidator.class)
class PasswordAnnotationTest {

    @MockBean
    private ConstraintValidatorContext context;

    @Autowired
    private PasswordValidator passwordValidator;

    @MockBean
    private SessionRepository sessionRepository;

    @MockBean
    private ClientRepository clientRepository;

    @MockBean
    private OrderRepository orderRepository;

    @MockBean
    private PaymentInformationRepository paymentInformationRepository;

    @MockBean
    private ClientAuditRepository clientAuditRepository;

    @Test
    void passwordAnnotationTest() {
        String validPass = "asdASD12$";
        String invalidPass = "test123";

        Assertions.assertFalse(passwordValidator.isValid(invalidPass, context));
        Assertions.assertTrue(passwordValidator.isValid(validPass, context));

    }

}
