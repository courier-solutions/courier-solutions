package courier.couriersolutions.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class NumericInputUtilsTest {

    @Test
    void roundUpTo2DigitsTest() {
        Float test1 = NumericInputUtils.roundUpTo2Digits(1.001f);
        Float test2 = NumericInputUtils.roundUpTo2Digits(1.999f);
        Float test3 = NumericInputUtils.roundUpTo2Digits(1.0005f);
        Float test4 = NumericInputUtils.roundUpTo2Digits(1.000f);

        Assertions.assertEquals(1.01f, test1);
        Assertions.assertEquals(2f, test2);
        Assertions.assertEquals(1.01f, test3);
        Assertions.assertEquals(1f, test4);
    }

}
