package courier.couriersolutions.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import courier.couriersolutions.CSHttpHeaders;
import courier.couriersolutions.dto.LogInResponseDTO;
import courier.couriersolutions.entities.Client;
import courier.couriersolutions.entities.Session;
import courier.couriersolutions.exceptions.error.ApplicationError;
import courier.couriersolutions.exceptions.error.RequestError;
import courier.couriersolutions.repository.ClientRepository;
import courier.couriersolutions.repository.ClientAuditRepository;
import courier.couriersolutions.repository.OrderRepository;
import courier.couriersolutions.repository.SessionRepository;
import courier.couriersolutions.repository.PaymentInformationRepository;
import courier.couriersolutions.services.ClientService;
import courier.couriersolutions.services.OrderCostService;
import courier.couriersolutions.services.SessionHandlingService;
import javassist.NotFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.restdocs.payload.JsonFieldType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.util.StringUtils;

import javax.servlet.http.Cookie;
import java.time.LocalDateTime;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.restdocs.headers.HeaderDocumentation.headerWithName;
import static org.springframework.restdocs.headers.HeaderDocumentation.requestHeaders;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessRequest;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessResponse;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.removeHeaders;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(LogOutController.class)
@AutoConfigureRestDocs(outputDir = "target/snippets")
@AutoConfigureMockMvc
class LogOutControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private SessionRepository sessionRepository;

    @MockBean
    private ClientRepository clientRepository;

    @MockBean
    private OrderRepository orderRepository;

    @MockBean
    private ClientAuditRepository clientAuditRepository;

    @MockBean
    private SessionHandlingService sessionHandlingService;

    @MockBean
    private PaymentInformationRepository paymentInformationRepository;

    @Autowired
    private ClientService clientService;

    @MockBean
    private OrderCostService orderCostService;

    @Test
    void testLogoutUser() throws Exception {
        when(sessionHandlingService.getActiveSession(any()))
                .thenAnswer(invocation -> {
                    Session session = new Session();
                    session.setSessionKey(UUID.randomUUID().toString());
                    session.setClient(createMockClient());
                    return session;
                });

        MvcResult mvcResult = mockMvc
                .perform(
                        MockMvcRequestBuilders.post("/api/logout")
                                .with(httpBasic("user", "pass"))
                                .with(csrf())
                                .cookie(new Cookie(CSHttpHeaders.SESS_KEY, "TestSessKey"))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(
                        document(
                                "logoutUser",
                                preprocessRequest(removeHeaders("Content-Length", "Host"), prettyPrint()),
                                preprocessResponse(prettyPrint()),
                                requestHeaders(
                                        headerWithName("Authorization").description("Basic authentication header")
                                ),
                                responseFields(
                                        fieldWithPath("status").type(JsonFieldType.STRING).description("Logout status")
                                )
                        )
                )
                .andDo(print())
                .andReturn();
        LogInResponseDTO responseDTO = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), LogInResponseDTO.class);

        Assertions.assertNotNull(responseDTO);
        Assertions.assertFalse(StringUtils.isEmpty(responseDTO.getStatus()));
    }

    @Test
    void testLogoutUserAlreadyDeleted() throws Exception {
        when(sessionHandlingService.getActiveSession(any()))
                .thenAnswer(invocation -> {
                    Session session = new Session();
                    session.setSessionKey(UUID.randomUUID().toString());
                    session.setClient(createMockClient());
                    return session;
                });

        doThrow(new NotFoundException("Test"))
                .when(sessionHandlingService).disableSessionForClient(any());

        MvcResult mvcResult = mockMvc
                .perform(
                        MockMvcRequestBuilders.post("/api/logout")
                                .with(httpBasic("user", "pass"))
                                .with(csrf())
                                .cookie(new Cookie(CSHttpHeaders.SESS_KEY, "TestSessKey"))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();
        RequestError error = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), RequestError.class);

        Assertions.assertEquals(ApplicationError.CS_SESSION_ALREADY_DELETED.getErrorName(), error.getErrorName());
        Assertions.assertEquals(ApplicationError.CS_SESSION_ALREADY_DELETED.getHttpStatus().toString(), error.getHttpStatus());
    }

    private Client createMockClient() {
        Client client = new Client();
        client.setId(1L);
        client.setFirstName("TestFirstName");
        client.setLastName("TestLastName");
        client.setAddress("TestAddress");
        client.setMobile("TestMobile");
        client.setEmail("Test@email");
        client.setPassword("testP@ssword1");
        client.setCreationDateTime(LocalDateTime.now());
        client.setUpdateDateTime(LocalDateTime.now());

        return client;
    }

}
