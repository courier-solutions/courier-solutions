package courier.couriersolutions.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import courier.couriersolutions.CSHttpHeaders;
import courier.couriersolutions.constants.DeliveryType;
import courier.couriersolutions.constants.OrderStatus;
import courier.couriersolutions.constants.ParcelType;
import courier.couriersolutions.constants.PaymentType;
import courier.couriersolutions.dto.OrderDTO;
import courier.couriersolutions.dto.OrderInformationDTO;
import courier.couriersolutions.dto.OrderWithPaymentDTO;
import courier.couriersolutions.dto.PaymentInformationDTO;
import courier.couriersolutions.dto.ParcelDTO;
import courier.couriersolutions.entities.Client;
import courier.couriersolutions.entities.Order;
import courier.couriersolutions.entities.OrderInformation;
import courier.couriersolutions.entities.Parcel;
import courier.couriersolutions.entities.Session;
import courier.couriersolutions.exceptions.EmptyPaymentInformation;
import courier.couriersolutions.exceptions.error.ApplicationError;
import courier.couriersolutions.exceptions.error.RequestError;
import courier.couriersolutions.repository.ClientRepository;
import courier.couriersolutions.repository.OrderRepository;
import courier.couriersolutions.repository.PaymentInformationRepository;
import courier.couriersolutions.repository.SessionRepository;
import courier.couriersolutions.repository.ClientAuditRepository;
import courier.couriersolutions.services.OrderCostService;
import courier.couriersolutions.services.OrderService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders;
import org.springframework.restdocs.mockmvc.RestDocumentationResultHandler;
import org.springframework.restdocs.payload.JsonFieldType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import javax.servlet.http.Cookie;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.restdocs.headers.HeaderDocumentation.headerWithName;
import static org.springframework.restdocs.headers.HeaderDocumentation.requestHeaders;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessRequest;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessResponse;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.removeHeaders;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.requestFields;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(OrderController.class)
@AutoConfigureRestDocs(outputDir = "target/snippets")
@AutoConfigureMockMvc
class OrderControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    @MockBean
    private SessionRepository sessionRepository;

    @MockBean
    private ClientRepository clientRepository;

    @MockBean
    private OrderRepository orderRepository;

    @MockBean
    private PaymentInformationRepository paymentInformationRepository;

    @MockBean
    private ClientAuditRepository clientAuditRepository;

    @MockBean
    private OrderService orderService;

    @MockBean
    private OrderCostService orderCostService;

    @Test
    void testCreateOrderEndpointSuccess() throws Exception {
        when(sessionRepository.findBySessionKey(any()))
                .thenAnswer(invocation -> {
                    Session session = new Session();
                    session.setSessionKey(UUID.randomUUID().toString());
                    session.setClient(new Client());

                    return Optional.of(session);
                });

        when(orderService.isValidPickupInformation(any(), any()))
                .thenAnswer(invocation -> true);

        when(orderService.saveOrder(any(), any()))
                .thenAnswer(invocation -> getOrderEntity());

        OrderWithPaymentDTO newOrder = getOrderWithPaymentDTO(PaymentType.DEBIT_CREDIT_CARD);

        MvcResult mvcResult = mockMvc.perform(post("/api/orders")
                .with(httpBasic("user", "pass")).with(csrf())
                .cookie(new Cookie(CSHttpHeaders.SESS_KEY, "TestSessKey"))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(newOrder)))
                .andExpect(status().isOk())
                .andDo(documentEndpoint())
                .andReturn();

        String response = mvcResult.getResponse().getContentAsString();
        Order createdOrder = mapper.readValue(response, Order.class);

        Assertions.assertNotNull(createdOrder);
    }

    @Test
    void testCreateOrderEndpointNotValidPickupInformation() throws Exception {
        when(sessionRepository.findBySessionKey(any()))
                .thenAnswer(invocation -> {
                    Session session = new Session();
                    session.setSessionKey(UUID.randomUUID().toString());
                    session.setClient(new Client());

                    return Optional.of(session);
                });

        when(orderService.isValidPickupInformation(any(), any()))
                .thenAnswer(invocation -> false);

        OrderWithPaymentDTO newOrder = getOrderWithPaymentDTO(PaymentType.DEBIT_CREDIT_CARD);

        MvcResult mvcResult = mockMvc.perform(post("/api/orders")
                .with(httpBasic("user", "pass")).with(csrf())
                .cookie(new Cookie(CSHttpHeaders.SESS_KEY, "TestSessKey"))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(newOrder)))
                .andExpect(status().isBadRequest())
                .andReturn();

        RequestError error = mapper.readValue(mvcResult.getResponse().getContentAsString(), RequestError.class);

        Assertions.assertEquals(ApplicationError.CS_NOT_VALID_PICKUP_INFORMATION.getErrorName(), error.getErrorName());
        Assertions.assertEquals(ApplicationError.CS_NOT_VALID_PICKUP_INFORMATION.getHttpStatus().toString(), error.getHttpStatus());
    }

    @Test
    void testCreateOrderEndpointMissingPaymentInformationException() throws Exception {
        when(sessionRepository.findBySessionKey(any()))
                .thenAnswer(invocation -> {
                    Session session = new Session();
                    session.setSessionKey(UUID.randomUUID().toString());
                    session.setClient(new Client());

                    return Optional.of(session);
                });

        when(orderService.isValidPickupInformation(any(), any()))
                .thenAnswer(invocation -> true);

        when(orderService.saveOrder(any(), any()))
                .thenThrow(new EmptyPaymentInformation());

        OrderWithPaymentDTO newOrder = getOrderWithPaymentDTO(PaymentType.DEBIT_CREDIT_CARD);

        MvcResult mvcResult = mockMvc.perform(post("/api/orders")
                .with(httpBasic("user", "pass")).with(csrf())
                .cookie(new Cookie(CSHttpHeaders.SESS_KEY, "TestSessKey"))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(newOrder)))
                .andExpect(status().isBadRequest())
                .andReturn();

        RequestError error = mapper.readValue(mvcResult.getResponse().getContentAsString(), RequestError.class);

        Assertions.assertEquals(ApplicationError.CS_MISSING_PAYMENT_INFORMATION.getErrorName(), error.getErrorName());
        Assertions.assertEquals(ApplicationError.CS_MISSING_PAYMENT_INFORMATION.getHttpStatus().toString(), error.getHttpStatus());
    }

    @Test
    void testGetOrderEndpointSuccess() throws Exception {
        when(sessionRepository.findBySessionKey(any()))
                .thenAnswer(invocation -> {
                    Session session = new Session();
                    session.setSessionKey(UUID.randomUUID().toString());
                    session.setClient(new Client());

                    return Optional.of(session);
                });

        when(orderRepository.findAllByClientId(any()))
                .thenAnswer(invocation -> Collections.singletonList(getOrderEntity()));

        MvcResult mvcResult = mockMvc.perform(get("/api/orders")
                .with(httpBasic("user", "pass")).with(csrf())
                .cookie(new Cookie(CSHttpHeaders.SESS_KEY, "TestSessKey"))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(getDocumentEndpoint())
                .andReturn();

        String response = mvcResult.getResponse().getContentAsString();
        List<Order> createdOrders = mapper.readValue(response, List.class);

        Assertions.assertNotNull(createdOrders);
    }

    @Test
    void testGetOrderNullEndpointSuccess() throws Exception {
        when(sessionRepository.findBySessionKey(any()))
                .thenAnswer(invocation -> {
                    Session session = new Session();
                    session.setSessionKey(UUID.randomUUID().toString());
                    session.setClient(new Client());

                    return Optional.of(session);
                });

        when(orderRepository.findAllByClientId(any()))
                .thenAnswer(invocation -> new ArrayList<>());

        MvcResult mvcResult = mockMvc.perform(get("/api/orders")
                .with(httpBasic("user", "pass")).with(csrf())
                .cookie(new Cookie(CSHttpHeaders.SESS_KEY, "TestSessKey"))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        String response = mvcResult.getResponse().getContentAsString();
        List<Order> createdOrders = mapper.readValue(response, List.class);

        Assertions.assertNotNull(createdOrders);
    }

    @Test
    void testCancelOrderEndpointSuccess() throws Exception {
        when(sessionRepository.findBySessionKey(any()))
                .thenAnswer(invocation -> {
                    Session session = new Session();
                    session.setSessionKey(UUID.randomUUID().toString());
                    session.setClient(new Client());

                    return Optional.of(session);
                });

        when(orderRepository.findByIdAndClientId(any(), any()))
                .thenAnswer(invocation -> Optional.of(getOrderEntity()));

        when(orderRepository.save(any()))
                .thenAnswer(invocation -> invocation.getArgument(0));

        MvcResult mvcResult = mockMvc.perform(RestDocumentationRequestBuilders.put("/api/orders/cancel/{id}", 1L)
                .with(httpBasic("user", "pass")).with(csrf())
                .cookie(new Cookie(CSHttpHeaders.SESS_KEY, "TestSessKey"))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(documentEndpointCancelOrder())
                .andReturn();

        String response = mvcResult.getResponse().getContentAsString();
        Order order = mapper.readValue(response, Order.class);

        Assertions.assertNotNull(order);
        Assertions.assertEquals(OrderStatus.CANCELED, order.getStatus());
    }

    @Test
    void testCancelOrderEndpointOrderNotFound() throws Exception {
        when(sessionRepository.findBySessionKey(any()))
                .thenAnswer(invocation -> {
                    Session session = new Session();
                    session.setSessionKey(UUID.randomUUID().toString());
                    session.setClient(new Client());

                    return Optional.of(session);
                });

        when(orderRepository.findByIdAndClientId(any(), any()))
                .thenAnswer(invocation -> Optional.empty());

        MvcResult mvcResult = mockMvc.perform(RestDocumentationRequestBuilders.put("/api/orders/cancel/{id}", 1L)
                .with(httpBasic("user", "pass")).with(csrf())
                .cookie(new Cookie(CSHttpHeaders.SESS_KEY, "TestSessKey"))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden())
                .andReturn();

        RequestError error = mapper.readValue(mvcResult.getResponse().getContentAsString(), RequestError.class);

        Assertions.assertEquals(ApplicationError.CS_ACCESS_DENIED.getErrorName(), error.getErrorName());
        Assertions.assertEquals(ApplicationError.CS_ACCESS_DENIED.getHttpStatus().toString(), error.getHttpStatus());
    }
    
    @Test
    void testCancelOrderEndpointOrderWrongStatus() throws Exception {
        when(sessionRepository.findBySessionKey(any()))
                .thenAnswer(invocation -> {
                    Session session = new Session();
                    session.setSessionKey(UUID.randomUUID().toString());
                    session.setClient(new Client());

                    return Optional.of(session);
                });

        when(orderRepository.findByIdAndClientId(any(), any()))
                .thenAnswer(invocation -> {
                    Order order = getOrderEntity();
                    order.setStatus(OrderStatus.CANCELED);
                    return Optional.of(order);
                });

        MvcResult mvcResult = mockMvc.perform(RestDocumentationRequestBuilders.put("/api/orders/cancel/{id}", 1L)
                .with(httpBasic("user", "pass")).with(csrf())
                .cookie(new Cookie(CSHttpHeaders.SESS_KEY, "TestSessKey"))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();

        RequestError error = mapper.readValue(mvcResult.getResponse().getContentAsString(), RequestError.class);

        Assertions.assertEquals(ApplicationError.CS_ORDER_CANCEL_NOT_ALLOWED.getErrorName(), error.getErrorName());
        Assertions.assertEquals(ApplicationError.CS_ORDER_CANCEL_NOT_ALLOWED.getHttpStatus().toString(), error.getHttpStatus());
    }

    @Test
    void testEditOrderEndpointSuccess() throws Exception {
        when(sessionRepository.findBySessionKey(any()))
                .thenAnswer(invocation -> {
                    Session session = new Session();
                    session.setSessionKey(UUID.randomUUID().toString());
                    session.setClient(new Client());

                    return Optional.of(session);
                });

        when(orderRepository.findByIdAndClientId(any(), any()))
                .thenAnswer(invocation -> Optional.of(getOrderEntity()));

        when(orderService.isValidPickupInformation(any(), any()))
                .thenAnswer(invocation -> true);

        when(orderRepository.save(any()))
                .thenAnswer(invocation -> invocation.getArgument(0));

        String expectedComment = "Edited.";
        OrderDTO editedOrderDTO = getOrderDTO();
        editedOrderDTO.setComment(expectedComment);

        MvcResult mvcResult = mockMvc.perform(RestDocumentationRequestBuilders.put("/api/orders/{id}", 1L)
                .with(httpBasic("user", "pass")).with(csrf())
                .cookie(new Cookie(CSHttpHeaders.SESS_KEY, "TestSessKey"))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(editedOrderDTO)))
                .andExpect(status().isOk())
                .andDo(documentEndpointEditOrder())
                .andReturn();

        String response = mvcResult.getResponse().getContentAsString();
        Order editedOrder = mapper.readValue(response, Order.class);

        Assertions.assertNotNull(editedOrder);
        Assertions.assertEquals(expectedComment, editedOrder.getComment());
    }

    @Test
    void testEditOrderEndpointOrderNotFound() throws Exception {
        when(sessionRepository.findBySessionKey(any()))
                .thenAnswer(invocation -> {
                    Session session = new Session();
                    session.setSessionKey(UUID.randomUUID().toString());
                    session.setClient(new Client());

                    return Optional.of(session);
                });

        when(orderRepository.findByIdAndClientId(any(), any()))
                .thenAnswer(invocation -> Optional.empty());

        MvcResult mvcResult = mockMvc.perform(RestDocumentationRequestBuilders.put("/api/orders/{id}", 1L)
                .with(httpBasic("user", "pass")).with(csrf())
                .cookie(new Cookie(CSHttpHeaders.SESS_KEY, "TestSessKey"))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(getOrderDTO())))
                .andExpect(status().isForbidden())
                .andReturn();

        RequestError error = mapper.readValue(mvcResult.getResponse().getContentAsString(), RequestError.class);

        Assertions.assertEquals(ApplicationError.CS_ACCESS_DENIED.getErrorName(), error.getErrorName());
        Assertions.assertEquals(ApplicationError.CS_ACCESS_DENIED.getHttpStatus().toString(), error.getHttpStatus());
    }

    @Test
    void testEditOrderEndpointOrderWrongStatus() throws Exception {
        when(sessionRepository.findBySessionKey(any()))
                .thenAnswer(invocation -> {
                    Session session = new Session();
                    session.setSessionKey(UUID.randomUUID().toString());
                    session.setClient(new Client());

                    return Optional.of(session);
                });

        when(orderRepository.findByIdAndClientId(any(), any()))
                .thenAnswer(invocation -> {
                    Order order = getOrderEntity();
                    order.setStatus(OrderStatus.CANCELED);
                    return Optional.of(order);
                });

        MvcResult mvcResult = mockMvc.perform(RestDocumentationRequestBuilders.put("/api/orders/{id}", 1L)
                .with(httpBasic("user", "pass")).with(csrf())
                .cookie(new Cookie(CSHttpHeaders.SESS_KEY, "TestSessKey"))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(getOrderDTO())))
                .andExpect(status().isBadRequest())
                .andReturn();

        RequestError error = mapper.readValue(mvcResult.getResponse().getContentAsString(), RequestError.class);

        Assertions.assertEquals(ApplicationError.CS_ORDER_EDIT_NOT_ALLOWED.getErrorName(), error.getErrorName());
        Assertions.assertEquals(ApplicationError.CS_ORDER_EDIT_NOT_ALLOWED.getHttpStatus().toString(), error.getHttpStatus());
    }

    @Test
    void testEditOrderEndpointNotValidPickupInformation() throws Exception {
        when(sessionRepository.findBySessionKey(any()))
                .thenAnswer(invocation -> {
                    Session session = new Session();
                    session.setSessionKey(UUID.randomUUID().toString());
                    session.setClient(new Client());

                    return Optional.of(session);
                });

        when(orderService.isValidPickupInformation(any(), any()))
                .thenAnswer(invocation -> false);

        when(orderRepository.findByIdAndClientId(any(), any()))
                .thenAnswer(invocation -> Optional.of(getOrderEntity()));

        MvcResult mvcResult = mockMvc.perform(RestDocumentationRequestBuilders.put("/api/orders/{id}", 1L)
                .with(httpBasic("user", "pass")).with(csrf())
                .cookie(new Cookie(CSHttpHeaders.SESS_KEY, "TestSessKey"))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(getOrderDTO())))
                .andExpect(status().isBadRequest())
                .andReturn();

        RequestError error = mapper.readValue(mvcResult.getResponse().getContentAsString(), RequestError.class);

        Assertions.assertEquals(ApplicationError.CS_NOT_VALID_PICKUP_INFORMATION.getErrorName(), error.getErrorName());
        Assertions.assertEquals(ApplicationError.CS_NOT_VALID_PICKUP_INFORMATION.getHttpStatus().toString(), error.getHttpStatus());
    }

    private Order getOrderEntity() {
        Parcel newParcel = new Parcel(ParcelType.DOCUMENTS, 1.01f, 2f, 1.01f, 1f, 1);
        newParcel.setId(1L);

        OrderInformation newPickupInfo = new OrderInformation(
                "Test Test",
                "test@test.com",
                "Test City",
                "Test st. 0",
                "00000",
                "+37060000000"
        );
        newPickupInfo.setId(1L);

        OrderInformation newDeliveryInfo = new OrderInformation(
                "Test Test",
                "test@test.com",
                "Test City",
                "Test st. 0",
                "00000",
                "+37060000000"
        );
        newDeliveryInfo.setId(2L);

        Client client = new Client();
        client.setId(1L);
        client.setEmail("test@gmail.com");
        client.setPassword("NiekasNesuzinos123!");
        client.setFirstName("Testas");
        client.setLastName("Netestas");
        client.setAddress("Nesakysiu niekad street");
        client.setMobile("+37025252525");
        client.setCreationDateTime(LocalDateTime.now());
        client.setUpdateDateTime(LocalDateTime.now());

        Order createdOrder = new Order(
                client,
                List.of(newParcel),
                newPickupInfo,
                LocalDate.now(),
                newDeliveryInfo,
                DeliveryType.STANDARD_SHIPPING,
                "",
                PaymentType.DEBIT_CREDIT_CARD,
                0f
        );
        createdOrder.setId(1L);
        createdOrder.setCreationDateTime(LocalDateTime.now());
        createdOrder.setUpdateDateTime(LocalDateTime.now());

        return createdOrder;
    }

    private OrderDTO getOrderDTO() {
        ParcelDTO newParcel = new ParcelDTO();
        newParcel.setType(ParcelType.DOCUMENTS);
        newParcel.setLength(1.001f);
        newParcel.setWidth(1.999f);
        newParcel.setHeight(1.0009f);
        newParcel.setWeight(1.000f);
        newParcel.setQuantity(1);

        OrderInformationDTO newPickupInfo = new OrderInformationDTO();
        newPickupInfo.setName("Test Test");
        newPickupInfo.setEmail("test@test.com");
        newPickupInfo.setCity("Test City");
        newPickupInfo.setAddress("Test st. 0");
        newPickupInfo.setPostalCode("00000");
        newPickupInfo.setPhoneNumber("+37060000000");

        OrderInformationDTO newDeliveryInfo = new OrderInformationDTO();
        newDeliveryInfo.setName("Test Test");
        newDeliveryInfo.setEmail("test@test.com");
        newDeliveryInfo.setCity("Test City");
        newDeliveryInfo.setAddress("Test st. 0");
        newDeliveryInfo.setPostalCode("00000");
        newDeliveryInfo.setPhoneNumber("+37060000000");

        OrderDTO newOrder = new OrderDTO();
        newOrder.setParcelList(List.of(newParcel));
        newOrder.setPickupInfo(newPickupInfo);
        newOrder.setPickupDate(LocalDate.now());
        newOrder.setDeliveryInfo(newDeliveryInfo);
        newOrder.setDeliveryType(DeliveryType.STANDARD_SHIPPING);
        newOrder.setComment("");
        newOrder.setPaymentType(PaymentType.CASH);

        return newOrder;
    }

    private OrderWithPaymentDTO getOrderWithPaymentDTO(PaymentType paymentType) {
        ParcelDTO newParcel = new ParcelDTO();
        newParcel.setType(ParcelType.DOCUMENTS);
        newParcel.setLength(1.001f);
        newParcel.setWidth(1.999f);
        newParcel.setHeight(1.0009f);
        newParcel.setWeight(1.000f);
        newParcel.setQuantity(1);

        OrderInformationDTO newPickupInfo = new OrderInformationDTO();
        newPickupInfo.setName("Test Test");
        newPickupInfo.setEmail("test@test.com");
        newPickupInfo.setCity("Test City");
        newPickupInfo.setAddress("Test st. 0");
        newPickupInfo.setPostalCode("00000");
        newPickupInfo.setPhoneNumber("+37060000000");

        OrderInformationDTO newDeliveryInfo = new OrderInformationDTO();
        newDeliveryInfo.setName("Test Test");
        newDeliveryInfo.setEmail("test@test.com");
        newDeliveryInfo.setCity("Test City");
        newDeliveryInfo.setAddress("Test st. 0");
        newDeliveryInfo.setPostalCode("00000");
        newDeliveryInfo.setPhoneNumber("+37060000000");

        PaymentInformationDTO paymentInformationDTO = new PaymentInformationDTO();
        paymentInformationDTO.setCardOwner("test test");
        paymentInformationDTO.setCardNumber("1234567890123456");
        paymentInformationDTO.setSecurityCode("111");
        paymentInformationDTO.setValidThru("02/21");
        paymentInformationDTO.setValidThru("02/21");

        OrderWithPaymentDTO newOrder = new OrderWithPaymentDTO();
        newOrder.setParcelList(List.of(newParcel));
        newOrder.setPickupInfo(newPickupInfo);
        newOrder.setPickupDate(LocalDate.now());
        newOrder.setDeliveryInfo(newDeliveryInfo);
        newOrder.setDeliveryType(DeliveryType.STANDARD_SHIPPING);
        newOrder.setComment("");
        newOrder.setPaymentType(paymentType);
        newOrder.setPaymentInformationDTO(paymentInformationDTO);

        return newOrder;
    }

    private RestDocumentationResultHandler documentEndpoint() {
        return document(
                "createOrder",
                preprocessRequest(removeHeaders("Content-Length", "Host"), prettyPrint()),
                preprocessResponse(prettyPrint()),
                requestHeaders(
                        headerWithName("Authorization").description("Basic auth credentials")
                ),
                requestFields(
                        fieldWithPath("parcels").type(JsonFieldType.ARRAY).description("An array of parcel objects"),
                        fieldWithPath("parcels[].type").type(JsonFieldType.STRING).description("The type of parcel"),
                        fieldWithPath("parcels[].length").type(JsonFieldType.NUMBER).description("The length of parcel"),
                        fieldWithPath("parcels[].width").type(JsonFieldType.NUMBER).description("The width of parcel"),
                        fieldWithPath("parcels[].height").type(JsonFieldType.NUMBER).description("The height of parcel"),
                        fieldWithPath("parcels[].weight").type(JsonFieldType.NUMBER).description("The weight of parcel"),
                        fieldWithPath("parcels[].quantity").type(JsonFieldType.NUMBER).description("The quantity of parcel"),

                        fieldWithPath("pickup_information").type(JsonFieldType.OBJECT).description("An object with pickup information"),
                        fieldWithPath("pickup_information.name").type(JsonFieldType.STRING).description("The name of the client"),
                        fieldWithPath("pickup_information.email").type(JsonFieldType.STRING).description("The email of the client"),
                        fieldWithPath("pickup_information.city").type(JsonFieldType.STRING).description("The city of the client"),
                        fieldWithPath("pickup_information.address").type(JsonFieldType.STRING).description("The address of the client"),
                        fieldWithPath("pickup_information.postal_code").type(JsonFieldType.STRING).description("The postal code of the client"),
                        fieldWithPath("pickup_information.phone_number").type(JsonFieldType.STRING).description("The phone number of the client"),
                        fieldWithPath("pickup_date").type(JsonFieldType.STRING).description("The order pickup date"),

                        fieldWithPath("payment_information").type(JsonFieldType.OBJECT).description("Payment information object"),
                        fieldWithPath("payment_information.card_owner").type(JsonFieldType.STRING).description("Debit Credit card owner full name (If not card payment sent NULL value)"),
                        fieldWithPath("payment_information.card_number").type(JsonFieldType.STRING).description("Debit Credit card number (If not card payment sent NULL value)"),
                        fieldWithPath("payment_information.security_code").type(JsonFieldType.STRING).description("Debit Credit card security code (If not card payment sent NULL value)"),
                        fieldWithPath("payment_information.valid_thru").type(JsonFieldType.STRING).description("Debit Credit card valid thru value (If not card payment sent NULL value)"),

                        fieldWithPath("delivery_information").type(JsonFieldType.OBJECT).description("An object with delivery information"),
                        fieldWithPath("delivery_information.name").type(JsonFieldType.STRING).description("The name of the recipient"),
                        fieldWithPath("delivery_information.email").type(JsonFieldType.STRING).description("The email of the recipient"),
                        fieldWithPath("delivery_information.city").type(JsonFieldType.STRING).description("The city of the recipient"),
                        fieldWithPath("delivery_information.address").type(JsonFieldType.STRING).description("The address of the recipient"),
                        fieldWithPath("delivery_information.postal_code").type(JsonFieldType.STRING).description("The postal code of the recipient"),
                        fieldWithPath("delivery_information.phone_number").type(JsonFieldType.STRING).description("The phone number of the recipient"),
                        fieldWithPath("delivery_type").type(JsonFieldType.STRING).description("The type of delivery"),

                        fieldWithPath("comment").type(JsonFieldType.STRING).description("The additional comments of order"),
                        fieldWithPath("payment_type").type(JsonFieldType.STRING).description("Order payment type")
                ),
                responseFields(
                        fieldWithPath("id").type(JsonFieldType.NUMBER).description("The id of order object"),
                        fieldWithPath("status").type(JsonFieldType.STRING).description("The status of order object"),
                        fieldWithPath("cost").type(JsonFieldType.NUMBER).description("The cost of order object"),
                        fieldWithPath("create_timestamp").type(JsonFieldType.STRING).description("The creation date and time of order object"),
                        fieldWithPath("update_timestamp").type(JsonFieldType.STRING).description("The update date and time of order object"),

                        fieldWithPath("client").type(JsonFieldType.OBJECT).description("An object of client"),
                        fieldWithPath("client.id").type(JsonFieldType.NUMBER).description("The id of client"),
                        fieldWithPath("client.email").type(JsonFieldType.STRING).description("Client email"),
                        fieldWithPath("client.password").type(JsonFieldType.STRING).description("Client password"),
                        fieldWithPath("client.first_name").type(JsonFieldType.STRING).description("Client first name"),
                        fieldWithPath("client.last_name").type(JsonFieldType.STRING).description("Client last name"),
                        fieldWithPath("client.address").type(JsonFieldType.STRING).description("Client address"),
                        fieldWithPath("client.mobile").type(JsonFieldType.STRING).description("Client mobile phone"),
                        fieldWithPath("client.creation_date_time").type(JsonFieldType.STRING).description("Record create date time"),
                        fieldWithPath("client.update_date_time").type(JsonFieldType.STRING).description("Record update date time"),

                        fieldWithPath("parcels").type(JsonFieldType.ARRAY).description("An array of parcel objects"),
                        fieldWithPath("parcels[].id").type(JsonFieldType.NUMBER).description("The id of parcel"),
                        fieldWithPath("parcels[].type").type(JsonFieldType.STRING).description("The type of parcel"),
                        fieldWithPath("parcels[].length").type(JsonFieldType.NUMBER).description("The length of parcel"),
                        fieldWithPath("parcels[].width").type(JsonFieldType.NUMBER).description("The width of parcel"),
                        fieldWithPath("parcels[].height").type(JsonFieldType.NUMBER).description("The height of parcel"),
                        fieldWithPath("parcels[].weight").type(JsonFieldType.NUMBER).description("The weight of parcel"),
                        fieldWithPath("parcels[].quantity").type(JsonFieldType.NUMBER).description("The quantity of parcel"),

                        fieldWithPath("pickup_information").type(JsonFieldType.OBJECT).description("An object with pickup information"),
                        fieldWithPath("pickup_information.id").type(JsonFieldType.NUMBER).description("The id of the pickup object"),
                        fieldWithPath("pickup_information.name").type(JsonFieldType.STRING).description("The name of the client"),
                        fieldWithPath("pickup_information.email").type(JsonFieldType.STRING).description("The email of the client"),
                        fieldWithPath("pickup_information.city").type(JsonFieldType.STRING).description("The city of the client"),
                        fieldWithPath("pickup_information.address").type(JsonFieldType.STRING).description("The address of the client"),
                        fieldWithPath("pickup_information.postal_code").type(JsonFieldType.STRING).description("The postal code of the client"),
                        fieldWithPath("pickup_information.phone_number").type(JsonFieldType.STRING).description("The phone number of the client"),
                        fieldWithPath("pickup_date").type(JsonFieldType.STRING).description("The order pickup date"),

                        fieldWithPath("delivery_information").type(JsonFieldType.OBJECT).description("An object with delivery information"),
                        fieldWithPath("delivery_information.id").type(JsonFieldType.NUMBER).description("The id of the delivery object"),
                        fieldWithPath("delivery_information.name").type(JsonFieldType.STRING).description("The name of the recipient"),
                        fieldWithPath("delivery_information.email").type(JsonFieldType.STRING).description("The email of the recipient"),
                        fieldWithPath("delivery_information.city").type(JsonFieldType.STRING).description("The city of the recipient"),
                        fieldWithPath("delivery_information.address").type(JsonFieldType.STRING).description("The address of the recipient"),
                        fieldWithPath("delivery_information.postal_code").type(JsonFieldType.STRING).description("The postal code of the recipient"),
                        fieldWithPath("delivery_information.phone_number").type(JsonFieldType.STRING).description("The phone number of the recipient"),
                        fieldWithPath("delivery_type").type(JsonFieldType.STRING).description("The type of delivery"),

                        fieldWithPath("comment").type(JsonFieldType.STRING).description("The additional comments of order"),
                        fieldWithPath("payment_type").type(JsonFieldType.STRING).description("Order payment type")
                )
        );
    }

    private RestDocumentationResultHandler getDocumentEndpoint() {
        return document(
                "getOrders",
                preprocessRequest(removeHeaders("Content-Length", "Host"), prettyPrint()),
                preprocessResponse(prettyPrint()),
                requestHeaders(
                        headerWithName("Authorization").description("Basic auth credentials")
                ),
                responseFields(
                        fieldWithPath("[]").type(JsonFieldType.ARRAY).description("An array of order objects"),
                        fieldWithPath("[].id").type(JsonFieldType.NUMBER).description("The id of order object"),
                        fieldWithPath("[].status").type(JsonFieldType.STRING).description("The status of order object"),
                        fieldWithPath("[].cost").type(JsonFieldType.NUMBER).description("The cost of order object"),
                        fieldWithPath("[].create_timestamp").type(JsonFieldType.STRING).description("The creation date and time of order object"),
                        fieldWithPath("[].update_timestamp").type(JsonFieldType.STRING).description("The update date and time of order object"),

                        fieldWithPath("[].client").type(JsonFieldType.OBJECT).description("An object of client"),
                        fieldWithPath("[].client.id").type(JsonFieldType.NUMBER).description("The id of client"),
                        fieldWithPath("[].client.email").type(JsonFieldType.STRING).description("Client email"),
                        fieldWithPath("[].client.password").type(JsonFieldType.STRING).description("Client password"),
                        fieldWithPath("[].client.first_name").type(JsonFieldType.STRING).description("Client first name"),
                        fieldWithPath("[].client.last_name").type(JsonFieldType.STRING).description("Client last name"),
                        fieldWithPath("[].client.address").type(JsonFieldType.STRING).description("Client address"),
                        fieldWithPath("[].client.mobile").type(JsonFieldType.STRING).description("Client mobile phone"),
                        fieldWithPath("[].client.creation_date_time").type(JsonFieldType.STRING).description("Record create date time"),
                        fieldWithPath("[].client.update_date_time").type(JsonFieldType.STRING).description("Record update date time"),

                        fieldWithPath("[].parcels").type(JsonFieldType.ARRAY).description("An array of parcel objects"),
                        fieldWithPath("[].parcels[].id").type(JsonFieldType.NUMBER).description("The id of parcel"),
                        fieldWithPath("[].parcels[].type").type(JsonFieldType.STRING).description("The type of parcel"),
                        fieldWithPath("[].parcels[].length").type(JsonFieldType.NUMBER).description("The length of parcel"),
                        fieldWithPath("[].parcels[].width").type(JsonFieldType.NUMBER).description("The width of parcel"),
                        fieldWithPath("[].parcels[].height").type(JsonFieldType.NUMBER).description("The height of parcel"),
                        fieldWithPath("[].parcels[].weight").type(JsonFieldType.NUMBER).description("The weight of parcel"),
                        fieldWithPath("[].parcels[].quantity").type(JsonFieldType.NUMBER).description("The quantity of parcel"),

                        fieldWithPath("[].pickup_information").type(JsonFieldType.OBJECT).description("An object with pickup information"),
                        fieldWithPath("[].pickup_information.id").type(JsonFieldType.NUMBER).description("The id of the pickup object"),
                        fieldWithPath("[].pickup_information.name").type(JsonFieldType.STRING).description("The name of the client"),
                        fieldWithPath("[].pickup_information.email").type(JsonFieldType.STRING).description("The email of the client"),
                        fieldWithPath("[].pickup_information.city").type(JsonFieldType.STRING).description("The city of the client"),
                        fieldWithPath("[].pickup_information.address").type(JsonFieldType.STRING).description("The address of the client"),
                        fieldWithPath("[].pickup_information.postal_code").type(JsonFieldType.STRING).description("The postal code of the client"),
                        fieldWithPath("[].pickup_information.phone_number").type(JsonFieldType.STRING).description("The phone number of the client"),
                        fieldWithPath("[].pickup_date").type(JsonFieldType.STRING).description("The order pickup date"),

                        fieldWithPath("[].delivery_information").type(JsonFieldType.OBJECT).description("An object with delivery information"),
                        fieldWithPath("[].delivery_information.id").type(JsonFieldType.NUMBER).description("The id of the delivery object"),
                        fieldWithPath("[].delivery_information.name").type(JsonFieldType.STRING).description("The name of the recipient"),
                        fieldWithPath("[].delivery_information.email").type(JsonFieldType.STRING).description("The email of the recipient"),
                        fieldWithPath("[].delivery_information.city").type(JsonFieldType.STRING).description("The city of the recipient"),
                        fieldWithPath("[].delivery_information.address").type(JsonFieldType.STRING).description("The address of the recipient"),
                        fieldWithPath("[].delivery_information.postal_code").type(JsonFieldType.STRING).description("The postal code of the recipient"),
                        fieldWithPath("[].delivery_information.phone_number").type(JsonFieldType.STRING).description("The phone number of the recipient"),
                        fieldWithPath("[].delivery_type").type(JsonFieldType.STRING).description("The type of delivery"),

                        fieldWithPath("[].comment").type(JsonFieldType.STRING).description("The additional comments of order"),
                        fieldWithPath("[].payment_type").type(JsonFieldType.STRING).description("Order payment type")
                )
        );
    }

    private RestDocumentationResultHandler documentEndpointCancelOrder() {
        return document(
                "cancelOrder",
                preprocessRequest(removeHeaders("Content-Length", "Host"), prettyPrint()),
                preprocessResponse(prettyPrint()),
                requestHeaders(
                        headerWithName("Authorization").description("Basic auth credentials")
                ),
                pathParameters(
                        parameterWithName("id").description("The id of order object")
                ),
                responseFields(
                        fieldWithPath("id").type(JsonFieldType.NUMBER).description("The id of order object"),
                        fieldWithPath("status").type(JsonFieldType.STRING).description("The status of order object"),
                        fieldWithPath("cost").type(JsonFieldType.NUMBER).description("The cost of order object"),
                        fieldWithPath("create_timestamp").type(JsonFieldType.STRING).description("The creation date and time of order object"),
                        fieldWithPath("update_timestamp").type(JsonFieldType.STRING).description("The update date and time of order object"),

                        fieldWithPath("client").type(JsonFieldType.OBJECT).description("An object of client"),
                        fieldWithPath("client.id").type(JsonFieldType.NUMBER).description("The id of client"),
                        fieldWithPath("client.email").type(JsonFieldType.STRING).description("Client email"),
                        fieldWithPath("client.password").type(JsonFieldType.STRING).description("Client password"),
                        fieldWithPath("client.first_name").type(JsonFieldType.STRING).description("Client first name"),
                        fieldWithPath("client.last_name").type(JsonFieldType.STRING).description("Client last name"),
                        fieldWithPath("client.address").type(JsonFieldType.STRING).description("Client address"),
                        fieldWithPath("client.mobile").type(JsonFieldType.STRING).description("Client mobile phone"),
                        fieldWithPath("client.creation_date_time").type(JsonFieldType.STRING).description("Record create date time"),
                        fieldWithPath("client.update_date_time").type(JsonFieldType.STRING).description("Record update date time"),

                        fieldWithPath("parcels").type(JsonFieldType.ARRAY).description("An array of parcel objects"),
                        fieldWithPath("parcels[].id").type(JsonFieldType.NUMBER).description("The id of parcel"),
                        fieldWithPath("parcels[].type").type(JsonFieldType.STRING).description("The type of parcel"),
                        fieldWithPath("parcels[].length").type(JsonFieldType.NUMBER).description("The length of parcel"),
                        fieldWithPath("parcels[].width").type(JsonFieldType.NUMBER).description("The width of parcel"),
                        fieldWithPath("parcels[].height").type(JsonFieldType.NUMBER).description("The height of parcel"),
                        fieldWithPath("parcels[].weight").type(JsonFieldType.NUMBER).description("The weight of parcel"),
                        fieldWithPath("parcels[].quantity").type(JsonFieldType.NUMBER).description("The quantity of parcel"),

                        fieldWithPath("pickup_information").type(JsonFieldType.OBJECT).description("An object with pickup information"),
                        fieldWithPath("pickup_information.id").type(JsonFieldType.NUMBER).description("The id of the pickup object"),
                        fieldWithPath("pickup_information.name").type(JsonFieldType.STRING).description("The name of the client"),
                        fieldWithPath("pickup_information.email").type(JsonFieldType.STRING).description("The email of the client"),
                        fieldWithPath("pickup_information.city").type(JsonFieldType.STRING).description("The city of the client"),
                        fieldWithPath("pickup_information.address").type(JsonFieldType.STRING).description("The address of the client"),
                        fieldWithPath("pickup_information.postal_code").type(JsonFieldType.STRING).description("The postal code of the client"),
                        fieldWithPath("pickup_information.phone_number").type(JsonFieldType.STRING).description("The phone number of the client"),
                        fieldWithPath("pickup_date").type(JsonFieldType.STRING).description("The order pickup date"),

                        fieldWithPath("delivery_information").type(JsonFieldType.OBJECT).description("An object with delivery information"),
                        fieldWithPath("delivery_information.id").type(JsonFieldType.NUMBER).description("The id of the delivery object"),
                        fieldWithPath("delivery_information.name").type(JsonFieldType.STRING).description("The name of the recipient"),
                        fieldWithPath("delivery_information.email").type(JsonFieldType.STRING).description("The email of the recipient"),
                        fieldWithPath("delivery_information.city").type(JsonFieldType.STRING).description("The city of the recipient"),
                        fieldWithPath("delivery_information.address").type(JsonFieldType.STRING).description("The address of the recipient"),
                        fieldWithPath("delivery_information.postal_code").type(JsonFieldType.STRING).description("The postal code of the recipient"),
                        fieldWithPath("delivery_information.phone_number").type(JsonFieldType.STRING).description("The phone number of the recipient"),
                        fieldWithPath("delivery_type").type(JsonFieldType.STRING).description("The type of delivery"),

                        fieldWithPath("comment").type(JsonFieldType.STRING).description("The additional comments of order"),
                        fieldWithPath("payment_type").type(JsonFieldType.STRING).description("Order payment type")
                )
        );
    }

    private RestDocumentationResultHandler documentEndpointEditOrder() {
        return document(
                "editOrder",
                preprocessRequest(removeHeaders("Content-Length", "Host"), prettyPrint()),
                preprocessResponse(prettyPrint()),
                requestHeaders(
                        headerWithName("Authorization").description("Basic auth credentials")
                ),
                pathParameters(
                        parameterWithName("id").description("The id of order object")
                ),
                requestFields(
                        fieldWithPath("parcels").type(JsonFieldType.ARRAY).description("An array of parcel objects"),
                        fieldWithPath("parcels[].type").type(JsonFieldType.STRING).description("The type of parcel"),
                        fieldWithPath("parcels[].length").type(JsonFieldType.NUMBER).description("The length of parcel"),
                        fieldWithPath("parcels[].width").type(JsonFieldType.NUMBER).description("The width of parcel"),
                        fieldWithPath("parcels[].height").type(JsonFieldType.NUMBER).description("The height of parcel"),
                        fieldWithPath("parcels[].weight").type(JsonFieldType.NUMBER).description("The weight of parcel"),
                        fieldWithPath("parcels[].quantity").type(JsonFieldType.NUMBER).description("The quantity of parcel"),

                        fieldWithPath("pickup_information").type(JsonFieldType.OBJECT).description("An object with pickup information"),
                        fieldWithPath("pickup_information.name").type(JsonFieldType.STRING).description("The name of the client"),
                        fieldWithPath("pickup_information.email").type(JsonFieldType.STRING).description("The email of the client"),
                        fieldWithPath("pickup_information.city").type(JsonFieldType.STRING).description("The city of the client"),
                        fieldWithPath("pickup_information.address").type(JsonFieldType.STRING).description("The address of the client"),
                        fieldWithPath("pickup_information.postal_code").type(JsonFieldType.STRING).description("The postal code of the client"),
                        fieldWithPath("pickup_information.phone_number").type(JsonFieldType.STRING).description("The phone number of the client"),
                        fieldWithPath("pickup_date").type(JsonFieldType.STRING).description("The order pickup date"),

                        fieldWithPath("delivery_information").type(JsonFieldType.OBJECT).description("An object with delivery information"),
                        fieldWithPath("delivery_information.name").type(JsonFieldType.STRING).description("The name of the recipient"),
                        fieldWithPath("delivery_information.email").type(JsonFieldType.STRING).description("The email of the recipient"),
                        fieldWithPath("delivery_information.city").type(JsonFieldType.STRING).description("The city of the recipient"),
                        fieldWithPath("delivery_information.address").type(JsonFieldType.STRING).description("The address of the recipient"),
                        fieldWithPath("delivery_information.postal_code").type(JsonFieldType.STRING).description("The postal code of the recipient"),
                        fieldWithPath("delivery_information.phone_number").type(JsonFieldType.STRING).description("The phone number of the recipient"),
                        fieldWithPath("delivery_type").type(JsonFieldType.STRING).description("The type of delivery"),

                        fieldWithPath("comment").type(JsonFieldType.STRING).description("The additional comments of order"),
                        fieldWithPath("payment_type").type(JsonFieldType.STRING).description("Order payment type")
                ),
                responseFields(
                        fieldWithPath("id").type(JsonFieldType.NUMBER).description("The id of order object"),
                        fieldWithPath("status").type(JsonFieldType.STRING).description("The status of order object"),
                        fieldWithPath("cost").type(JsonFieldType.NUMBER).description("The cost of order object"),
                        fieldWithPath("create_timestamp").type(JsonFieldType.STRING).description("The creation date and time of order object"),
                        fieldWithPath("update_timestamp").type(JsonFieldType.STRING).description("The update date and time of order object"),

                        fieldWithPath("client").type(JsonFieldType.OBJECT).description("An object of client"),
                        fieldWithPath("client.id").type(JsonFieldType.NUMBER).description("The id of client"),
                        fieldWithPath("client.email").type(JsonFieldType.STRING).description("Client email"),
                        fieldWithPath("client.password").type(JsonFieldType.STRING).description("Client password"),
                        fieldWithPath("client.first_name").type(JsonFieldType.STRING).description("Client first name"),
                        fieldWithPath("client.last_name").type(JsonFieldType.STRING).description("Client last name"),
                        fieldWithPath("client.address").type(JsonFieldType.STRING).description("Client address"),
                        fieldWithPath("client.mobile").type(JsonFieldType.STRING).description("Client mobile phone"),
                        fieldWithPath("client.creation_date_time").type(JsonFieldType.STRING).description("Record create date time"),
                        fieldWithPath("client.update_date_time").type(JsonFieldType.STRING).description("Record update date time"),

                        fieldWithPath("parcels").type(JsonFieldType.ARRAY).description("An array of parcel objects"),
                        fieldWithPath("parcels[].id").type(JsonFieldType.NUMBER).description("The id of parcel"),
                        fieldWithPath("parcels[].type").type(JsonFieldType.STRING).description("The type of parcel"),
                        fieldWithPath("parcels[].length").type(JsonFieldType.NUMBER).description("The length of parcel"),
                        fieldWithPath("parcels[].width").type(JsonFieldType.NUMBER).description("The width of parcel"),
                        fieldWithPath("parcels[].height").type(JsonFieldType.NUMBER).description("The height of parcel"),
                        fieldWithPath("parcels[].weight").type(JsonFieldType.NUMBER).description("The weight of parcel"),
                        fieldWithPath("parcels[].quantity").type(JsonFieldType.NUMBER).description("The quantity of parcel"),

                        fieldWithPath("pickup_information").type(JsonFieldType.OBJECT).description("An object with pickup information"),
                        fieldWithPath("pickup_information.id").type(JsonFieldType.NUMBER).description("The id of the pickup object"),
                        fieldWithPath("pickup_information.name").type(JsonFieldType.STRING).description("The name of the client"),
                        fieldWithPath("pickup_information.email").type(JsonFieldType.STRING).description("The email of the client"),
                        fieldWithPath("pickup_information.city").type(JsonFieldType.STRING).description("The city of the client"),
                        fieldWithPath("pickup_information.address").type(JsonFieldType.STRING).description("The address of the client"),
                        fieldWithPath("pickup_information.postal_code").type(JsonFieldType.STRING).description("The postal code of the client"),
                        fieldWithPath("pickup_information.phone_number").type(JsonFieldType.STRING).description("The phone number of the client"),
                        fieldWithPath("pickup_date").type(JsonFieldType.STRING).description("The order pickup date"),

                        fieldWithPath("delivery_information").type(JsonFieldType.OBJECT).description("An object with delivery information"),
                        fieldWithPath("delivery_information.id").type(JsonFieldType.NUMBER).description("The id of the delivery object"),
                        fieldWithPath("delivery_information.name").type(JsonFieldType.STRING).description("The name of the recipient"),
                        fieldWithPath("delivery_information.email").type(JsonFieldType.STRING).description("The email of the recipient"),
                        fieldWithPath("delivery_information.city").type(JsonFieldType.STRING).description("The city of the recipient"),
                        fieldWithPath("delivery_information.address").type(JsonFieldType.STRING).description("The address of the recipient"),
                        fieldWithPath("delivery_information.postal_code").type(JsonFieldType.STRING).description("The postal code of the recipient"),
                        fieldWithPath("delivery_information.phone_number").type(JsonFieldType.STRING).description("The phone number of the recipient"),
                        fieldWithPath("delivery_type").type(JsonFieldType.STRING).description("The type of delivery"),

                        fieldWithPath("comment").type(JsonFieldType.STRING).description("The additional comments of order"),
                        fieldWithPath("payment_type").type(JsonFieldType.STRING).description("Order payment type")
                )
        );
    }

}
