package courier.couriersolutions.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import courier.couriersolutions.CSHttpHeaders;
import courier.couriersolutions.dto.ClientInfoDTO;
import courier.couriersolutions.dto.PassInfoDTO;
import courier.couriersolutions.entities.Client;
import courier.couriersolutions.entities.Session;
import courier.couriersolutions.exceptions.error.ApplicationError;
import courier.couriersolutions.exceptions.error.RequestError;
import courier.couriersolutions.repository.ClientRepository;
import courier.couriersolutions.repository.ClientAuditRepository;
import courier.couriersolutions.repository.OrderRepository;
import courier.couriersolutions.repository.SessionRepository;
import courier.couriersolutions.repository.PaymentInformationRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.http.MediaType;
import org.springframework.restdocs.mockmvc.RestDocumentationResultHandler;
import org.springframework.restdocs.payload.JsonFieldType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import javax.servlet.http.Cookie;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.restdocs.headers.HeaderDocumentation.headerWithName;
import static org.springframework.restdocs.headers.HeaderDocumentation.requestHeaders;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessRequest;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessResponse;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.removeHeaders;
import static org.springframework.restdocs.payload.PayloadDocumentation.requestFields;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(ClientController.class)
@AutoConfigureRestDocs(outputDir = "target/snippets")
@AutoConfigureMockMvc
class ClientControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private SessionRepository sessionRepository;

    @MockBean
    private ClientRepository clientRepository;

    @MockBean
    private OrderRepository orderRepository;

    @MockBean
    private ClientAuditRepository clientAuditRepository;

    @MockBean
    private PaymentInformationRepository paymentInformationRepository;

    @Test
    void testGetClientInfo() throws Exception {
        when(sessionRepository.findBySessionKey(any()))
                .thenAnswer(invocation -> {
                    Session session = new Session();
                    session.setSessionKey(UUID.randomUUID().toString());
                    session.setClient(createMockClient());
                    return Optional.of(session);
                });

        MvcResult mvcResult = mockMvc.perform(get("/api/client-info")
                .with(httpBasic("user", "pass")).with(csrf())
                .cookie(new Cookie(CSHttpHeaders.SESS_KEY, "TestSessKey"))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(getClientInfoDocumentEndpoint())
                .andReturn();

        String response = mvcResult.getResponse().getContentAsString();
        Client fetchedClient = objectMapper.readValue(response, Client.class);

        Assertions.assertNotNull(fetchedClient);
    }

    @Test
    void testUpdateClientInfo() throws Exception {
        when(sessionRepository.findBySessionKey(any()))
                .thenAnswer(invocation -> {
                    Session session = new Session();
                    session.setSessionKey(UUID.randomUUID().toString());
                    session.setClient(createMockClient());
                    return Optional.of(session);
                });

        when(clientRepository.save(any()))
                .then(invocation -> createMockClient());

        ClientInfoDTO clientInfo = new ClientInfoDTO(
                "testFName",
                "testLName",
                "testAddress",
                "+37011111111"
        );

        MvcResult mvcResult = mockMvc.perform(put("/api/client-info")
                .with(httpBasic("user", "pass")).with(csrf())
                .cookie(new Cookie(CSHttpHeaders.SESS_KEY, "TestSessKey"))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(clientInfo)))
                .andExpect(status().isOk())
                .andDo(updateClientInfoDocumentEndpoint())
                .andReturn();

        String response = mvcResult.getResponse().getContentAsString();
        Client fetchedClient = objectMapper.readValue(response, Client.class);

        Assertions.assertNotNull(fetchedClient);
    }

    @Test
    void testUpdateClientPass() throws Exception {
        when(sessionRepository.findBySessionKey(any()))
                .thenAnswer(invocation -> {
                    Session session = new Session();
                    session.setSessionKey(UUID.randomUUID().toString());
                    session.setClient(createMockClient());
                    return Optional.of(session);
                });

        when(clientRepository.save(any()))
                .then(invocation -> createMockClient());

        PassInfoDTO passInfo = new PassInfoDTO(
                "testP@ssword1",
                "testNewP@ssword1",
                "testNewP@ssword1"
        );

        MvcResult mvcResult = mockMvc.perform(put("/api/client-password")
                .with(httpBasic("user", "pass")).with(csrf())
                .cookie(new Cookie(CSHttpHeaders.SESS_KEY, "TestSessKey"))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(passInfo)))
                .andExpect(status().isOk())
                .andDo(updateClientPassDocumentEndpoint())
                .andReturn();

        String response = mvcResult.getResponse().getContentAsString();
        Client fetchedClient = objectMapper.readValue(response, Client.class);

        Assertions.assertNotNull(fetchedClient);
    }

    @Test
    void testUpdateClientPassFailConfirm() throws Exception {
        when(sessionRepository.findBySessionKey(any()))
                .thenAnswer(invocation -> {
                    Session session = new Session();
                    session.setSessionKey(UUID.randomUUID().toString());
                    session.setClient(createMockClient());
                    return Optional.of(session);
                });

        when(clientRepository.save(any()))
                .then(invocation -> createMockClient());

        PassInfoDTO passInfo = new PassInfoDTO(
                "testBadP@ssword1",
                "testNewP@ssword1",
                "testNewP@ssword1"

        );

        MvcResult mvcResult = mockMvc.perform(put("/api/client-password")
                .with(httpBasic("user", "pass")).with(csrf())
                .cookie(new Cookie(CSHttpHeaders.SESS_KEY, "TestSessKey"))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(passInfo)))
                .andExpect(status().isForbidden())
                .andReturn();

        RequestError error = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), RequestError.class);

        Assertions.assertEquals(ApplicationError.CS_ACCESS_DENIED.getErrorName(), error.getErrorName());
        Assertions.assertEquals(ApplicationError.CS_ACCESS_DENIED.getHttpStatus().toString(), error.getHttpStatus());
    }

    @Test
    void testUpdateClientPassFailRepeat() throws Exception {
        when(sessionRepository.findBySessionKey(any()))
                .thenAnswer(invocation -> {
                    Session session = new Session();
                    session.setSessionKey(UUID.randomUUID().toString());
                    session.setClient(createMockClient());
                    return Optional.of(session);
                });

        when(clientRepository.save(any()))
                .then(invocation -> createMockClient());

        PassInfoDTO passInfo = new PassInfoDTO(
                "testP@ssword1",
                "testNewP@ssword1",
                "testBadNewP@ssword1"

        );

        MvcResult mvcResult = mockMvc.perform(put("/api/client-password")
                .with(httpBasic("user", "pass")).with(csrf())
                .cookie(new Cookie(CSHttpHeaders.SESS_KEY, "TestSessKey"))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(passInfo)))
                .andExpect(status().isBadRequest())
                .andReturn();

        RequestError error = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), RequestError.class);

        Assertions.assertEquals(ApplicationError.CS_PASSWORDS_DONT_MATCH.getErrorName(), error.getErrorName());
        Assertions.assertEquals(ApplicationError.CS_PASSWORDS_DONT_MATCH.getHttpStatus().toString(), error.getHttpStatus());
    }

    @Test
    void testSessionInterceptorNullCookies() throws Exception {
        MvcResult mvcResult = mockMvc.perform(put("/api/client-password")
                .with(httpBasic("user", "pass")).with(csrf())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized())
                .andReturn();

        RequestError error = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), RequestError.class);

        Assertions.assertEquals(ApplicationError.CS_MISSING_SESS_KEY.getErrorName(), error.getErrorName());
        Assertions.assertEquals(ApplicationError.CS_MISSING_SESS_KEY.getHttpStatus().toString(), error.getHttpStatus());
    }

    @Test
    void testSessionInterceptorNotFoundRequiredCookie() throws Exception {
        MvcResult mvcResult = mockMvc.perform(put("/api/client-password")
                .with(httpBasic("user", "pass")).with(csrf())
                .cookie(new Cookie("BAD_HEADER", "TestSessKey"))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized())
                .andReturn();

        RequestError error = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), RequestError.class);

        Assertions.assertEquals(ApplicationError.CS_MISSING_SESS_KEY.getErrorName(), error.getErrorName());
        Assertions.assertEquals(ApplicationError.CS_MISSING_SESS_KEY.getHttpStatus().toString(), error.getHttpStatus());
    }

    @Test
    void testSessionInterceptorNotFoundActiveSession() throws Exception {
        when(sessionRepository.findBySessionKey(any()))
                .thenAnswer(invocation -> Optional.empty());

        MvcResult mvcResult = mockMvc.perform(put("/api/client-password")
                .with(httpBasic("user", "pass")).with(csrf())
                .cookie(new Cookie(CSHttpHeaders.SESS_KEY, "TestSessKey"))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized())
                .andReturn();

        RequestError error = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), RequestError.class);

        Assertions.assertEquals(ApplicationError.CS_SESSION_EXPIRED.getErrorName(), error.getErrorName());
        Assertions.assertEquals(ApplicationError.CS_SESSION_EXPIRED.getHttpStatus().toString(), error.getHttpStatus());
    }

    @Test
    void testHandleOptimisticLockingFailureException() throws Exception{
        ClientInfoDTO clientInfo = new ClientInfoDTO(
                "testFName",
                "testLName",
                "testAddress",
                "+37011111111"
        );

        Client tempGood = createMockClient();
        tempGood.setEmail("labas@labas");

        when(sessionRepository.findBySessionKey(any()))
                .thenAnswer(invocation -> {
                    Session session = new Session();
                    session.setSessionKey(UUID.randomUUID().toString());
                    session.setClient(createMockClient());
                    return Optional.of(session);
                });

        when(clientRepository.save(any()))
                .then(invocation -> {
                    Client client = invocation.getArgument(0);

                    if(client.getEmail().equals("labas@labas")) {
                        return createMockClient();
                    }

                    throw new OptimisticLockingFailureException("Test");
                });

        when(clientRepository.findById(any()))
                .then(invocation -> Optional.of(tempGood));

        MvcResult mvcResult = mockMvc.perform(put("/api/client-info")
                .with(httpBasic("user", "pass")).with(csrf())
                .cookie(new Cookie(CSHttpHeaders.SESS_KEY, "TestSessKey"))
                .param("bypassOptLock", "true")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(clientInfo)))
                .andExpect(status().isOk())
                .andReturn();

        String response = mvcResult.getResponse().getContentAsString();
        Client fetchedClient = objectMapper.readValue(response, Client.class);

        Assertions.assertNotNull(fetchedClient);
    }

    private Client createMockClient() {
        Client client = new Client();
        client.setId(1L);
        client.setFirstName("TestFirstName");
        client.setLastName("TestLastName");
        client.setAddress("TestAddress");
        client.setMobile("TestMobile");
        client.setEmail("Test@email");
        client.setPassword("testP@ssword1");
        client.setCreationDateTime(LocalDateTime.now());
        client.setUpdateDateTime(LocalDateTime.now());

        return client;
    }

    private RestDocumentationResultHandler getClientInfoDocumentEndpoint() {
        return document(
                "getClientInfo",
                preprocessRequest(removeHeaders("Content-Length", "Host"), prettyPrint()),
                preprocessResponse(prettyPrint()),
                requestHeaders(
                        headerWithName("Authorization").description("Basic auth credentials")
                ),
                responseFields(
                        fieldWithPath("id").type(JsonFieldType.NUMBER).description("The id of Client object"),
                        fieldWithPath("email").type(JsonFieldType.STRING).description("The email of Client object"),
                        fieldWithPath("password").type(JsonFieldType.STRING).description("The password of Client object"),
                        fieldWithPath("first_name").type(JsonFieldType.STRING).description("The first name of Client object"),
                        fieldWithPath("last_name").type(JsonFieldType.STRING).description("The last name of Client object"),
                        fieldWithPath("address").type(JsonFieldType.STRING).description("The address of Client object"),
                        fieldWithPath("mobile").type(JsonFieldType.STRING).description("The phone number of Client object"),
                        fieldWithPath("creation_date_time").type(JsonFieldType.STRING).description("The creation date and time of Client object"),
                        fieldWithPath("update_date_time").type(JsonFieldType.STRING).description("The update date and time of Client object")
                )
        );
    }

    private RestDocumentationResultHandler updateClientInfoDocumentEndpoint() {
        return document(
                "updateClientInfo",
                preprocessRequest(removeHeaders("Content-Length", "Host"), prettyPrint()),
                preprocessResponse(prettyPrint()),
                requestHeaders(
                        headerWithName("Authorization").description("Basic auth credentials")
                ),
                requestFields(
                        fieldWithPath("first_name").type(JsonFieldType.STRING).description("The first name of Client object"),
                        fieldWithPath("last_name").type(JsonFieldType.STRING).description("The last name of Client object"),
                        fieldWithPath("address").type(JsonFieldType.STRING).description("The address of Client object"),
                        fieldWithPath("mobile").type(JsonFieldType.STRING).description("The phone number of Client object")
                ),
                responseFields(
                        fieldWithPath("id").type(JsonFieldType.NUMBER).description("The id of Client object"),
                        fieldWithPath("email").type(JsonFieldType.STRING).description("The email of Client object"),
                        fieldWithPath("password").type(JsonFieldType.STRING).description("The password of Client object"),
                        fieldWithPath("first_name").type(JsonFieldType.STRING).description("The first name of Client object"),
                        fieldWithPath("last_name").type(JsonFieldType.STRING).description("The last name of Client object"),
                        fieldWithPath("address").type(JsonFieldType.STRING).description("The address of Client object"),
                        fieldWithPath("mobile").type(JsonFieldType.STRING).description("The phone number of Client object"),
                        fieldWithPath("creation_date_time").type(JsonFieldType.STRING).description("The creation date and time of Client object"),
                        fieldWithPath("update_date_time").type(JsonFieldType.STRING).description("The update date and time of Client object")
                )
        );
    }

    private RestDocumentationResultHandler updateClientPassDocumentEndpoint() {
        return document(
                "updateClientPass",
                preprocessRequest(removeHeaders("Content-Length", "Host"), prettyPrint()),
                preprocessResponse(prettyPrint()),
                requestHeaders(
                        headerWithName("Authorization").description("Basic auth credentials")
                ),
                requestFields(
                        fieldWithPath("current_password").type(JsonFieldType.STRING).description("The current password of Client object"),
                        fieldWithPath("new_password").type(JsonFieldType.STRING).description("The new password of Client object"),
                        fieldWithPath("repeat_new_password").type(JsonFieldType.STRING).description("The repeated new password of Client object")
                ),
                responseFields(
                        fieldWithPath("id").type(JsonFieldType.NUMBER).description("The id of Client object"),
                        fieldWithPath("email").type(JsonFieldType.STRING).description("The email of Client object"),
                        fieldWithPath("password").type(JsonFieldType.STRING).description("The password of Client object"),
                        fieldWithPath("first_name").type(JsonFieldType.STRING).description("The first name of Client object"),
                        fieldWithPath("last_name").type(JsonFieldType.STRING).description("The last name of Client object"),
                        fieldWithPath("address").type(JsonFieldType.STRING).description("The address of Client object"),
                        fieldWithPath("mobile").type(JsonFieldType.STRING).description("The phone number of Client object"),
                        fieldWithPath("creation_date_time").type(JsonFieldType.STRING).description("The creation date and time of Client object"),
                        fieldWithPath("update_date_time").type(JsonFieldType.STRING).description("The update date and time of Client object")
                )
        );
    }
}
