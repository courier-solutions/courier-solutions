package courier.couriersolutions.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import courier.couriersolutions.dto.ClientDTO;
import courier.couriersolutions.dto.LogInDTO;
import courier.couriersolutions.dto.LogInResponseDTO;
import courier.couriersolutions.entities.Client;
import courier.couriersolutions.exceptions.error.ApplicationError;
import courier.couriersolutions.exceptions.error.RequestError;
import courier.couriersolutions.repository.ClientRepository;
import courier.couriersolutions.repository.ClientAuditRepository;
import courier.couriersolutions.repository.OrderRepository;
import courier.couriersolutions.repository.SessionRepository;
import courier.couriersolutions.repository.PaymentInformationRepository;
import courier.couriersolutions.services.OrderCostService;
import courier.couriersolutions.services.SessionHandlingService;
import courier.couriersolutions.services.ClientService;
import java.time.LocalDateTime;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.restdocs.payload.JsonFieldType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.util.StringUtils;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.restdocs.headers.HeaderDocumentation.headerWithName;
import static org.springframework.restdocs.headers.HeaderDocumentation.requestHeaders;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessRequest;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessResponse;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.removeHeaders;
import static org.springframework.restdocs.payload.PayloadDocumentation.requestFields;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(AccessController.class)
@AutoConfigureRestDocs(outputDir = "target/snippets")
@AutoConfigureMockMvc
class AccessControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private SessionRepository sessionRepository;

    @MockBean
    private ClientRepository clientRepository;

    @MockBean
    private OrderRepository orderRepository;

    @MockBean
    private ClientAuditRepository clientAuditRepository;

    @MockBean
    private SessionHandlingService sessionHandlingService;

    @MockBean
    private PaymentInformationRepository paymentInformationRepository;

    @Autowired
    private ClientService clientService;

    @MockBean
    private OrderCostService orderCostService;

    @Test
    void testLoginUser() throws Exception {
        when(clientRepository.findByEmailAndPassword(any(), any()))
                .thenAnswer(invocation -> {
                    Client user = new Client();

                    return Optional.of(user);
                });

        when(sessionHandlingService.createSessionKey(any()))
                .thenAnswer(invocation -> "TestSessionKey");

        LogInDTO logInDTO = new LogInDTO();
        logInDTO.setUsername("testClient@gmail.com");
        logInDTO.setPassword("Test@123");

        MvcResult mvcResult = mockMvc
                .perform(
                        MockMvcRequestBuilders.post("/api/login")
                                .with(httpBasic("user", "pass"))
                                .with(csrf())
                                .content(objectMapper.writeValueAsString(logInDTO))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(
                        document(
                                "loginUser",
                                preprocessRequest(removeHeaders("Content-Length", "Host"), prettyPrint()),
                                preprocessResponse(prettyPrint()),
                                requestHeaders(
                                        headerWithName("Authorization").description("Basic authentication header")
                                ),
                                requestFields(
                                        fieldWithPath("username").type(JsonFieldType.STRING).description("Client username (email)"),
                                        fieldWithPath("password").type(JsonFieldType.STRING).description("Client password")
                                ),
                                responseFields(
                                        fieldWithPath("status").type(JsonFieldType.STRING).description("Login status")
                                )
                        )
                )
                .andDo(print())
                .andReturn();
        LogInResponseDTO responseDTO = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), LogInResponseDTO.class);

        Assertions.assertNotNull(responseDTO);
        Assertions.assertFalse(StringUtils.isEmpty(responseDTO.getStatus()));
    }

    @Test
    void testLoginUserNotFound() throws Exception {
        when(clientRepository.findByEmailAndPassword(any(), any()))
                .thenAnswer(invocation -> Optional.empty());

        LogInDTO logInDTO = new LogInDTO();
        logInDTO.setUsername("testClient@gmail.com");
        logInDTO.setPassword("Test@123");

        MvcResult mvcResult = mockMvc
                .perform(
                        MockMvcRequestBuilders.post("/api/login")
                                .with(httpBasic("user", "pass"))
                                .with(csrf())
                                .content(objectMapper.writeValueAsString(logInDTO))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andReturn();

        RequestError error = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), RequestError.class);

        Assertions.assertEquals(ApplicationError.CS_CLIENT_NOT_FOUND.getErrorName(), error.getErrorName());
        Assertions.assertEquals(ApplicationError.CS_CLIENT_NOT_FOUND.getHttpStatus().toString(), error.getHttpStatus());
    }

    @Test
    void testRegisterUser() throws Exception {
        when(clientRepository.save(any()))
            .thenAnswer(invocation -> {
                Client client = new Client("test@mail.com","compl1Ant^","Some","Name","Somewhere in Vilnius","+37061455425");
                client.setId(1L);
                client.setUpdateDateTime(LocalDateTime.now());
                client.setCreationDateTime(LocalDateTime.now());

                return client;
            });

        ClientDTO clientDTO = new ClientDTO();
        clientDTO.setEmail("test@mail.com");
        clientDTO.setPassword("compl1Ant^");
        clientDTO.setRepeatPassword("compl1Ant^");
        clientDTO.setFirstName("Some");
        clientDTO.setLastName("Name");
        clientDTO.setMobile("+37061455425");
        clientDTO.setAddress("Somewhere in Vilnius");

        MvcResult mvcResult = mockMvc.perform(
            MockMvcRequestBuilders.post("/api/register")
                .with(httpBasic("user", "pass"))
                .with(csrf())
                .content(objectMapper.writeValueAsString(clientDTO))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andDo(
                document(
                    "registerUser",
                    preprocessRequest(removeHeaders("Content-Length", "Host"), prettyPrint()),
                    preprocessResponse(prettyPrint()),
                    requestHeaders(
                        headerWithName("Authorization").description("Basic authentication header")
                    ),
                    requestFields(
                        fieldWithPath("email").type(JsonFieldType.STRING).description("Client username (email)"),
                        fieldWithPath("password").type(JsonFieldType.STRING).description("Client password"),
                        fieldWithPath("repeat_password").type(JsonFieldType.STRING).description("Client password to check if it matches"),
                        fieldWithPath("mobile").type(JsonFieldType.STRING).description("Client mobile phone"),
                        fieldWithPath("address").type(JsonFieldType.STRING).description("Client address"),
                        fieldWithPath("first_name").type(JsonFieldType.STRING).description("Client first name"),
                        fieldWithPath("last_name").type(JsonFieldType.STRING).description("Client last name")
                    ),
                    responseFields(
                        fieldWithPath("id").type(JsonFieldType.NUMBER).description("Client id"),
                        fieldWithPath("email").type(JsonFieldType.STRING).description("Client username (email)"),
                        fieldWithPath("password").type(JsonFieldType.STRING).description("Client password"),
                        fieldWithPath("address").type(JsonFieldType.STRING).description("Client address"),
                        fieldWithPath("mobile").type(JsonFieldType.STRING).description("Client mobile phone"),
                        fieldWithPath("first_name").type(JsonFieldType.STRING).description("Client first name"),
                        fieldWithPath("last_name").type(JsonFieldType.STRING).description("Client last name"),
                        fieldWithPath("creation_date_time").type(JsonFieldType.STRING).description("Client creation timestamp"),
                        fieldWithPath("update_date_time").type(JsonFieldType.STRING).description("Client update timestamp")
                    )
                )
            )
            .andDo(print())
            .andReturn();

        Client client = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), Client.class);

        Assertions.assertNotNull(client);
    }

    @Test
    void testRegisterUserPasswordsDontMatch() throws Exception {

        ClientDTO clientDTO = new ClientDTO();
        clientDTO.setEmail("test@mail.com");
        clientDTO.setPassword("compl1Ant^");
        clientDTO.setRepeatPassword("compl1Ant^4");
        clientDTO.setFirstName("Some");
        clientDTO.setLastName("Name");
        clientDTO.setMobile("+37061455425");
        clientDTO.setAddress("Somewhere in Vilnius");

        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post("/api/register")
            .with(httpBasic("user", "pass"))
            .with(csrf())
            .content(objectMapper.writeValueAsString(clientDTO))
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest())
        .andReturn();

        RequestError error = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), RequestError.class);

        Assertions.assertEquals(ApplicationError.CS_PASSWORDS_DONT_MATCH.getErrorName(), error.getErrorName());
        Assertions.assertEquals(ApplicationError.CS_PASSWORDS_DONT_MATCH.getHttpStatus().toString(), error.getHttpStatus());
    }

    @Test
    void testRegisterUserAlreadyExists() throws Exception {
        when(clientRepository.existsByEmail(any())).thenAnswer(invocation -> true);

        ClientDTO clientDTO = new ClientDTO();
        clientDTO.setEmail("test@mail.com");
        clientDTO.setPassword("compl1Ant^");
        clientDTO.setRepeatPassword("compl1Ant^");
        clientDTO.setFirstName("Some");
        clientDTO.setLastName("Name");
        clientDTO.setMobile("+37061455425");
        clientDTO.setAddress("Somewhere in Vilnius");

        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post("/api/register")
            .with(httpBasic("user", "pass"))
            .with(csrf())
            .content(objectMapper.writeValueAsString(clientDTO))
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest())
        .andReturn();

        RequestError error = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), RequestError.class);

        Assertions.assertEquals(ApplicationError.CS_CLIENT_ALREADY_EXISTS.getErrorName(), error.getErrorName());
        Assertions.assertEquals(ApplicationError.CS_CLIENT_ALREADY_EXISTS.getHttpStatus().toString(), error.getHttpStatus());
    }
}
