package courier.couriersolutions.context;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;
import java.util.Objects;

@Configuration
@EnableJpaRepositories("courier.couriersolutions.repository")
public class DatabaseTestConfig {

    @Autowired
    private Environment environment;

    @Bean
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();

        dataSource.setDriverClassName(Objects.requireNonNull(environment.getProperty("database.postgres.driver")));
        dataSource.setUrl(environment.getProperty("database.postgres.url"));
        dataSource.setUsername(environment.getProperty("database.postgres.username"));
        dataSource.setPassword(environment.getProperty("database.postgres.password"));

        return dataSource;
    }

}
