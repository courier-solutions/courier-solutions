package courier.couriersolutions.services;

import courier.couriersolutions.Application;
import courier.couriersolutions.context.DatabaseTestConfig;
import courier.couriersolutions.entities.Client;
import courier.couriersolutions.entities.Session;
import courier.couriersolutions.repository.ClientAuditRepository;
import courier.couriersolutions.repository.ClientRepository;
import courier.couriersolutions.repository.PaymentInformationRepository;
import courier.couriersolutions.repository.SessionRepository;
import javassist.NotFoundException;
import org.awaitility.Awaitility;
import org.awaitility.Durations;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.Duration;
import java.util.UUID;
import java.util.concurrent.Callable;

import static org.awaitility.Awaitility.await;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {Application.class, DatabaseTestConfig.class})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class SessionRefreshServiceTestIT {

    @Autowired
    private SessionHandlingServiceImpl sessionHandlingService;

    @Autowired
    private SessionRepository sessionRepository;

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private ClientAuditRepository clientAuditRepository;

    @MockBean
    private PaymentInformationRepository paymentInformationRepository;

    @MockBean
    private OrderCostService orderCostService;

    @Value("${session.timeout.minutes}")
    public long sessionTimeoutMinutes;

    private Session session;
    private Client client;

    @BeforeAll
    void init() {
        client = clientRepository.save(new Client("testEmail", "password", "firstName",
                "lastName", "address", "+37011122234"));
        session = sessionRepository.save(new Session(client, "TEST-" + UUID.randomUUID()));
    }

    @AfterAll
    void delete() {
        clientAuditRepository.deleteAllByClientId(client.getId());
        clientRepository.deleteById(client.getId());
    }

    @Test
    void testOutdatedSessionDeactivationAfterUpdate() throws Exception {
        Awaitility.setDefaultTimeout(Durations.ONE_MINUTE);
        Awaitility.await().pollDelay(Duration.ofSeconds(20)).until(() -> true);

        try {
            sessionHandlingService.getActiveSession(session.getSessionKey());
        } catch (NotFoundException e) {
            throw new Exception("Failed test!");
        }

        long waitSecondsAtLeast = sessionTimeoutMinutes * 60 - 15;
        long waitSecondsAtMost = sessionTimeoutMinutes * 60 + 15;

        Duration durationAtLeast = Duration.ofSeconds(waitSecondsAtLeast);
        Duration durationAtMost = Duration.ofSeconds(waitSecondsAtMost);

        await().atLeast(durationAtLeast).atMost(durationAtMost).until(checkIfSessionFails());
    }

    @Test
    void testOutdatedSessionDeactivation() {
        long waitSeconds = sessionTimeoutMinutes * 60 + 15;

        Duration duration = Duration.ofSeconds(waitSeconds);

        await().atMost(duration).until(checkIfSessionFails());
    }

    private Callable<Boolean> checkIfSessionFails() {
        return () -> sessionRepository.findBySessionKey(session.getSessionKey()).isEmpty();
    }

}
