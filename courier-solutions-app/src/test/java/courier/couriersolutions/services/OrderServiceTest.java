package courier.couriersolutions.services;


import courier.couriersolutions.constants.PaymentType;
import courier.couriersolutions.dto.OrderInformationDTO;
import courier.couriersolutions.entities.Client;
import courier.couriersolutions.entities.Order;
import courier.couriersolutions.entities.PaymentInformation;
import courier.couriersolutions.exceptions.EmptyPaymentInformation;
import courier.couriersolutions.repository.ClientRepository;
import courier.couriersolutions.repository.ClientAuditRepository;
import courier.couriersolutions.repository.OrderRepository;
import courier.couriersolutions.repository.PaymentInformationRepository;
import courier.couriersolutions.repository.SessionRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.times;

@WebMvcTest(OrderService.class)
class OrderServiceTest {

    @Autowired
    private OrderService orderService;

    @MockBean
    private SessionRepository sessionRepository;

    @MockBean
    private ClientRepository clientRepository;

    @MockBean
    private OrderRepository orderRepository;

    @MockBean
    private PaymentInformationRepository paymentInformationRepository;

    @MockBean
    private ClientAuditRepository clientAuditRepository;

    @MockBean
    private OrderCostService orderCostService;

    @Test
    void testIsValidPickupInformationOk() {
        OrderInformationDTO orderInformationDTO = new OrderInformationDTO();
        orderInformationDTO.setPhoneNumber("+37012345678");
        orderInformationDTO.setAddress("Vilnius");
        orderInformationDTO.setEmail("test@test.com");

        Client client = new Client();
        client.setMobile("+37012345678");
        client.setAddress("Vilnius");
        client.setEmail("test@test.com");

        boolean isValid = orderService.isValidPickupInformation(orderInformationDTO, client);

        Assertions.assertTrue(isValid);
    }

    @Test
    void testIsValidPickupInformationNotOk() {
        testIsValidPickupInformationNotOkTest("", "Kaunas", "labas@labas.com");
        testIsValidPickupInformationNotOkTest("37011111111", "", "labas@labas.com");
        testIsValidPickupInformationNotOkTest("37011111111", "Kaunas", "");
        testIsValidPickupInformationNotOkTest("", "", "");
    }

    @Test
    void testSaveOrderOk() throws EmptyPaymentInformation {
        Order newOrder = new Order();
        newOrder.setPaymentType(PaymentType.DEBIT_CREDIT_CARD);

        PaymentInformation paymentInformation = new PaymentInformation();
        paymentInformation.setCardOwner("test test");
        paymentInformation.setCardNumber("1234123412341234");
        paymentInformation.setSecurityCode("111");
        paymentInformation.setValidThru("02/21");

        when(orderRepository.save(any()))
                .thenAnswer(invocation -> {
                    Order order = new Order();
                    order.setId(123L);

                    return order;
                });

        when(paymentInformationRepository.save(any()))
                .thenAnswer(invocation -> new PaymentInformation());

        Order savedOrder = orderService.saveOrder(newOrder, paymentInformation);

        Assertions.assertEquals(123L, savedOrder.getId());

        verify(orderRepository, times(1))
                .save(any());
        verify(paymentInformationRepository, times(1))
                .save(any());
    }

    @Test
    void testSaveOrderOkNotCard() throws EmptyPaymentInformation {
        Order newOrder = new Order();
        newOrder.setPaymentType(PaymentType.CASH);

        when(orderRepository.save(any()))
                .thenAnswer(invocation -> {
                    Order order = new Order();
                    order.setId(123L);

                    return order;
                });

        Order savedOrder = orderService.saveOrder(newOrder, new PaymentInformation());

        Assertions.assertEquals(123L, savedOrder.getId());

        verify(orderRepository, times(1))
                .save(any());
        verify(paymentInformationRepository, times(0))
                .save(any());
    }

    @Test
    void testSaveOrderEmptyPaymentInformation1() {
        Order newOrder = new Order();
        newOrder.setPaymentType(PaymentType.DEBIT_CREDIT_CARD);

        when(orderRepository.save(any()))
                .thenAnswer(invocation -> {
                    Order order = new Order();
                    order.setId(123L);

                    return order;
                });

        Assertions.assertThrows(EmptyPaymentInformation.class,
                () -> orderService.saveOrder(newOrder, new PaymentInformation()));
    }

    @Test
    void testSaveOrderEmptyPaymentInformation2() {
        Order newOrder = new Order();
        newOrder.setPaymentType(PaymentType.DEBIT_CREDIT_CARD);

        when(orderRepository.save(any()))
                .thenAnswer(invocation -> {
                    Order order = new Order();
                    order.setId(123L);

                    return order;
                });

        PaymentInformation paymentInformation = new PaymentInformation();
        paymentInformation.setCardOwner("test test");

        Assertions.assertThrows(EmptyPaymentInformation.class,
                () -> orderService.saveOrder(newOrder, paymentInformation));
    }

    @Test
    void testSaveOrderEmptyPaymentInformation3() {
        Order newOrder = new Order();
        newOrder.setPaymentType(PaymentType.DEBIT_CREDIT_CARD);

        when(orderRepository.save(any()))
                .thenAnswer(invocation -> {
                    Order order = new Order();
                    order.setId(123L);

                    return order;
                });

        PaymentInformation paymentInformation = new PaymentInformation();
        paymentInformation.setCardOwner("test test");
        paymentInformation.setCardNumber("1111111111111111");

        Assertions.assertThrows(EmptyPaymentInformation.class,
                () -> orderService.saveOrder(newOrder, paymentInformation));
    }

    @Test
    void testSaveOrderEmptyPaymentInformation4() {
        Order newOrder = new Order();
        newOrder.setPaymentType(PaymentType.DEBIT_CREDIT_CARD);

        when(orderRepository.save(any()))
                .thenAnswer(invocation -> {
                    Order order = new Order();
                    order.setId(123L);

                    return order;
                });

        PaymentInformation paymentInformation = new PaymentInformation();
        paymentInformation.setCardOwner("test test");
        paymentInformation.setCardNumber("1111111111111111");
        paymentInformation.setSecurityCode("111");

        Assertions.assertThrows(EmptyPaymentInformation.class,
                () -> orderService.saveOrder(newOrder, paymentInformation));
    }

    @Test
    void testSaveOrderEmptyPaymentInformationNotCard() throws EmptyPaymentInformation {
        Order newOrder = new Order();
        newOrder.setPaymentType(PaymentType.CASH);

        when(orderRepository.save(any()))
                .thenAnswer(invocation -> {
                    Order order = new Order();
                    order.setId(123L);

                    return order;
                });

        Order savedOrder = orderService.saveOrder(newOrder, new PaymentInformation());

        Assertions.assertEquals(123L, savedOrder.getId());

        verify(orderRepository, times(1))
                .save(any());
        verify(paymentInformationRepository, times(0))
                .save(any());
    }

    private void testIsValidPickupInformationNotOkTest(String orderPhoneNumber, String orderAddress, String orderEmail) {
        OrderInformationDTO orderInformationDTO = new OrderInformationDTO();
        orderInformationDTO.setPhoneNumber(orderPhoneNumber);
        orderInformationDTO.setAddress(orderAddress);
        orderInformationDTO.setEmail(orderEmail);

        Client client = new Client();
        client.setMobile("+37011111111");
        client.setAddress("Kaunas");
        client.setEmail("labas@labas.com");

        boolean isValid = orderService.isValidPickupInformation(orderInformationDTO, client);

        Assertions.assertFalse(isValid);
    }

}
