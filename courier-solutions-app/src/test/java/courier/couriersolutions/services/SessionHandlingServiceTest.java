package courier.couriersolutions.services;

import courier.couriersolutions.entities.Client;
import courier.couriersolutions.entities.Session;
import courier.couriersolutions.repository.ClientRepository;
import courier.couriersolutions.repository.ClientAuditRepository;
import courier.couriersolutions.repository.OrderRepository;
import courier.couriersolutions.repository.PaymentInformationRepository;
import courier.couriersolutions.repository.SessionRepository;
import javassist.NotFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Optional;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.times;

@WebMvcTest(SessionHandlingService.class)
class SessionHandlingServiceTest {

    @Autowired
    private SessionHandlingService sessionHandlingService;

    @MockBean
    private SessionRepository sessionRepository;

    @MockBean
    private ClientRepository clientRepository;

    @MockBean
    private OrderRepository orderRepository;

    @MockBean
    private PaymentInformationRepository paymentInformationRepository;

    @MockBean
    private ClientAuditRepository clientAuditRepository;

    @MockBean
    private OrderCostService orderCostService;

    private Client client;

    @BeforeEach
    public void init() {
        client = new Client();
    }

    @Test
    void testCreateSessionKey() {
        when(sessionRepository.findByClient(any()))
                .thenAnswer(invocation -> {
                    Session session = new Session();
                    return Optional.of(session);
                });

        when(sessionRepository.save(any()))
                .thenAnswer(invocation -> new Session(client, UUID.randomUUID().toString()));

        sessionHandlingService.createSessionKey(client);

        verify(sessionRepository, times(1))
                .save(any());

        verify(sessionRepository, times(1))
                .delete(any());

        verify(clientAuditRepository, times(1))
                .save(any());
    }

    @Test
    void testCreateSessionKeyNotPresent() {
        when(sessionRepository.findByClient(any()))
                .thenAnswer(invocation -> Optional.empty());

        when(sessionRepository.save(any()))
                .thenAnswer(invocation -> new Session(client, UUID.randomUUID().toString()));

        sessionHandlingService.createSessionKey(client);

        verify(sessionRepository, times(1))
                .save(any());
    }

    @Test
    void testIsValidSessionActiveSession() {
        when(sessionRepository.findByClientAndSessionKey(any(), any()))
                .thenAnswer(invocation -> {
                    Session session = new Session();
                    return Optional.of(session);
                });

        boolean validSession = sessionHandlingService.isValidSession(client, UUID.randomUUID().toString());

        verify(sessionRepository, times(1))
                .save(any());

        Assertions.assertTrue(validSession);
    }

    @Test
    void testIsValidSessionDisabledSession() {
        when(sessionRepository.findByClientAndSessionKey(any(), any()))
                .thenAnswer(invocation -> Optional.empty());

        boolean validSession = sessionHandlingService.isValidSession(client, UUID.randomUUID().toString());

        verify(sessionRepository, times(0))
                .save(any());

        Assertions.assertFalse(validSession);
    }

    @Test
    void testGetActiveSessionBySessKey() throws Exception {
        String sesskey = UUID.randomUUID().toString();
        when(sessionRepository.findBySessionKey(any()))
                .thenAnswer(invocation -> {
                    Session session = new Session();
                    session.setSessionKey(sesskey);
                    return Optional.of(session);
                });

        Session session = sessionHandlingService.getActiveSession(sesskey);

        verify(sessionRepository, times(1))
                .save(any());

        Assertions.assertEquals(sesskey, session.getSessionKey());
    }

    @Test
    void testGetClientsActiveSession() throws Exception {
        String sesskey = UUID.randomUUID().toString();
        when(sessionRepository.findByClient(any()))
                .thenAnswer(invocation -> {
                    Session session = new Session();
                    session.setSessionKey(sesskey);
                    return Optional.of(session);
                });

        Session session = sessionHandlingService.getClientsActiveSession(client);

        verify(sessionRepository, times(1))
                .save(any());

        Assertions.assertEquals(sesskey, session.getSessionKey());
    }

    @Test
    void testGetActiveSessionBySessKeyNotFoundException() {
        when(sessionRepository.findBySessionKey(any()))
                .thenAnswer(invocation -> Optional.empty());

        Assertions.assertThrows(NotFoundException.class, () -> sessionHandlingService.getActiveSession(UUID.randomUUID().toString()));

        verify(sessionRepository, times(0))
                .save(any());
    }

    @Test
    void testGetClientsActiveSessionNotFoundException() {
        when(sessionRepository.findByClient(any()))
                .thenAnswer(invocation -> Optional.empty());

        Assertions.assertThrows(NotFoundException.class, () -> sessionHandlingService.getClientsActiveSession(client));

        verify(sessionRepository, times(0))
                .save(any());
    }

    @Test
    void testDisableSessionForClient() throws Exception {
        when(sessionRepository.findByClient(any()))
                .thenAnswer(invocation -> {
                    Session session = new Session();
                    return Optional.of(session);
                });

        sessionHandlingService.disableSessionForClient(client);

        verify(sessionRepository, times(1))
                .delete(any());

        verify(clientAuditRepository, times(1))
                .save(any());
    }

    @Test
    void testDisableSessionForClientNotFoundException() {
        when(sessionRepository.findByClient(any()))
                .thenAnswer(invocation -> Optional.empty());

        Assertions.assertThrows(NotFoundException.class, () -> sessionHandlingService.disableSessionForClient(client));

        verify(sessionRepository, times(0))
                .save(any());
    }

}
