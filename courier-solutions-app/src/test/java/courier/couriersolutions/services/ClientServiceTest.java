package courier.couriersolutions.services;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import courier.couriersolutions.dto.ClientDTO;
import courier.couriersolutions.exceptions.ClientAlreadyExistsException;
import courier.couriersolutions.exceptions.PasswordsDontMatchException;
import courier.couriersolutions.repository.ClientAuditRepository;
import courier.couriersolutions.repository.ClientRepository;
import courier.couriersolutions.repository.OrderRepository;
import courier.couriersolutions.repository.PaymentInformationRepository;
import courier.couriersolutions.repository.SessionRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;

@WebMvcTest(ClientService.class)
class ClientServiceTest {

    @MockBean
    private SessionRepository sessionRepository;

    @MockBean
    private ClientRepository clientRepository;

    @MockBean
    private OrderRepository orderRepository;

    @MockBean
    private PaymentInformationRepository paymentInformationRepository;

    @MockBean
    private ClientAuditRepository clientAuditRepository;

    @Autowired
    private ClientService clientService;

    @Test
    void passwordsDontMatchExceptionTest() {
        ClientDTO clientDTO = new ClientDTO();
        clientDTO.setPassword("gai");
        clientDTO.setRepeatPassword("dys");

        Assertions.assertThrows(PasswordsDontMatchException.class, () -> {
            clientService.validate(clientDTO);
        });
    }

    @Test
    void userAlreadyExistsExceptionTest() {
        when(clientRepository.existsByEmail(any())).thenAnswer(invocation -> true);

        ClientDTO clientDTO = new ClientDTO();
        clientDTO.setPassword("gai");
        clientDTO.setRepeatPassword("gai");
        Assertions
            .assertThrows(ClientAlreadyExistsException.class, () -> {
                clientService.validate(clientDTO);
            });
    }

    @Test
    void clientServiceValidationTest() throws Exception {
        when(clientRepository.existsByEmail(any())).thenAnswer(invocation -> false);

        ClientDTO clientDTO = new ClientDTO();
        clientDTO.setPassword("gai");
        clientDTO.setRepeatPassword("gai");
        clientService.validate(clientDTO);

        verify(clientRepository, times(1))
            .existsByEmail(any());
    }

}
