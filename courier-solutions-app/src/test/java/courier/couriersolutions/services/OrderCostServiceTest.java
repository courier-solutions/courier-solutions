package courier.couriersolutions.services;

import courier.couriersolutions.configuration.OrderCostConfig;
import courier.couriersolutions.constants.DeliveryType;
import courier.couriersolutions.constants.ParcelType;
import courier.couriersolutions.dto.OrderDTO;
import courier.couriersolutions.dto.OrderInformationDTO;
import courier.couriersolutions.dto.ParcelDTO;
import courier.couriersolutions.repository.ClientAuditRepository;
import courier.couriersolutions.repository.ClientRepository;
import courier.couriersolutions.repository.OrderRepository;
import courier.couriersolutions.repository.PaymentInformationRepository;
import courier.couriersolutions.repository.SessionRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.runner.ApplicationContextRunner;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;
import java.util.Collections;

@WebMvcTest(OrderCostService.class)
class OrderCostServiceTest {
    @Autowired
    private OrderService orderService;

    @MockBean
    private SessionRepository sessionRepository;

    @MockBean
    private ClientRepository clientRepository;

    @MockBean
    private OrderRepository orderRepository;

    @MockBean
    private PaymentInformationRepository paymentInformationRepository;

    @MockBean
    private ClientAuditRepository clientAuditRepository;

    @MockBean
    private OrderCostService orderCostService;

    private final ApplicationContextRunner contextRunner = new ApplicationContextRunner();

    @Test
    void testStandardOrderCostServiceStandardTodayStandardShipping() {
        OrderDTO newOrder = getOrderDTO(ParcelType.STANDARD, LocalDate.now(), DeliveryType.STANDARD_SHIPPING);

        contextRunner.withPropertyValues("orders.pricing=standard")
                .withUserConfiguration(OrderCostConfig.class)
                .run(context -> {
                    OrderCostService orderCostService = context.getBean(StandardOrderCostService.class);
                    Float cost = orderCostService.calculateOrderCost(newOrder);

                    Assertions.assertEquals(47.41f, cost);
                });
    }

    @Test
    void testReducedOrderCostServiceDocumentsOtherDayFastShipping() {
        OrderDTO newOrder = getOrderDTO(ParcelType.DOCUMENTS, LocalDate.now().plusDays(1), DeliveryType.FAST_SHIPPING);

        contextRunner.withPropertyValues("orders.pricing=reduced")
                .withUserConfiguration(OrderCostConfig.class)
                .run(context -> {
                    OrderCostService orderCostService = context.getBean(ReducedOrderCostService.class);
                    Float cost = orderCostService.calculateOrderCost(newOrder);

                    Assertions.assertEquals(42.97f, cost);
                });
    }

    @Test
    void testStandardOrderCostFragileTodayStandardShipping() {
        OrderDTO newOrder = getOrderDTO(ParcelType.FRAGILE, LocalDate.now(), DeliveryType.STANDARD_SHIPPING);

        contextRunner.withPropertyValues("orders.pricing=standard")
                .withUserConfiguration(OrderCostConfig.class)
                .run(context -> {
                    OrderCostService orderCostService = context.getBean(StandardOrderCostService.class);
                    Float cost = orderCostService.calculateOrderCost(newOrder);

                    Assertions.assertEquals(58.11f, cost);
                });
    }

    @Test
    void testStandardOrderCostAnimalsTodayStandardShipping() {
        OrderDTO newOrder = getOrderDTO(ParcelType.ANIMALS, LocalDate.now(), DeliveryType.STANDARD_SHIPPING);

        contextRunner.withPropertyValues("orders.pricing=standard")
                .withUserConfiguration(OrderCostConfig.class)
                .run(context -> {
                    OrderCostService orderCostService = context.getBean(StandardOrderCostService.class);
                    Float cost = orderCostService.calculateOrderCost(newOrder);

                    Assertions.assertEquals(68.81f, cost);
                });
    }

    // TODO enable this test when ths is fixed https://ba-jira.atlassian.net/browse/CS-36
//    @Test
//    void testStandardOrderCostBelowThreeEuros() {
//        OrderDTO newOrder = getOrderDTOBelowThreeEuros();
//
//        contextRunner.withPropertyValues("orders.pricing=standard")
//                .withUserConfiguration(OrderCostConfig.class)
//                .run(context -> {
//                    OrderCostService orderCostService = context.getBean(StandardOrderCostService.class);
//                    Float cost = orderCostService.calculateOrderCost(newOrder);
//
//                    Assertions.assertEquals(28f, cost);
//                });
//    }

    private OrderDTO getOrderDTO(ParcelType parcelType, LocalDate pickupDate, DeliveryType deliveryType) {
        ParcelDTO newParcel = new ParcelDTO();
        newParcel.setType(parcelType);
        newParcel.setLength(1.01f);
        newParcel.setWidth(2f);
        newParcel.setHeight(1.01f);
        newParcel.setWeight(1f);
        newParcel.setQuantity(2);

        OrderInformationDTO orderInformation = new OrderInformationDTO();
        orderInformation.setPostalCode("00000");

        OrderDTO newOrder = new OrderDTO();
        newOrder.setPickupInfo(orderInformation);
        newOrder.setDeliveryInfo(orderInformation);
        newOrder.setParcelList(Collections.singletonList(newParcel));
        newOrder.setPickupDate(pickupDate);
        newOrder.setDeliveryType(deliveryType);

        return newOrder;
    }

    private OrderDTO getOrderDTOBelowThreeEuros() {
        ParcelDTO newParcel = new ParcelDTO();
        newParcel.setType(ParcelType.STANDARD);
        newParcel.setLength(0.5f);
        newParcel.setWidth(0.5f);
        newParcel.setHeight(0.5f);
        newParcel.setWeight(1f);
        newParcel.setQuantity(1);

        OrderInformationDTO orderInformation = new OrderInformationDTO();
        orderInformation.setPostalCode("00000");

        OrderDTO newOrder = new OrderDTO();
        newOrder.setPickupInfo(orderInformation);
        newOrder.setDeliveryInfo(orderInformation);
        newOrder.setParcelList(Collections.singletonList(newParcel));
        newOrder.setPickupDate(LocalDate.now());
        newOrder.setDeliveryType(DeliveryType.STANDARD_SHIPPING);

        return newOrder;
    }
}
