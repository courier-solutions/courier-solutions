package courier.couriersolutions.services;

import courier.couriersolutions.entities.Session;
import courier.couriersolutions.repository.ClientRepository;
import courier.couriersolutions.repository.ClientAuditRepository;
import courier.couriersolutions.repository.OrderRepository;
import courier.couriersolutions.repository.PaymentInformationRepository;
import courier.couriersolutions.repository.SessionRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.times;

@WebMvcTest(SessionRefreshService.class)
class SessionRefreshServiceTest {

    @Autowired
    private SessionRefreshService sessionRefreshService;

    @MockBean
    private SessionRepository sessionRepository;

    @MockBean
    private ClientRepository clientRepository;

    @MockBean
    private OrderRepository orderRepository;

    @MockBean
    private PaymentInformationRepository paymentInformationRepository;

    @MockBean
    private ClientAuditRepository clientAuditRepository;

    @MockBean
    private OrderCostService orderCostService;

    @Test
    void testSessionRefresh() {
        when(sessionRepository.findAllByUpdateDateTimeBefore(any()))
                .thenAnswer(invocation -> {
                    Session session = new Session();
                    return Collections.singletonList(session);
                });

        sessionRefreshService.sessionRefresh();

        verify(sessionRepository, times(1))
                .deleteAll(any());
    }

    @Test
    void testSessionRefreshNotFound() {
        when(sessionRepository.findAllByUpdateDateTimeBefore(any()))
                .thenAnswer(invocation -> new ArrayList<>());

        sessionRefreshService.sessionRefresh();

        verify(sessionRepository, times(0))
                .saveAll(any());
    }

}
