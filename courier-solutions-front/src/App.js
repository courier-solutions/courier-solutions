import './App.css';
import {Route, Switch} from "react-router-dom";
import {Container} from "react-bootstrap";
import Header from "./Header";
import Footer from "./Footer";
import Register from "./components/Register";
import Login from "./components/Login";
import Logout from "./components/Logout";
import ViewOrders from "./components/order/OrderView";
import OrderDetails from "./components/order/OrderDetails";
import OrderCreate from "./components/order/OrderCreate";
import EditOrder from "./components/order/OrderEdit";
import PriceCheck from "./components/order/OrderPriceCheck";
import Profile from "./components/client/Profile";
import AuthRoute from "./utils/AuthRoute";
import Home from "./components/Home";
import NotFound from "./components/NotFound";

function App() {
    return (
        <div className={"d-flex flex-column min-vh-100"}>
            <Header/>
            <main>
                <Container className={"my-3"}>
                    <Switch>
                        <Route path={"/"} exact component={Home}/>
                        <Route path={"/login"} component={Login}/>
                        <Route path={"/register"} component={Register}/>
                        <AuthRoute path={"/view-orders"}>
                            <ViewOrders/>
                        </AuthRoute>
                        <AuthRoute path={"/create-order"}>
                            <OrderCreate/>
                        </AuthRoute>
                        <AuthRoute path={"/order-details"}>
                            <OrderDetails/>
                        </AuthRoute>
                        <AuthRoute path={"/price-check"}>
                            <PriceCheck/>
                        </AuthRoute>
                        <AuthRoute path={"/profile"}>
                            <Profile/>
                        </AuthRoute>
                        <AuthRoute path={"/edit-order"}>
                            <EditOrder/>
                        </AuthRoute>
                        <AuthRoute path={"/logout"}>
                            <Logout/>
                        </AuthRoute>
                        <Route path={"*"} component={NotFound}/>
                    </Switch>
                </Container>
            </main>
            <Footer/>
        </div>
    );
}

export default App;
