import {PARCEL_TYPE} from "../../constants/ParcelType";
import {Button, Col, Form, Row} from "react-bootstrap";
import React from "react";

export function Parcel({parcel, index, handleChange, handleRemove, cost}) {
    return (
        <React.Fragment>
            <Row>
                <Col>
                    <h5>Package {index + 1}</h5>
                </Col>
                <Col>
                    <b>Cost: {cost} €</b>
                </Col>
            </Row>
            {index !== 0 &&
            <Button variant={"danger"} type="button" onClick={() => handleRemove(index)}>Remove package</Button>}

            <Row xs={1} md={2} lg={4}>
                <Form.Group as={Col} controlId={"length"}>
                    <Form.Label>Package length (m)</Form.Label>
                    <Form.Control name={"length"} placeholder={"Enter length"} value={parcel.length}
                                  onChange={(e) => handleChange(e, index)} pattern={"^\\d+(\\.\\d+)?$"} required/>
                </Form.Group>

                <Form.Group as={Col} controlId={"width"}>
                    <Form.Label>Package width (m)</Form.Label>
                    <Form.Control name={"width"} placeholder={"Enter width"} value={parcel.width}
                                  onChange={(e) => handleChange(e, index)} pattern={"^\\d+(\\.\\d+)?$"} required/>
                </Form.Group>

                <Form.Group as={Col} controlId={"height"}>
                    <Form.Label>Package height (m)</Form.Label>
                    <Form.Control name={"height"} placeholder={"Enter height"} value={parcel.height}
                                  onChange={(e) => handleChange(e, index)} pattern={"^\\d+(\\.\\d+)?$"} required/>
                </Form.Group>

                <Form.Group as={Col} controlId={"weight"}>
                    <Form.Label>Package weight (kg)</Form.Label>
                    <Form.Control name={"weight"} placeholder={"Enter weight"} value={parcel.weight}
                                  onChange={(e) => handleChange(e, index)} pattern={"^\\d+(\\.\\d+)?$"} required/>
                </Form.Group>
            </Row>

            <Row lg={4}>
                <Form.Group as={Col} controlId={"type"}>
                    <Form.Label>Package type</Form.Label>
                    <Form.Control name={"type"} as={"select"} value={parcel.type}
                                  onChange={(e) => handleChange(e, index)} custom>
                        {Object.keys(PARCEL_TYPE).map(key => (
                            <option key={key} value={key}>{PARCEL_TYPE[key]}</option>
                        ))}
                    </Form.Control>
                </Form.Group>

                <Form.Group as={Col} controlId={"quantity"}>
                    <Form.Label>Quantity</Form.Label>
                    <Form.Control name={"quantity"} type={"number"} value={parcel.quantity}
                                  onChange={(e) => handleChange(e, index)} min={"1"}/>
                </Form.Group>
            </Row>
        </React.Fragment>
    );
}