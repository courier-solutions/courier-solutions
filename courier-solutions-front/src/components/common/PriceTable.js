import React from "react";
import {Table} from "react-bootstrap";

export default function PriceTable() {
    return (
        <React.Fragment>
            <Table striped bordered>
                <thead>
                <tr>
                    <th>Package type</th>
                    <th>Multiplier</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Standard</td>
                    <td>1</td>
                </tr>
                <tr>
                    <td>Documents</td>
                    <td>1.1</td>
                </tr>
                <tr>
                    <td>Fragile</td>
                    <td>1.5</td>
                </tr>
                <tr>
                    <td>Animals</td>
                    <td>2</td>
                </tr>
                </tbody>
            </Table>
            <Table striped bordered>
                <thead>
                <tr>
                    <th>Dimension</th>
                    <th>Price per m^3</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>1 m^3</td>
                    <td>5 €</td>
                </tr>
                </tbody>
            </Table>
            <Table striped bordered>
                <thead>
                <tr>
                    <th>Weight</th>
                    <th>Price per kg</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>1 kg</td>
                    <td>0.5 €</td>
                </tr>
                </tbody>
            </Table>
            <Table striped bordered>
                <thead>
                <tr>
                    <th>Delivery distance</th>
                    <th>Price per km</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>1 - 20 km</td>
                    <td>0.1 €</td>
                </tr>
                <tr>
                    <td>20 - 100 km</td>
                    <td>0.15 €</td>
                </tr>
                <tr>
                    <td>100+ km</td>
                    <td>0.2 €</td>
                </tr>
                </tbody>
            </Table>
            <Table striped bordered>
                <thead>
                <tr>
                    <th>Pickup date</th>
                    <th>Price</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Today</td>
                    <td>1 €</td>
                </tr>
                <tr>
                    <td>Next day</td>
                    <td>0 €</td>
                </tr>
                </tbody>
            </Table>
            <Table striped bordered>
                <thead>
                <tr>
                    <th>Delivery time</th>
                    <th>Price</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Standard (within 10 working days)</td>
                    <td>0 €</td>
                </tr>
                <tr>
                    <td>Fast (2 - 3 working days)</td>
                    <td>2 €</td>
                </tr>
                </tbody>
            </Table>
        </React.Fragment>
    );
}