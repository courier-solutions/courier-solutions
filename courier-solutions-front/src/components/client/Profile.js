import {Alert, Button, Col, Form, InputGroup, Row, Modal} from "react-bootstrap";
import React, {useEffect, useState} from "react";
import {getClient, putClientData, putClientPass} from "../../services/client.service";
import {FETCH_STATUS} from "../../constants/FetchStatus";
import {useFormInput} from "../../utils/FormInputHook";

export default function Profile() {
    const [client, setClient] = useState({});
    const [fetchStatus, setFetchStatus] = useState(FETCH_STATUS.LOADING);
    const [validPass, setValidPass] = useState(true);
    const [repeatGood, setRepeatGood] = useState(true);
    const [infoSaveGood, setInfoSaveGood] = useState(null);
    const [passSaveGood, setPassSaveGood] = useState(null);

    const [firstName, setFirstName, changeFirstName] = useFormInput("");
    const [lastName, setLastName, changeLastName] = useFormInput("");
    const [address, setAddress, changeAddress] = useFormInput("");
    const [mobile, setMobile, changeMobile] = useFormInput("");
    const [currentPass, setCurrentPass] = useFormInput("");
    const [newPass, setNewPass] = useFormInput("");
    const [repPass, setRepPass] = useFormInput("");

    const [modalText, setModalText] = useState(null);

    function getNewClient() {
        return {
            first_name: firstName,
            last_name: lastName,
            address: address,
            mobile: `+370${mobile}`
        };
    }

    function handleCancelEditing() {
        window.location.reload();
    }

    function handleOverwrite() {
        setModalText(null);
        setInfoSaveGood(null);

        putClientData(getNewClient(), true).then((res) => {
            setInfoSaveGood(true);
        }).catch((err) => {
            setInfoSaveGood(false);
        });
    }

    function handleInfoSubmit(e) {
        e.preventDefault();

        setInfoSaveGood(null);

        putClientData(getNewClient(), false).then((res) => {
            setInfoSaveGood(true);
        }).catch((err) => {
            setInfoSaveGood(false);

            if (err.errorName && err.errorName === 'CS_OPT_LOCK_ERROR') {
                setModalText((err.text) ? err.text : 'Server error. Please try again later.');
            }
        });
    }

    function handlePassSubmit(e) {
        e.preventDefault();

        setPassSaveGood(null);
        setValidPass(true);
        setRepeatGood(true);

        if (currentPass !== client.password) {
            setValidPass(false);
            return;
        }

        if (newPass !== repPass) {
            setRepeatGood(false);
            return;
        }

        const newClientPass = {
            current_password: currentPass,
            new_password: newPass,
            repeat_new_password: repPass
        }

        putClientPass(newClientPass).then((res) => {
            setPassSaveGood(true);
            setClient(res);
        }).catch((err) => {
            setPassSaveGood(false);
        });
    }

    useEffect(() => {
        getClient().then((res) => {
            setClient(res);

            changeFirstName(res.first_name);
            changeLastName(res.last_name);
            changeAddress(res.address);
            changeMobile(res.mobile.substring(4));

            setFetchStatus(FETCH_STATUS.SUCCESS);
        }).catch((err) => {
            setFetchStatus(FETCH_STATUS.ERROR);
        })
    }, []);

    useEffect(() => {
        setInfoSaveGood(null);
    }, [firstName, lastName, address, mobile])

    useEffect(() => {
        setPassSaveGood(null);
    }, [currentPass, newPass, repPass])

    if (fetchStatus === FETCH_STATUS.LOADING) {
        return (
            <React.Fragment>
                <h3>Edit your information</h3>
                <Alert variant='info'>Loading your information. Please wait.</Alert>
            </React.Fragment>
        );
    }

    if (fetchStatus === FETCH_STATUS.ERROR) {
        return (
            <React.Fragment>
                <h3>Edit your information</h3>
                <Alert variant='danger'>Error loading your information. Please refresh the page.</Alert>
            </React.Fragment>
        );
    }

    if (fetchStatus === FETCH_STATUS.SUCCESS) {
        return (
            <React.Fragment>
                <h2>Edit your information</h2>
                <Form className={"mt-3"} onSubmit={handleInfoSubmit}>
                    <h4>Edit your personal details</h4>
                    {infoSaveGood && <Alert variant='success'>Update successful.</Alert>}
                    {infoSaveGood === false && <Alert variant='danger'>Error while updating information.</Alert>}
                    <Form.Row as={Row} xl={2} lg={1}>
                        <Col xl={3} lg={3}>
                            <Form.Group controlId={"fFirstName"}>
                                <Form.Label>First name</Form.Label>
                                <Form.Control type={"text"} value={firstName} onChange={setFirstName}
                                              placeholder={"Enter first name"} required/>
                            </Form.Group>
                        </Col>
                        <Col xl={3} lg={3}>
                            <Form.Group controlId={"fLastName"}>
                                <Form.Label>Last name</Form.Label>
                                <Form.Control type={"text"} value={lastName} onChange={setLastName}
                                              placeholder={"Enter last name"} required/>
                            </Form.Group>
                        </Col>
                    </Form.Row>
                    <Form.Group as={Col} lg={6} className={"px-0"} controlId={"fAddress"}>
                        <Form.Label>Address</Form.Label>
                        <Form.Control type={"text"} value={address} onChange={setAddress}
                                      placeholder={"Enter address"} required/>
                    </Form.Group>
                    <Form.Group controlId={"fMobile"}>
                        <Form.Label>Phone number</Form.Label>
                        <InputGroup>
                            <InputGroup.Prepend>
                                <InputGroup.Text>+370</InputGroup.Text>
                                <Form.Control type={"tel"} value={mobile} onChange={setMobile} minLength={"8"}
                                              maxLength={"8"} pattern={"^\\d+$"} placeholder={"Enter phone number"}
                                              required/>
                            </InputGroup.Prepend>
                        </InputGroup>
                    </Form.Group>
                    <Button variant="success" type={"submit"}>Save</Button>
                </Form>

                <Form className={"mt-5"} onSubmit={handlePassSubmit}>
                    <h4>Change password</h4>
                    {!validPass && <Alert variant='danger'>The current password is wrong.</Alert>}
                    {!repeatGood && <Alert variant='danger'>The new passwords don't match.</Alert>}
                    {passSaveGood && <Alert variant='success'>Update successful.</Alert>}
                    {passSaveGood === false && <Alert variant='danger'>Error while updating password.</Alert>}
                    <Form.Group as={Col} lg={6} className={"px-0"} controlId={"fCurrentPass"}>
                        <Form.Label>Current password</Form.Label>
                        <Form.Control type={"password"} value={currentPass} onChange={setCurrentPass} minLength={"8"}
                                      maxLength={"20"}
                                      placeholder={"Enter your current password"} required/>
                    </Form.Group>
                    <Form.Group as={Col} lg={6} className={"px-0"} controlId={"fNewPass"}>
                        <Form.Label>New password</Form.Label>
                        <Form.Control type={"password"} value={newPass} onChange={setNewPass}
                                      placeholder={"Enter your new password"} minLength={"8"} maxLength={"20"}
                                      pattern={"^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&-+=()]).{8,20}$"}
                                      required/>
                    </Form.Group>
                    <Form.Group as={Col} lg={6} className={"px-0"} controlId={"fRepPass"}>
                        <Form.Label>Confirm Password</Form.Label>
                        <Form.Control type={"password"} value={repPass} onChange={setRepPass}
                                      placeholder={"Confirm your new password"} minLength={"8"} maxLength={"20"}
                                      pattern={"^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&-+=()]).{8,20}$"}
                                      required/>
                    </Form.Group>
                    <Button variant="success" type={"submit"}>Save</Button>
                </Form>
                <Modal show={modalText} onHide={handleCancelEditing}>
                    <Modal.Header closeButton>
                        <Modal.Title>Action failed</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        The information you're editing has already changed.
                        Do you want to cancel and refresh the information or overwrite changes?
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={handleCancelEditing}>Cancel editing</Button>
                        <Button variant="danger" onClick={handleOverwrite}>Overwrite</Button>
                    </Modal.Footer>
                </Modal>
            </React.Fragment>
        );
    }
}