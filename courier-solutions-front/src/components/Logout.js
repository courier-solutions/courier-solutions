import React, {useEffect} from 'react';
import {Redirect} from "react-router-dom";
import {logout} from "../services/login.service";
import {useAuth} from "../context/AuthContext";

function Logout() {
    const auth = useAuth();

    useEffect(() => {
        auth.logout();
        logout();
    });

    return <Redirect to={'/login'} />;
}

export default Logout;