import {Jumbotron} from "react-bootstrap";
import {useLocation} from "react-router-dom";

function NotFound() {
    const {pathname} = useLocation();

    return (
        <Jumbotron>
            <h3>No match for <code>{pathname}</code>. Click <a href={"/"}>here</a> to return to homepage.</h3>
        </Jumbotron>
    );
}

export default NotFound;