import React, {useState} from 'react';
import {login} from "../services/login.service";
import {Alert, Button, Col, Form} from "react-bootstrap";
import {useHistory} from "react-router-dom";
import {useFormInput} from "../utils/FormInputHook";
import {useAuth} from "../context/AuthContext";

function Login() {
    const history = useHistory();
    const {setAuthenticated} = useAuth();

    const [username, setUsername] = useFormInput("");
    const [loginErr, setLoginErr] = useState(false);
    const [err, setErr] = useState(false);
    const [password, setPassword, changePassword] = useFormInput("");

    function handleSubmit(e) {
        e.preventDefault();

        const loginData = {
            username: username,
            password: password
        };

        login(loginData)
            .then(() => {
                setAuthenticated(true);
                history.push('/view-orders');
            })
            .catch((error) => {
                if (error.errorName === "CS_CLIENT_NOT_FOUND") {
                    setLoginErr(true);
                } else {
                    setErr(true);
                }
                changePassword("")
            });
    }

    return (
        <React.Fragment>
            {loginErr && <Alert variant='danger'>Wrong credentials. Please try again.</Alert>}
            {err && <Alert variant='danger'>System error. Please try again.</Alert>}
            <Form onSubmit={handleSubmit}>
                <h2>Welcome to Courier Solutions</h2>
                <Form.Group as={Col} md={6} lg={4} className={"px-0"} controlId={"email"}>
                    <Form.Label>Username</Form.Label>
                    <Form.Control type={"email"} value={username} onChange={setUsername}
                                  placeholder={"Enter email"} required/>
                </Form.Group>
                <Form.Group as={Col} md={6} lg={4} className={"px-0"} controlId={"password"}>
                    <Form.Label>Password</Form.Label>
                    <Form.Control type={"password"} value={password} onChange={setPassword}
                                  placeholder={"Enter password"} required/>
                </Form.Group>
                <Button type={"submit"}>Login</Button>
            </Form>
        </React.Fragment>
    );
}

export default Login;