import React, {useState} from 'react';
import {useHistory} from "react-router-dom";
import {Alert, Button, Col, Form} from "react-bootstrap";
import {register} from '../services/register.service'
import {useFormInput} from "../utils/FormInputHook";

function Register() {
    const history = useHistory();

    const [email, setEmail] = useFormInput("")
    const [firstName, setFirstName] = useFormInput("")
    const [lastName, setLastName] = useFormInput("")
    const [mobile, setMobile] = useFormInput("")
    const [address, setAddress] = useFormInput("")
    const [password, setPassword] = useFormInput("")
    const [repeatPassword, setRepeatPassword] = useFormInput("")
    const [successMessage, setSuccessMessage] = useState(null)
    const [err, setErr] = useState(null);

    function sendDetailsToServer() {
        const payload = {
            email: email,
            password: password,
            repeat_password: repeatPassword,
            first_name: firstName,
            last_name: lastName,
            address: address,
            mobile: mobile
        }
        register(payload)
            .then((response) => {
                setSuccessMessage("Registration successful. Redirecting to login page")
                setTimeout(() => { redirectToHome() }, 3000)
            })
            .catch((error) => {
                if (error.errorName === "CS_PASSWORDS_DONT_MATCH") {
                    setErr("Passwords do not match");
                }
                else if (error.errorName === "CS_CLIENT_ALREADY_EXISTS") {
                    setErr("User with this email already exists");
                } else {
                    setErr("Server error. Please try again later");
                }
            });
    }

    function redirectToHome() {
        history.push('/login');
    }

    function handleSubmitClick(e) {
        const regex = new RegExp("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&-+=()!]).{8,20}$");
        e.preventDefault();
        if (password !== repeatPassword) {
            setErr("Passwords do not match");
        } else if (!regex.test(password)) {
            setErr("Password must be between 8 and 20 symbols, contain uppercase, lowercase letter, number and a special character");
        } else {
            sendDetailsToServer()
        }
    }

    return (
        <React.Fragment>
            {err && <Alert variant='danger'>{err}</Alert>}
            {successMessage && <Alert variant='success'>{successMessage}</Alert>}
            <Form onSubmit={handleSubmitClick}>
                <h2>Welcome to Courier Solutions</h2>
                <Form.Group as={Col} className={"px-0"} md={6} lg={4} controlId={"email"}>
                    <Form.Label>Username</Form.Label>
                    <Form.Control type={"email"} value={email} onChange={setEmail}
                        placeholder={"Enter email"} required />
                </Form.Group>
                <Form.Group as={Col} className={"px-0"} md={6} lg={4} controlId={"password"}>
                    <Form.Label>Password</Form.Label>
                    <Form.Control type={"password"} value={password} onChange={(e) => { setPassword(e); setErr(null) }}
                        placeholder={"Enter password"} required />
                </Form.Group>
                <Form.Group as={Col} className={"px-0"} md={6} lg={4} controlId={"repeatPassword"}>
                    <Form.Label>Repeat Password</Form.Label>
                    <Form.Control type={"password"} value={repeatPassword} onChange={(e) => { setRepeatPassword(e); setErr(null) }}
                        placeholder={"Repeat password"} required />
                </Form.Group>
                <Form.Group as={Col} className={"px-0"} md={6} lg={4} controlId={"firstName"}>
                    <Form.Label>First Name</Form.Label>
                    <Form.Control type={"text"} value={firstName} onChange={setFirstName}
                        placeholder={"Enter your first name"} required />
                </Form.Group>
                <Form.Group as={Col} className={"px-0"} md={6} lg={4} controlId={"lastName"}>
                    <Form.Label>Last Name</Form.Label>
                    <Form.Control type={"text"} value={lastName} onChange={setLastName}
                        placeholder={"Enter your last name"} required />
                </Form.Group>
                <Form.Group as={Col} className={"px-0"} md={6} lg={4} controlId={"address"}>
                    <Form.Label>Address</Form.Label>
                    <Form.Control type={"text"} value={address} onChange={setAddress}
                        placeholder={"Enter your address"} required />
                </Form.Group>
                <Form.Group as={Col} className={"px-0"} md={6} lg={4} controlId={"mobile"}>
                    <Form.Label>Mobile</Form.Label>
                    <Form.Control type={"text"} value={mobile} onChange={setMobile}
                        placeholder={"Enter your mobile"} required />
                </Form.Group>
                <Button variant={"success"} type={"submit"}>Register</Button>
            </Form>
        </React.Fragment>
    )
}

export default Register;