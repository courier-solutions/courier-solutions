import React, {useState} from "react";
import {Alert, Button, Col, Form, InputGroup, Modal, Row} from "react-bootstrap";
import {editOrder} from "../../services/order.service";
import {Link, useHistory, useLocation} from "react-router-dom";
import {useFormInput} from "../../utils/FormInputHook";

export default function EditOrder() {

    const today = new Date().toISOString().split('T')[0];

    const history = useHistory();
    const order = useLocation().state;

    // Pickup vars
    const [pName, setPickupName] = useFormInput(order.pickup_information.name);
    const [pEmail, setPickupEmail] = useFormInput(order.pickup_information.email);
    const [pCity, setPickupCity] = useFormInput(order.pickup_information.city);
    const [pAddress, setPickupAddress] = useFormInput(order.pickup_information.address);
    const [pPostalCode, setPickupPostalCode] = useFormInput(order.pickup_information.postal_code);
    const [pPhoneNumber, setPickupPhoneNumber] = useFormInput(order.pickup_information.phone_number.replace('+370', ''));
    const [pDate, setPickupDate] = useFormInput(order.pickup_date);

    // Delivery vars
    const [dName, setDeliveryName] = useFormInput(order.delivery_information.name);
    const [dEmail, setDeliveryEmail] = useFormInput(order.delivery_information.email);
    const [dCity, setDeliveryCity] = useFormInput(order.delivery_information.city);
    const [dAddress, setDeliveryAddress] = useFormInput(order.delivery_information.address);
    const [dPostalCode, setDeliveryPostalCode] = useFormInput(order.delivery_information.postal_code);
    const [dPhoneNumber, setDeliveryPhoneNumber] = useFormInput(order.delivery_information.phone_number.replace('+370', ''));
    const [dType, setDeliveryType] = useFormInput(order.delivery_type);

    const [comment, setComment] = useFormInput(order.comment);

    const [modalText, setModalText] = useState(null);
    const [successMessage, setSuccessMessage] = useState(null);

    function handleModalClose() {
        setModalText(null);
    }

    function handleSubmit(e) {
        e.preventDefault()

        const editedOrder = {
            id: order.id,
            parcels: order.parcels,
            pickup_information: {
                name: pName,
                email: pEmail,
                city: pCity,
                address: pAddress,
                postal_code: pPostalCode,
                phone_number: `+370${pPhoneNumber}`,
            },
            pickup_date: pDate,
            delivery_information: {
                name: dName,
                email: dEmail,
                city: dCity,
                address: dAddress,
                postal_code: dPostalCode,
                phone_number: `+370${dPhoneNumber}`,
            },
            delivery_type: dType,
            comment: comment,
            payment_type: order.payment_type
        };

        editOrder(editedOrder).then(res => {
            setSuccessMessage("Registration successful. Redirecting to orders page.")
            setTimeout(() => {
                history.push('/view-orders');
            }, 3000)
        }).catch(err => {
            setModalText((err.text) ? err.text : 'Server error. Please try again later.');
        });
    }

    return (
        <React.Fragment>
            <Link className={'d-block mb-2'} to={{
                pathname: "/order-details",
                state: order
            }}>{"<"} Go back</Link>

            <Form onSubmit={handleSubmit} className={'mb-3'}>
                <h1>Edit Package Info</h1>

                <Row>
                    <Col md={6}>
                        <h2>Pickup Info</h2>

                        <Form.Group controlId={"pName"}>
                            <Form.Label>Full name</Form.Label>
                            <Form.Control type={"text"} value={pName} onChange={setPickupName}
                                          placeholder={"Enter name"} required/>
                        </Form.Group>

                        <Form.Group controlId={"formBasicEmail"}>
                            <Form.Label>Email address</Form.Label>
                            <Form.Control type={"email"} value={pEmail} onChange={setPickupEmail}
                                          placeholder={"Enter email"} required/>
                        </Form.Group>

                        <Form.Group controlId={"pCity"}>
                            <Form.Label>City</Form.Label>
                            <Form.Control type={"text"} value={pCity} onChange={setPickupCity}
                                          placeholder={"Enter city"} required/>
                        </Form.Group>

                        <Form.Group controlId={"pAddress"}>
                            <Form.Label>Address</Form.Label>
                            <Form.Control type={"text"} value={pAddress} onChange={setPickupAddress}
                                          placeholder={"Enter address"} required/>
                        </Form.Group>

                        <Form.Group controlId={"pPostalCode"}>
                            <Form.Label>Postal code</Form.Label>
                            <Form.Control type={"text"} value={pPostalCode} onChange={setPickupPostalCode}
                                          minLength={"5"} maxLength={"5"} pattern={"^\\d+$"}
                                          placeholder={"Enter postal code"} required/>
                        </Form.Group>

                        <Form.Group controlId={"pPhoneNumber"}>
                            <Form.Label>Phone number</Form.Label>
                            <InputGroup>
                                <InputGroup.Prepend>
                                    <InputGroup.Text>+370</InputGroup.Text>
                                </InputGroup.Prepend>
                                <Form.Control type={"tel"} value={pPhoneNumber} onChange={setPickupPhoneNumber}
                                              minLength={"8"} maxLength={"8"} pattern={"^\\d+$"}
                                              placeholder={"Enter phone number"} required/>
                            </InputGroup>
                        </Form.Group>

                        <Form.Group controlId={"pDate"}>
                            <Form.Label>Pickup date</Form.Label>
                            <Form.Control type={"date"} value={pDate} onChange={setPickupDate} min={today}
                                          placeholder={"Enter pickup date"} required/>
                        </Form.Group>
                    </Col>

                    <Col md={6}>
                        <h2>Delivery Info</h2>

                        <Form.Group controlId={"dName"}>
                            <Form.Label>Full name</Form.Label>
                            <Form.Control type={"text"} value={dName} onChange={setDeliveryName}
                                          placeholder={"Enter name"} required/>
                        </Form.Group>

                        <Form.Group controlId={"formBasicEmail"}>
                            <Form.Label>Email address</Form.Label>
                            <Form.Control type={"email"} value={dEmail} onChange={setDeliveryEmail}
                                          placeholder={"Enter email"} required/>
                        </Form.Group>

                        <Form.Group controlId={"dCity"}>
                            <Form.Label>City</Form.Label>
                            <Form.Control type={"text"} value={dCity} onChange={setDeliveryCity}
                                          placeholder={"Enter city"} required/>
                        </Form.Group>

                        <Form.Group controlId={"dAddress"}>
                            <Form.Label>Address</Form.Label>
                            <Form.Control type={"text"} value={dAddress} onChange={setDeliveryAddress}
                                          placeholder={"Enter address"} required/>
                        </Form.Group>

                        <Form.Group controlId={"dPostalCode"}>
                            <Form.Label>Postal code</Form.Label>
                            <Form.Control type={"text"} value={dPostalCode} onChange={setDeliveryPostalCode}
                                          minLength={"5"} maxLength={"5"} pattern={"^\\d+$"}
                                          placeholder={"Enter postal code"} required/>
                        </Form.Group>

                        <Form.Group controlId={"dPhoneNumber"}>
                            <Form.Label>Phone number</Form.Label>
                            <InputGroup>
                                <InputGroup.Prepend>
                                    <InputGroup.Text>+370</InputGroup.Text>
                                </InputGroup.Prepend>
                                <Form.Control type={"tel"} value={dPhoneNumber} onChange={setDeliveryPhoneNumber}
                                              minLength={"8"} maxLength={"8"} pattern={"^\\d+$"}
                                              placeholder={"Enter phone number"} required/>
                            </InputGroup>
                        </Form.Group>

                        <Form.Group controlId={"dType"}>
                            <Form.Label>Delivery type</Form.Label>
                            <Form.Control as={"select"} value={dType} onChange={setDeliveryType} custom>
                                <option value="STANDARD_SHIPPING">Standard Shipping</option>
                                <option value="FAST_SHIPPING">Fast Shipping</option>
                            </Form.Control>
                        </Form.Group>
                    </Col>
                </Row>

                <Form.Group controlId={"comment"}>
                    <Form.Label>Additional comments</Form.Label>
                    <Form.Control as={"textarea"} value={comment} onChange={setComment} placeholder={"Enter comment"}/>
                </Form.Group>

                <Button type={"button"} variant={'secondary'} onClick={history.goBack}>Cancel</Button>
                <Button type={"submit"} variant={'success'} className={'ml-2'}>Save</Button>
            </Form>

            <Modal show={modalText} onHide={handleModalClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Order editing failed</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    {modalText}
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleModalClose}>Close</Button>
                </Modal.Footer>
            </Modal>

            {successMessage && <Alert variant='success'>{successMessage}</Alert>}
        </React.Fragment>
    );
}