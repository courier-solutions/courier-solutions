import React, {useState} from "react";
import {Accordion, Alert, Button, Card, Col, Modal, Row} from "react-bootstrap";
import {PARCEL_TYPE} from "../../constants/ParcelType";
import {Link, useHistory, useLocation} from "react-router-dom";
import {cancelOrder} from "../../services/order.service";
import {DELIVERY_TYPE} from "../../constants/DeliveryType";
import {PAYMENT_TYPE} from "../../constants/PaymentType";

function ParcelDetails({parcel, parcelId}) {
    return (
        <Accordion className={'mt-3'}>
            <Card>
                <Accordion.Toggle as={Card.Header} eventKey="0">
                    Parcel {parcelId + 1}
                </Accordion.Toggle>
                <Accordion.Collapse eventKey="0">
                    <Card.Body>
                        <Row md={1} className={"align-items-center"}>
                            <Col>
                                <Row xs={1} md={4}>
                                    <Col>
                                        Length: {parcel.length}m
                                    </Col>
                                    <Col>
                                        Height: {parcel.height}m
                                    </Col>
                                    <Col>
                                        Width: {parcel.width}m
                                    </Col>
                                    <Col>
                                        Weight: {parcel.weight}kg
                                    </Col>
                                </Row>
                            </Col>
                            <Col>
                                <Row sm={4}>
                                    <Col>
                                        Type: {PARCEL_TYPE[parcel.type]}
                                    </Col>
                                    <Col>
                                        Quantity: {parcel.quantity}
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                    </Card.Body>
                </Accordion.Collapse>
            </Card>
        </Accordion>
    );
}

export default function OrderDetails() {
    const history = useHistory();
    const {state} = useLocation();
    const [order, setOrder] = useState(state);

    const [modalText, setModalText] = useState(null);

    function handleModalClose() {
        setModalText(null);
    }

    function handleCancelButton() {
        cancelOrder(order).then((res) => {
            setOrder(res);
        }).catch((err) => {
            setModalText((err.text) ? err.text : 'Server error. Please try again later.');
        });
    }

    function handleEditButton() {
        history.push('/edit-order', state);
    }

    if (order != null) {
        return (
            <React.Fragment>
                <Link to={"/view-orders"}>{"<"}Go back</Link>
                <h2>Order {order.create_timestamp} details</h2>

                <Row>
                    <Col md={6} className={'align-self-center'}>
                        <h5 className={'mb-0'}>Status: {order.status}</h5>
                    </Col>
                    <Col md={6}>
                        {order.status === 'VERIFIED' &&
                        <React.Fragment>
                            <div className={'d-none d-md-block'}>
                                <Button onClick={handleEditButton} className={'float-right ml-2 mb-2'}>Edit order</Button>
                                <Button variant='danger' onClick={handleCancelButton} className={'float-right'}>Cancel order</Button>
                            </div>
                            <div className={'d-block d-md-none'}>
                                <Button variant='danger' onClick={handleCancelButton} className={'mt-2'}>Cancel order</Button>
                                <Button onClick={handleEditButton} className={'mt-2 ml-2'}>Edit order</Button>
                            </div>
                        </React.Fragment>
                        }
                    </Col>
                </Row>

                {order.parcels.map((parcel, index) =>
                    <ParcelDetails key={index} parcel={parcel} parcelId={index}/>
                )}

                <Row xs={1} md={2}>
                    <Col className={'mt-3'}>
                        <Card>
                            <Card.Header>Pickup details</Card.Header>
                            <Card.Body>
                                <Card.Text>Sender name: {order.pickup_information.name}</Card.Text>
                                <Card.Text>Sender email: {order.pickup_information.email}</Card.Text>
                                <Card.Text>Sender phone number: {order.pickup_information.phone_number}</Card.Text>
                                <Card.Text>City: {order.pickup_information.city}</Card.Text>
                                <Card.Text>Address: {order.pickup_information.address}</Card.Text>
                                <Card.Text>Postal code: {order.pickup_information.postal_code}</Card.Text>
                                <Card.Text>Pickup date: {order.pickup_date}</Card.Text>
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col className={'mt-3'}>
                        <Card>
                            <Card.Header>Delivery details</Card.Header>
                            <Card.Body>
                                <Card.Text>Recipient name: {order.delivery_information.name}</Card.Text>
                                <Card.Text>Recipient email: {order.delivery_information.email}</Card.Text>
                                <Card.Text>Recipient phone number: {order.delivery_information.phone_number}</Card.Text>
                                <Card.Text>City: {order.delivery_information.city}</Card.Text>
                                <Card.Text>Address: {order.delivery_information.address}</Card.Text>
                                <Card.Text>Postal code: {order.delivery_information.postal_code}</Card.Text>
                                <Card.Text>Delivery type: {DELIVERY_TYPE[order.delivery_type]}</Card.Text>
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>

                <Card className={'mt-3 mb-3'}>
                    <Card.Header>Payment information</Card.Header>
                    <Card.Body>
                        <Row xs={1} md={2} lg={4}>
                            <Col>
                                <Card.Text>Payment Type: {PAYMENT_TYPE[order.payment_type]}</Card.Text>
                            </Col>
                            <Col>
                                <Card.Text>Cost: {order.cost} €</Card.Text>
                            </Col>
                        </Row>
                    </Card.Body>
                </Card>

                <Modal show={modalText} onHide={handleModalClose}>
                    <Modal.Header closeButton>
                        <Modal.Title>Order cancellation failed</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        {modalText}
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={handleModalClose}>Close</Button>
                    </Modal.Footer>
                </Modal>
            </React.Fragment>
        );
    } else {
        return <Alert variant='danger'>Error loading order details.</Alert>
    }
}