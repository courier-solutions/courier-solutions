import React, {useEffect, useState} from "react";
import {Alert, Card, Col, Row} from "react-bootstrap";
import {Link} from "react-router-dom";
import {getOrders} from "../../services/order.service";
import {FETCH_STATUS} from "../../constants/FetchStatus";

function OrderCard(order) {
    return (
        <Card className={'mt-3'}>
            <Card.Header>
                <Row xs={1} md={4}>
                    <Col xs={12} md={6}>
                        Order: {order.order.create_timestamp}
                    </Col>
                    <Col>
                        Cost: {order.order.cost} €
                    </Col>
                    <Col>
                        <b>Status: {order.order.status}</b>
                    </Col>
                </Row>
            </Card.Header>
            <Card.Body>
                <Row className={'align-items-center'}>
                    <Col xs={6} md={9}>
                        <Row xs={1} md={3}>
                            <Col>
                                <Card.Text>Brief info:</Card.Text>
                            </Col>
                            <Col>
                                <Card.Text># of parcels: {order.order.parcels.length}</Card.Text>
                            </Col>
                            <Col>
                                <Card.Text>To: {order.order.delivery_information.name}</Card.Text>
                            </Col>
                        </Row>
                    </Col>
                    <Col xs={6} md={3}>
                        <Link to={{
                            pathname: "/order-details",
                            state: order.order
                        }}>View details</Link>
                    </Col>
                </Row>
            </Card.Body>
        </Card>
    );
}

export default function ViewOrders() {
    const [orders, setOrders] = useState([]);
    const [fetchStatus, setFetchStatus] = useState(FETCH_STATUS.LOADING);

    useEffect(() => {
        fetchOrders();
    }, []);

    function fetchOrders() {
        getOrders().then((res) => {
            setOrders(res);
            setFetchStatus(FETCH_STATUS.SUCCESS);
        }).catch((err) => {
            setFetchStatus(FETCH_STATUS.ERROR);
        });
    }

    if (fetchStatus === FETCH_STATUS.ERROR) {
        return (
            <React.Fragment>
                <h2>Your orders</h2>
                <Alert variant='danger'>Error loading orders. Please refresh the page.</Alert>
            </React.Fragment>
        );
    } else if (fetchStatus === FETCH_STATUS.LOADING) {
        return (
            <React.Fragment>
                <h2>Your orders</h2>
                <Alert variant='info'>Loading orders. Please wait.</Alert>
            </React.Fragment>
        );
    } else if (orders.length === 0) {
        return (
            <React.Fragment>
                <h2>Your orders</h2>
                <Alert variant='info'>There are no orders at this time.</Alert>
            </React.Fragment>
        );
    }

    return (
        <React.Fragment>
            <h2>Your orders</h2>
            {orders.map((order, index) => <OrderCard key={index} order={order}/>)}
        </React.Fragment>
    );
}