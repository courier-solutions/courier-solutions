import './OrderCreate.css';
import React, {useEffect, useState} from "react";
import {Alert, Button, Col, Form, Image, InputGroup, Modal, Row} from "react-bootstrap";
import {create} from "../../services/order.service";
import {PARCEL_TYPE} from "../../constants/ParcelType";
import {DELIVERY_TYPE} from "../../constants/DeliveryType";
import {calculateOrderCost, calculateParcelCost} from "../../utils/OrderCost";
import {useFormInput} from "../../utils/FormInputHook";
import {Parcel} from "../common/Parcel";
import {useHistory} from "react-router-dom";
import {PAYMENT_TYPE} from "../../constants/PaymentType";
import PriceTable from "../common/PriceTable";
import infoIcon from "../../assets/info-circle.svg";

function PaymentInformationComponent({paymentInformation, disablePaymentInfo, handleChange}) {
    return (
        <React.Fragment>
            <Row xs={1} md={2} lg={4}>
                <Form.Group as={Col} controlId={"cardOwner"}>
                    <Form.Label>Card owner name</Form.Label>
                    <Form.Control name={"cardOwner"} placeholder={"Enter card owner"}
                                  value={paymentInformation.cardOwner} onChange={(e) => handleChange(e)} required
                                  disabled={disablePaymentInfo}/>
                </Form.Group>

                <Form.Group as={Col} controlId={"cardNumber"}>
                    <Form.Label>Credit / Debit card number</Form.Label>
                    <Form.Control name={"cardNumber"} placeholder={"Enter card number"}
                                  value={paymentInformation.cardNumber} onChange={(e) => handleChange(e)}
                                  minLength={"16"} maxLength={"16"} pattern={"^[1-9][0-9]{15}"} required
                                  disabled={disablePaymentInfo}/>
                </Form.Group>
            </Row>
            <Row lg={4}>
                <Form.Group as={Col} controlId={"securityCode"}>
                    <Form.Label>Card security code</Form.Label>
                    <Form.Control name={"securityCode"} placeholder={"CVV"} value={paymentInformation.securityCode}
                                  onChange={(e) => handleChange(e)} maxLength={"3"} pattern={"^[0-9]{3}"} required
                                  disabled={disablePaymentInfo}/>
                </Form.Group>

                <Form.Group as={Col} controlId={"validThru"}>
                    <Form.Label>Card valid thru</Form.Label>
                    <Form.Control name={"validThru"} placeholder={"MM/YY"} value={paymentInformation.validThru}
                                  onChange={(e) => handleChange(e)} maxLength={"5"}
                                  pattern={"^(0[1-9]|1[0-2])\\/[0-9]{2}"} required disabled={disablePaymentInfo}/>
                </Form.Group>
            </Row>
        </React.Fragment>
    );
}

export default function OrderCreate() {
    // Parcel vars
    const [parcels, setParcels] = useState([
        {length: 0, height: 0, width: 0, weight: 0, type: Object.keys(PARCEL_TYPE)[0], quantity: 1}
    ]);
    const [parcelsCost, setParcelsCost] = useState([0]);

    const [paymentInformation, setPaymentInformation] = useState(
        {cardOwner: "", cardNumber: "", securityCode: "", validThru: ""}
    );

    // Pickup vars
    const [pName, setPickupName] = useFormInput("");
    const [pEmail, setPickupEmail] = useFormInput("");
    const [pCity, setPickupCity] = useFormInput("");
    const [pAddress, setPickupAddress] = useFormInput("");
    const [pPostalCode, setPickupPostalCode] = useFormInput("");
    const [pPhoneNumber, setPickupPhoneNumber] = useFormInput("");
    const [pDate, setPickupDate] = useFormInput("");
    // Delivery vars
    const [dName, setDeliveryName] = useFormInput("");
    const [dEmail, setDeliveryEmail] = useFormInput("");
    const [dCity, setDeliveryCity] = useFormInput("");
    const [dAddress, setDeliveryAddress] = useFormInput("");
    const [dPostalCode, setDeliveryPostalCode] = useFormInput("");
    const [dPhoneNumber, setDeliveryPhoneNumber] = useFormInput("");
    const [dType, setDeliveryType] = useFormInput(Object.keys(DELIVERY_TYPE)[0]);

    const [comment, setComment] = useFormInput("");

    const [orderCost, setOrderCost] = useState(0);

    const [priceTable, setPriceTable] = useState(false);

    const [paymentType, setPaymentType] = useFormInput(Object.keys(PAYMENT_TYPE)[0]);
    const [disablePaymentInfo, setDisablePaymentInfo] = useState(false);

    const today = new Date().toISOString().split('T')[0];


    const [error, setError] = useState(false);
    const history = useHistory();

    useEffect(() => {
        setOrderCost(calculateOrderCost(parcelsCost, pDate, dType, pPostalCode, dPostalCode, today));
    }, [parcelsCost, pDate, dType, pPostalCode, dPostalCode, today]);

    useEffect(() => {
        const tmpDisable = paymentType !== Object.keys(PAYMENT_TYPE)[0]

        if (tmpDisable === true) {
            setPaymentInformation({cardOwner: "", cardNumber: "", securityCode: "", validThru: ""})
        }

        setDisablePaymentInfo(tmpDisable)
    }, [paymentType]);

    function handleParcelChange(event, index) {
        const values = [...parcels];
        values[index][event.target.name] = event.target.value;
        setParcels(values);

        const costs = [...parcelsCost];
        costs[index] = calculateParcelCost(values[index]);
        setParcelsCost(costs);
    }

    function handleParcelRemove(index) {
        const values = [...parcels];
        values.splice(index, 1);
        setParcels(values);

        const costs = [...parcelsCost];
        costs.splice(index, 1);
        setParcelsCost(costs);
    }

    function handleParcelAdd() {
        const values = [...parcels];
        values.push({length: 0, height: 0, width: 0, weight: 0, type: Object.keys(PARCEL_TYPE)[3], quantity: 1});
        setParcels(values);

        const costs = [...parcelsCost];
        costs.push(0);
        setParcelsCost(costs);
    }

    function handlePaymentInformationChange(event) {
        const values = {...paymentInformation};
        values[event.target.name] = event.target.value;
        setPaymentInformation(values);
    }

    function handleSubmit(e) {
        e.preventDefault();

        const order = {
            parcels: parcels,
            pickup_information: {
                name: pName,
                email: pEmail,
                city: pCity,
                address: pAddress,
                postal_code: pPostalCode,
                phone_number: `+370${pPhoneNumber}`,
            },
            pickup_date: pDate,
            delivery_information: {
                name: dName,
                email: dEmail,
                city: dCity,
                address: dAddress,
                postal_code: dPostalCode,
                phone_number: `+370${dPhoneNumber}`,
            },
            delivery_type: dType,
            comment: comment,
            payment_type: paymentType,
            payment_information: {
                card_owner: paymentInformation.cardOwner,
                card_number: paymentInformation.cardNumber,
                security_code: paymentInformation.securityCode,
                valid_thru: paymentInformation.validThru,
            }
        };

        create(order)
            .then(() => {
                history.push("/view-orders")
            })
            .catch(() => {
                setError(true)
            });
    }

    return (
        <React.Fragment>
            <Alert variant="info">
                <Alert.Heading>Total cost: {orderCost} €
                    <Image className={"float-right clickable-image"} src={infoIcon}
                           onClick={() => setPriceTable(true)}/>
                </Alert.Heading>
            </Alert>

            <Form onSubmit={handleSubmit}>
                <h2>Package Info</h2>
                {parcels.map((parcel, index) => (
                    <Parcel key={index} parcel={parcel} index={index} handleChange={handleParcelChange}
                            handleRemove={handleParcelRemove} cost={parcelsCost[index]}/>
                ))}
                <Button type="button" className={"mb-3"} onClick={handleParcelAdd}>Add package</Button>

                <Row xs={1} sm={2}>
                    <Col>
                        <h2>Pickup Info</h2>

                        <Form.Group controlId={"pName"}>
                            <Form.Label>Full name</Form.Label>
                            <Form.Control type={"text"} value={pName} onChange={setPickupName}
                                          placeholder={"Enter name"} required/>
                        </Form.Group>

                        <Form.Group controlId={"formBasicEmail"}>
                            <Form.Label>Email address</Form.Label>
                            <Form.Control type={"email"} value={pEmail} onChange={setPickupEmail}
                                          placeholder={"Enter email"} required/>
                        </Form.Group>

                        <Form.Group controlId={"pCity"}>
                            <Form.Label>City</Form.Label>
                            <Form.Control type={"text"} value={pCity} onChange={setPickupCity}
                                          placeholder={"Enter city"} required/>
                        </Form.Group>

                        <Form.Group controlId={"pAddress"}>
                            <Form.Label>Address</Form.Label>
                            <Form.Control type={"text"} value={pAddress} onChange={setPickupAddress}
                                          placeholder={"Enter address"} required/>
                        </Form.Group>

                        <Form.Group controlId={"pPostalCode"}>
                            <Form.Label>Postal code</Form.Label>
                            <Form.Control type={"text"} value={pPostalCode} onChange={setPickupPostalCode}
                                          minLength={"5"} maxLength={"5"} pattern={"^\\d+$"}
                                          placeholder={"Enter postal code"} required/>
                        </Form.Group>

                        <Form.Group controlId={"pPhoneNumber"}>
                            <Form.Label>Phone number</Form.Label>
                            <InputGroup>
                                <InputGroup.Prepend>
                                    <InputGroup.Text>+370</InputGroup.Text>
                                </InputGroup.Prepend>
                                <Form.Control type={"tel"} value={pPhoneNumber} onChange={setPickupPhoneNumber}
                                              minLength={"8"} maxLength={"8"} pattern={"^\\d+$"}
                                              placeholder={"Enter phone number"} required/>
                            </InputGroup>
                        </Form.Group>

                        <Form.Group controlId={"pDate"}>
                            <Form.Label>Pickup date</Form.Label>
                            <Form.Control type={"date"} value={pDate} onChange={setPickupDate} min={today}
                                          placeholder={"Enter pickup date"} required/>
                        </Form.Group>
                    </Col>

                    <Col>
                        <h2>Delivery Info</h2>

                        <Form.Group controlId={"dName"}>
                            <Form.Label>Full name</Form.Label>
                            <Form.Control type={"text"} value={dName} onChange={setDeliveryName}
                                          placeholder={"Enter name"} required/>
                        </Form.Group>

                        <Form.Group controlId={"formBasicEmail"}>
                            <Form.Label>Email address</Form.Label>
                            <Form.Control type={"email"} value={dEmail} onChange={setDeliveryEmail}
                                          placeholder={"Enter email"} required/>
                        </Form.Group>

                        <Form.Group controlId={"dCity"}>
                            <Form.Label>City</Form.Label>
                            <Form.Control type={"text"} value={dCity} onChange={setDeliveryCity}
                                          placeholder={"Enter city"} required/>
                        </Form.Group>

                        <Form.Group controlId={"dAddress"}>
                            <Form.Label>Address</Form.Label>
                            <Form.Control type={"text"} value={dAddress} onChange={setDeliveryAddress}
                                          placeholder={"Enter address"} required/>
                        </Form.Group>

                        <Form.Group controlId={"dPostalCode"}>
                            <Form.Label>Postal code</Form.Label>
                            <Form.Control type={"text"} value={dPostalCode} onChange={setDeliveryPostalCode}
                                          minLength={"5"} maxLength={"5"} pattern={"^\\d+$"}
                                          placeholder={"Enter postal code"} required/>
                        </Form.Group>

                        <Form.Group controlId={"dPhoneNumber"}>
                            <Form.Label>Phone number</Form.Label>
                            <InputGroup>
                                <InputGroup.Prepend>
                                    <InputGroup.Text>+370</InputGroup.Text>
                                </InputGroup.Prepend>
                                <Form.Control type={"tel"} value={dPhoneNumber} onChange={setDeliveryPhoneNumber}
                                              minLength={"8"} maxLength={"8"} pattern={"^\\d+$"}
                                              placeholder={"Enter phone number"} required/>
                            </InputGroup>
                        </Form.Group>

                        <Form.Group controlId={"dType"}>
                            <Form.Label>Delivery type</Form.Label>
                            <Form.Control as={"select"} value={dType} onChange={setDeliveryType} custom>
                                {Object.keys(DELIVERY_TYPE).map(key => (
                                    <option key={key} value={key}>{DELIVERY_TYPE[key]}</option>
                                ))}
                            </Form.Control>
                        </Form.Group>
                    </Col>
                </Row>

                <Form.Group controlId={"comment"}>
                    <Form.Label>Additional comments</Form.Label>
                    <Form.Control as={"textarea"} value={comment} onChange={setComment} placeholder={"Enter comment"}/>
                </Form.Group>

                <Form.Group as={Col} md={6} lg={4} className={"px-0"} controlId={"paymentType"}>
                    <Form.Label>Payment type</Form.Label>
                    <Form.Control as={"select"} value={paymentType} onChange={setPaymentType} custom>
                        {Object.keys(PAYMENT_TYPE).map(key => (
                            <option key={key} value={key}>{PAYMENT_TYPE[key]}</option>
                        ))}
                    </Form.Control>
                </Form.Group>

                <PaymentInformationComponent paymentInformation={paymentInformation}
                                             disablePaymentInfo={disablePaymentInfo}
                                             handleChange={handlePaymentInformationChange}/>

                <Button variant={"success"} type={"submit"}>Create</Button>
            </Form>

            <Modal show={priceTable} onHide={() => setPriceTable(false)}>
                <Modal.Header closeButton>
                    <Modal.Title>Price Table</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <PriceTable/>
                </Modal.Body>
            </Modal>

            <Modal show={error} onHide={() => setError(false)}>
                <Modal.Header closeButton>
                    <Modal.Title>Server error</Modal.Title>
                </Modal.Header>

                <Modal.Body>
                    <p>Please try again later.</p>
                </Modal.Body>
            </Modal>
        </React.Fragment>
    );
}