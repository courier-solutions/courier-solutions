import React, {useEffect, useState} from "react";
import {PARCEL_TYPE} from "../../constants/ParcelType";
import {DELIVERY_TYPE} from "../../constants/DeliveryType";
import {Alert, Button, Col, Form, Image, Modal, Row} from "react-bootstrap";
import {Parcel} from "../common/Parcel";
import {useFormInput} from "../../utils/FormInputHook";
import {calculateOrderCost, calculateParcelCost} from "../../utils/OrderCost";
import infoIcon from "../../assets/info-circle.svg"
import PriceTable from "../common/PriceTable";

export default function PriceCheck() {
    const [parcels, setParcels] = useState([
        {length: 0, height: 0, width: 0, weight: 0, type: Object.keys(PARCEL_TYPE)[3], quantity: 1, cost: 0}
    ]);
    const [parcelsCost, setParcelsCost] = useState([0]);

    const [date, setPickupDate] = useFormInput("");
    const [type, setDeliveryType] = useFormInput(Object.keys(DELIVERY_TYPE)[0]);
    const [pPostalCode, setPickupPostalCode] = useFormInput("");
    const [dPostalCode, setDeliveryPostalCode] = useFormInput("");

    const [orderCost, setOrderCost] = useState(0);

    const [priceTable, setPriceTable] = useState(false);

    const today = new Date().toISOString().split('T')[0];

    useEffect(() => {
        setOrderCost(calculateOrderCost(parcelsCost, date, type, pPostalCode, dPostalCode, today));
    }, [parcelsCost, date, type, pPostalCode, dPostalCode, today]);

    function handleParcelChange(event, index) {
        const values = [...parcels];
        values[index][event.target.name] = event.target.value;
        setParcels(values);

        const costs = [...parcelsCost];
        costs[index] = calculateParcelCost(values[index]);
        setParcelsCost(costs);
    }

    function handleParcelRemove(index) {
        const values = [...parcels];
        values.splice(index, 1);
        setParcels(values);

        const costs = [...parcelsCost];
        costs.splice(index, 1);
        setParcelsCost(costs);
    }

    function handleParcelAdd() {
        const values = [...parcels];
        values.push({length: 0, height: 0, width: 0, weight: 0, type: Object.keys(PARCEL_TYPE)[3], quantity: 1});
        setParcels(values);

        const costs = [...parcelsCost];
        costs.push(0);
        setParcelsCost(costs);
    }

    return (
        <React.Fragment>
            <Alert variant="info">
                <Alert.Heading>Total cost: {orderCost} €
                    <Image className={"float-right clickable-image"} src={infoIcon}
                           onClick={() => setPriceTable(true)}/>
                </Alert.Heading>
            </Alert>

            {parcels.map((parcel, index) => (
                <Parcel key={index} parcel={parcel} index={index} handleChange={handleParcelChange}
                        handleRemove={handleParcelRemove} cost={parcelsCost[index]}/>
            ))}
            <Button type="button" onClick={handleParcelAdd}>Add package</Button>

            <br/><br/>
            <Row lg={4} md={2} xs={1}>
                <Form.Group as={Col} controlId={"pPostalCode"}>
                    <Form.Label>Pickup postal code</Form.Label>
                    <Form.Control type={"text"} value={pPostalCode} onChange={setPickupPostalCode} minLength={"5"}
                                  maxLength={"5"} pattern={"^\\d+$"} placeholder={"Enter postal code"}/>
                </Form.Group>
                <Form.Group as={Col} controlId={"dPostalCode"}>
                    <Form.Label>Delivery postal code</Form.Label>
                    <Form.Control type={"text"} value={dPostalCode} onChange={setDeliveryPostalCode} minLength={"5"}
                                  maxLength={"5"} pattern={"^\\d+$"} placeholder={"Enter pickup postal code"}/>
                </Form.Group>
            </Row>
            <Row lg={4} md={2} xs={1}>
                <Form.Group as={Col} controlId={"date"}>
                    <Form.Label>Pickup date</Form.Label>
                    <Form.Control type={"date"} value={date} onChange={setPickupDate} min={today}/>
                </Form.Group>
                <Form.Group as={Col} controlId={"type"}>
                    <Form.Label>Delivery type</Form.Label>
                    <Form.Control as={"select"} value={type} onChange={setDeliveryType} custom>
                        {Object.keys(DELIVERY_TYPE).map(key => (
                            <option key={key} value={key}>{DELIVERY_TYPE[key]}</option>
                        ))}
                    </Form.Control>
                </Form.Group>
            </Row>

            <Modal show={priceTable} onHide={() => setPriceTable(false)}>
                <Modal.Header closeButton>
                    <Modal.Title>Price Table</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <PriceTable/>
                </Modal.Body>
            </Modal>
        </React.Fragment>
    );
}