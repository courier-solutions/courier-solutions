export const PAYMENT_TYPE = {
    DEBIT_CREDIT_CARD: 'Debit / Credit Card',
    BANK_PAYMENT: 'Bank payment',
    CASH: 'Cash',
}