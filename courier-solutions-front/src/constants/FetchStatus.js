export const FETCH_STATUS = {
    LOADING: 0,
    SUCCESS: 1,
    ERROR: 2,
}