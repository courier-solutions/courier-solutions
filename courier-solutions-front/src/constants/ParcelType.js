export const PARCEL_TYPE = {
    ANIMALS: 'Animals',
    DOCUMENTS: 'Documents',
    FRAGILE: 'Fragile',
    STANDARD: 'Standard',
}