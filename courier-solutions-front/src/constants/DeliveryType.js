export const DELIVERY_TYPE = {
    STANDARD_SHIPPING: 'Standard Shipping',
    FAST_SHIPPING: 'Fast Shipping',
}