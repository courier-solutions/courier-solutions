import React from "react";
import {Container, Nav, Navbar} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";
import {useAuth} from "./context/AuthContext";

function UnauthenticatedNav() {
    return (
        <Navbar.Collapse className={"justify-content-end"}>
            <Nav>
                <LinkContainer to='/register'>
                    <Nav.Link>Register</Nav.Link>
                </LinkContainer>
                <LinkContainer to='/login'>
                    <Nav.Link>Login</Nav.Link>
                </LinkContainer>
            </Nav>
        </Navbar.Collapse>
    );
}

function AuthenticatedNav() {
    return (
        <Navbar.Collapse>
            <Nav className={"mr-auto"}>
                <LinkContainer to='/view-orders'>
                    <Nav.Link>View orders</Nav.Link>
                </LinkContainer>
                <LinkContainer to='/create-order'>
                    <Nav.Link>Create order</Nav.Link>
                </LinkContainer>
                <LinkContainer to='/price-check'>
                    <Nav.Link>Price estimation</Nav.Link>
                </LinkContainer>
                <LinkContainer to='/profile'>
                    <Nav.Link>Profile</Nav.Link>
                </LinkContainer>
            </Nav>
            <Nav>
                <LinkContainer to='/logout'>
                    <Nav.Link>Logout</Nav.Link>
                </LinkContainer>
            </Nav>
        </Navbar.Collapse>
    );
}

function Header() {
    const {isAuthenticated} = useAuth();
    return (
        // <header>
        <Navbar bg='dark' variant='dark' expand='lg' sticky='top' collapseOnSelect>
            <Container>
                <LinkContainer to='/'>
                    <Navbar.Brand>Courier Solutions</Navbar.Brand>
                </LinkContainer>

                <Navbar.Toggle/>
                {isAuthenticated ? <AuthenticatedNav/> : <UnauthenticatedNav/>}
            </Container>
        </Navbar>
        // </header>
    );
}

export default Header;