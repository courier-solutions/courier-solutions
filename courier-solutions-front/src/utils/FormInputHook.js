import {useState} from "react";

function useFormInput(initialValue) {
    const [value, setValue] = useState(initialValue);

    function changeValue(newValue) {
        setValue(newValue);
    }

    function handleChange(e) {
        setValue(e.target.value);
    }

    return [value, handleChange, changeValue];
}

export {useFormInput}