import {Redirect, Route} from "react-router-dom";
import {useAuth} from "../context/AuthContext";

function AuthRoute({children, ...rest}) {
    const {isAuthenticated} = useAuth();
    return (
        <Route {...rest} render={() =>
            isAuthenticated ? <>{children}</> : <Redirect to={"/"}/>
        }/>
    );
}

export default AuthRoute