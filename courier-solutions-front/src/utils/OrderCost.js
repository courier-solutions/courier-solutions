import {DELIVERY_TYPE} from "../constants/DeliveryType";
import {PARCEL_TYPE} from "../constants/ParcelType";

const FAST_SHIPPING = Object.keys(DELIVERY_TYPE)[1]

const ANIMALS = Object.keys(PARCEL_TYPE)[0]
const DOCUMENTS = Object.keys(PARCEL_TYPE)[1]
const FRAGILE = Object.keys(PARCEL_TYPE)[2]

export function calculateParcelCost(parcel) {
    // Every 1m^3 = 5 Eur
    let parcelCost = parcel.length * parcel.width * parcel.height * 5;

    // Every 1kg = 50ct
    parcelCost += parcel.weight * 0.5;

    // Quantity multiplier
    parcelCost *= Math.floor(parcel.quantity);

    // Parcel type multiplier
    switch (parcel.type) {
        case ANIMALS:
            parcelCost *= 2;
            break;
        case DOCUMENTS:
            parcelCost *= 1.1;
            break;
        case FRAGILE:
            parcelCost *= 1.5;
            break;
        default:
            break;
    }

    parcelCost = Math.ceil(parcelCost * 100) / 100;
    return isNaN(parcelCost) ? 0 : parcelCost;
}

export function calculateOrderCost(parcelCosts, date, type, pickupPostalCode, deliveryPostalCode, today) {
    let newCost = parcelCosts.reduce((partial_sum, element) => partial_sum + element);

    if (pickupPostalCode.length === 5 && deliveryPostalCode.length === 5) {
        newCost += calculateDistanceCost(pickupPostalCode, deliveryPostalCode);
    }

    if (date === today) {
        newCost += 1;
    }
    if (type === FAST_SHIPPING) {
        newCost += 2;
    }

    if (newCost < 3) {
        newCost = 3;
    }

    return newCost;
}

// TODO see: https://ba-jira.atlassian.net/browse/CS-36
function calculateDistanceCost(pickupPostalCode, deliveryPostalCode) {
    // ...
    // if (distance < 20) cost = distance * 0.1
    // if (distance > 100) cost = distance * 0.2
    // else cost = distance * 0.15

    // For now we'll use predefined value
    return 25;
}