const baseURL = process.env.REACT_APP_BACKEND_BASE_URL
const basicAuth = process.env.REACT_APP_BASIC_AUTHORIZATION

async function client(endpoint, {data, ...customConfig} = {}) {
    const config = {
        method: data ? "POST" : "GET",
        body: data ? JSON.stringify(data) : undefined,
        headers: {
            "Authorization": `Basic ${btoa(basicAuth)}`,
            "Content-Type": data ? "application/json" : undefined,
            "Accept": "application/json",
            ...customConfig.headers,
        },
        credentials: "include",
        ...customConfig,
    }

    return window.fetch(`${baseURL}/${endpoint}`, config)
        .then(async response => {
            if (response.status === 401) {
                localStorage.removeItem('__auth_key__');
                window.location.assign(window.location);
            }

            const result = await response.json()
            if (response.ok) {
                return result
            } else {
                return Promise.reject(result)
            }
        })
}

export {client}