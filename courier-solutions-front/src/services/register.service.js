import {client} from "../utils/APIClient";

function register(clientData) {
    return client("register", {data: clientData})
}

export { register }