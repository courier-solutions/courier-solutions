import {client} from "../utils/APIClient";

function login(loginData) {
    return client("login", {data: loginData})
}

function logout() {
    return client('logout', {method: 'POST'});
}

export {login, logout}
