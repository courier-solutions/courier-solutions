import {client} from "../utils/APIClient";

function create(orderData) {
    return client("orders", {data: orderData})
}

function getOrders() {
    return client("orders")
}

function cancelOrder(orderData) {
    return client(`orders/cancel/${orderData.id}`, {method: 'PUT'});
}

function editOrder(orderData) {
    return client(`orders/${orderData.id}`, {data: orderData, method: 'PUT'});
}

export {create, getOrders, cancelOrder, editOrder}
