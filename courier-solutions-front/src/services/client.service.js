import {client} from "../utils/APIClient";

function putClientPass(clientPass) {
    return client("client-password", {data: clientPass, method: "PUT"})
}

function putClientData(clientData, bypassOptLock) {
    let endpoint = 'client-info';
    if (bypassOptLock) {
        endpoint += '?bypassOptLock=true';
    }

    return client(endpoint, {data: clientData, method: "PUT"})
}

function getClient() {
    return client("client-info")
}

export {putClientPass, putClientData, getClient}
