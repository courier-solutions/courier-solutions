import {createContext, useContext, useState} from "react";

const authKey = '__auth_key__';
const AuthContext = createContext();
const {Provider} = AuthContext;

function AuthProvider({children}) {
    const auth = localStorage.getItem(authKey);

    const [isAuthenticated, setAuthenticated] = useState(auth !== null);

    function setAuth(authenticated) {
        localStorage.setItem(authKey, authenticated);
        setAuthenticated(authenticated);
    }

    function logout() {
        localStorage.removeItem(authKey);
        setAuthenticated(false);
    }

    return (
        <Provider value={{isAuthenticated, setAuthenticated: authenticated => setAuth(authenticated), logout}}>
            {children}
        </Provider>
    );
}

function useAuth() {
    return useContext(AuthContext);
}

export {AuthProvider, useAuth};