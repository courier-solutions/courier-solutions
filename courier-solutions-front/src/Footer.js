import React from "react";

function Footer() {
    const year = new Date().getFullYear()
    return (
        <footer className='bg-dark mt-auto py-3 text-white text-center'>
            &copy; {year} Courier Solutions
        </footer>
    );
}

export default Footer;